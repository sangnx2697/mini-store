package view;

import AppPackage.AnimationClass;
import Model.Dao.Statictis_DAO;
import Model.Dao.Users_DAO;
import Model.Helper.AnimationHelper;
import Model.Helper.ButtonAction;
import Model.Helper.Chart;
import Model.Helper.ColorHelper;
import Model.Helper.Dialog;
import Model.Helper.FillTxt;
import Model.Helper.LabelHeper;
import Model.Helper.Navigation;
import Model.Helper.VisibleHelper;
import Model.Helper.SolveIMG;
import Model.Table.CTHDB;
import Model.Table.CTHDM;
import Model.Table.HoaDonBan;
import Model.Table.HoaDonMua;
import Model.Table.LuongNV;
import Model.Table.NhaCungCap;
import Model.Table.SanPham;
import Model.Table.TTNS;
import Model.Table.Users;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableColumn;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;
import library.LimitText;
import library.XLDL;
import library.XLICon;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author nhm95
 */
public class MainFrame extends javax.swing.JFrame {

    private String Path = "";
    private final AnimationClass animation = new AnimationClass();
    private boolean animateSet = true;
    private final JFrame f = this;
    private boolean checkrun = true,
            chklblmanageemployee_status = true,
            chklblmanageemployee_user = true,
            chklblmanageemployee_invoiceout = true,
            checkManageEmployee = true,
            checkManageProducts = true,
            chklblmanageproducts_vendors = true,
            chklblmanageproducts_product = true,
            chklblmanageproducts_invoicein = true,
            chklblmenusystem_manageclick = true,
            chklblmanageemployee_invoiceoutclick = true;

    private Object[] value = null;
    private Object[] name = null;
    private ArrayList<Model.Table.Statictis> arr = new ArrayList();

    /**
     * Creates new form MainFrame
     */
    public MainFrame() {
        initComponents();

        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        Containner();
        txtUser.setText("taichinh2");
        txtPass.setText("tc123b");
    }

    private void Containner() {
        Edit();
        Icon();
        Thread liveAction = new Thread(new Runnable() {
            @Override
            public void run() {
                Action();
            }
        });
        Thread getdata = new Thread(new Runnable() {
            @Override
            public void run() {
                liveAction.start();
                GetTableData();
                ShowDataInTextField();
                CheckLength();
            }
        });
        getdata.start();
    }

    private void ShowDataInTextField() {
        FillTxt.ShowHDB(tblInvoicesOut, txtInvoiceSold_ID, txtInvoiceSold_ImplID, txtInvoiceSold_DateCreated, txtInvoiceSold_ProductID, txtInvoiceSold_Price, txtInvoiceSold_Quantity, txtInvoiceSold_TotalMoney);
        FillTxt.ShowHDM(tblManageProducts_InvoicesIn, txtInvoiceBought_ID1, txtInvoiceBought_ImplID, txtInvoiceBought_ProductID, txtInvoiceBought_VendorID, txtInvoiceBought_Price, txtInvoiceBought_Quantity, txtInvoiceBought_DateCreated, txtInvoiceBought_TotalMoney);
        FillTxt.ShowLuongNV(tblManageFinacial, txtIFinalcial_PositionID, txtFinacial_Position, txtIFinacial_Salary);
        FillTxt.ShowTTNV(tblStatusEmployees, txtEmployee_ID, txtEmployee_Name, txtEmployee_Address, txtEployee_PersonalID, cbbEmployee_Position, lblEmloyee_Avatar);
        FillTxt.ShowUsers(tblUser, txtUser_EmplID, txtUser_Username, txtUser_Password, cbbUser_role, lblUser_Avatar);
        FillTxt.ShowVendor(tblManageProducts_Vendors, txtVendor_ID, txtVendor_Name, txtVendor_Address);
        FillTxt.Showsp(tblManageProducts_Product, txtProduct_ID, txtProduct_Name, txtProduct_VendorID, cbbProduct_Unit, txtProduct_Price, txtProduct_Quantity);
    }

    private void Action() {
        ShowWorks();
        LabelClick();
        LabelHover();
        Button();
        ButtonAction();
        showchartall();
        showchart();
        showchartmonth();
        changeradio();
        cbbaction();
        changesizeTbl();
    }
    Chart chart = null;
    int index = 0;
    String textfind = "";

    private void changesizeTbl() {
        TableColumn column = tblManageFinacial.getColumnModel().getColumn(1);
        column.setPreferredWidth(30);
    }

    private void changeradio() {
        // chỉnh các radio của bảng finacial
        rdbStatictis_Finacial_All.setSelected(true);
        rdbStatictis_InvoiceIn_All.setSelected(true);
        rdbStatictis_InvoiceOut_All.setSelected(true);
        cbbStatictis_Finacial.setVisible(false);
        cbbStatictis_InvoiceIn.setVisible(false);
        cbbStatictis_InvoiceOutl.setVisible(false);

        rdbStatictis_Finacial_All.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (rdbStatictis_Finacial_All.isSelected()) {
                    cbbStatictis_Finacial.setVisible(false);
                }
            }
        });
        rdbStatictis_Finalcial_Month.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (rdbStatictis_Finalcial_Month.isSelected()) {
                    cbbStatictis_Finacial.setVisible(true);
                }
            }
        });
        // chỉnh các radio của bảng invoiceIn
        rdbStatictis_InvoiceIn_All.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (rdbStatictis_InvoiceIn_All.isSelected()) {
                    cbbStatictis_InvoiceIn.setVisible(false);
                }
            }
        });
        rdbStatictis_InvoiceIn_Month.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (rdbStatictis_InvoiceIn_Month.isSelected()) {
                    cbbStatictis_InvoiceIn.setVisible(true);
                }
            }
        });
        // chỉnh các radio của bảng invoiceOut
        rdbStatictis_InvoiceOut_All.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (rdbStatictis_InvoiceOut_All.isSelected()) {
                    cbbStatictis_InvoiceOutl.setVisible(false);
                }
            }
        });
        rdbStatictis_InvoiceOut_Month.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (rdbStatictis_InvoiceOut_Month.isSelected()) {
                    cbbStatictis_InvoiceOutl.setVisible(true);
                }
            }
        });
    }

    private void ButtonAction() {
        Component[] cbn = pnlManageProducts_ProductController.getComponents();
        System.out.println(cbn.length);
        System.out.println(pnlManageProducts_ProductController.getComponentCount());
        /*
         Button Employee Status
         */
 /* button Insert Personal */
        btnEmployee_Insert.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                TTNS t = new TTNS(txtEmployee_ID.getText(), txtEmployee_Name.getText(), txtEmployee_Address.getText(), txtEployee_PersonalID.getText(), cbbEmployee_Position.getSelectedItem().toString(), !Path.equals("") ? Path : "account.png", TTNS.INSERT);
                t.GetDataNS(tblStatusEmployees, cbbEmployee_Position);
                Path = "";
            }
        });
        /* button Update Personal*/
        btnEmployee_Update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                TTNS t = new TTNS(txtEmployee_ID.getText(), txtEmployee_Name.getText(), txtEmployee_Address.getText(), txtEployee_PersonalID.getText(), cbbEmployee_Position.getSelectedItem().toString(), !Path.equals("") ? Path : "account.png", TTNS.UPDATE);
                t.GetDataNS(tblStatusEmployees, cbbEmployee_Position);
                Path = "";
            }
        });
        /* button Delete Personal */
        btnEmployee_Delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                int index = tblStatusEmployees.getSelectedRow();
                if (index > -1) {
                    System.out.println(tblStatusEmployees.getValueAt(index, 0).toString());
                    TTNS t = new TTNS(tblStatusEmployees.getValueAt(index, 0).toString(), TTNS.DELETE);
                    t.GetDataNS(tblStatusEmployees, cbbEmployee_Position);
                } else {
                    TTNS t = new TTNS(txtEmployee_ID.getText(), TTNS.DELETE);
                    t.GetDataNS(tblStatusEmployees, cbbEmployee_Position);
                }
            }
        });
        /* button Find Personal */
        btnEmloyee_Find.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new TTNS().GetDataNS(tblStatusEmployees, txtEmployee_Find.getText(), rdoPersonal_EmplID.isSelected() ? "MaNV" : rdoPersonal_Position.isSelected() ? "MaCV" : rdoPersonal_Name.isSelected() ? "HoTen" : "CMND");
            }
        });
        /* KeyChange Find Personal */
        txtEmployee_Find.addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                int k = e.getKeyCode();
                if (XLDL.checkChar(k)) {
                    textfind += e.getKeyText(k);
                }
                if (k == KeyEvent.VK_BACK_SPACE && textfind.length() > 0) {
                    textfind = textfind.substring(0, textfind.length() >= 1 ? textfind.length() - 1 : 1);
                }
                new TTNS().GetDataNS(tblStatusEmployees, textfind, rdoPersonal_EmplID.isSelected() ? "MaNV" : rdoPersonal_Position.isSelected() ? "MaCV" : rdoPersonal_Name.isSelected() ? "HoTen" : "CMND");
            }
        });

        /*
         Button invoiceIn
         */
 /* button INsert Invoice In*/
        btnInvoiceBought_Insert.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                HoaDonMua hdm = new HoaDonMua(txtInvoiceBought_ID1.getText(), txtInvoiceBought_ImplID.getText(), txtInvoiceBought_VendorID.getText(), txtInvoiceBought_DateCreated.getText(), txtInvoiceBought_TotalMoney.getText(), HoaDonMua.INSERT);
                new CTHDM(txtInvoiceBought_ID1.getText(), txtInvoiceBought_ProductID.getText(), txtInvoiceBought_Price.getText(), Integer.parseInt(txtInvoiceBought_Quantity.getText()), txtInvoiceBought_TotalMoney.getText(), CTHDM.INSERT);
                hdm.GetDataHDM(tblManageProducts_InvoicesIn);
            }
        });
        /* button Update Invoices IN*/
        btnInvoiceBought_Update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {

                HoaDonMua hdm = new HoaDonMua(txtInvoiceBought_ID1.getText(), txtInvoiceBought_ImplID.getText(), txtInvoiceBought_VendorID.getText(), txtInvoiceBought_DateCreated.getText(), txtInvoiceBought_TotalMoney.getText(), HoaDonMua.UPDATE);
                new CTHDM(txtInvoiceBought_ID1.getText(), txtInvoiceBought_ProductID.getText(), txtInvoiceBought_Price.getText(), Integer.parseInt(txtInvoiceBought_Quantity.getText()), txtInvoiceBought_TotalMoney.getText(), CTHDM.UPDATE);
                hdm.GetDataHDM(tblManageProducts_InvoicesIn);
            }
        });
        /* button Delete Invoice In */
        btnInvoiceBought_Delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                int index = tblManageProducts_InvoicesIn.getSelectedRow();
                if (index > -1) {
                    HoaDonMua hdm = new HoaDonMua(tblManageProducts_InvoicesIn.getValueAt(index, 0).toString(), HoaDonMua.DELETE);
                    new CTHDM(tblManageProducts_InvoicesIn.getValueAt(index, 0).toString(), CTHDM.DELETE);
                    hdm.GetDataHDM(tblManageProducts_InvoicesIn);
                } else {
                    HoaDonMua hdm = new HoaDonMua(txtInvoiceBought_ID1.getText(), HoaDonMua.DELETE);
                    new CTHDM(txtInvoiceBought_ID1.getText(), CTHDM.DELETE);
                    hdm.GetDataHDM(tblManageProducts_InvoicesIn);
                }
            }
        });
        /* button Find Invoices In */
        btnInvoiceBought_Find.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                new HoaDonMua().GetDataHDM(tblManageProducts_InvoicesIn, rdoInvoiceIn_EmplID.isSelected() ? "HoaDonMua" : rdoInvoiceIn_InvoiceID.isSelected() ? "CTHDM" : rdoInvoiceIn_ProductID.isSelected() ? "CTHDM" : "HoaDonMua",
                        txtInvoicebought_Find.getText(), rdoInvoiceIn_EmplID.isSelected() ? "MaNV" : rdoInvoiceIn_InvoiceID.isSelected() ? "MaHDM" : rdoInvoiceIn_ProductID.isSelected() ? "MaSP" : "MaNCC");

            }
        });
        /* Keychange Find Invoices In*/
        txtInvoicebought_Find.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                int k = e.getKeyCode();
                if (XLDL.checkChar(k)) {
                    textfind += e.getKeyText(k);
                }
                if (k == KeyEvent.VK_BACK_SPACE && textfind.length() > 0) {
                    textfind = textfind.substring(0, textfind.length() >= 1 ? textfind.length() - 1 : 1);
                }
                new HoaDonMua().GetDataHDM(tblManageProducts_InvoicesIn, rdoInvoiceIn_EmplID.isSelected() ? "HoaDonMua" : rdoInvoiceIn_InvoiceID.isSelected() ? "CTHDM" : rdoInvoiceIn_ProductID.isSelected() ? "CTHDM" : "HoaDonMua",
                        textfind, rdoInvoiceIn_EmplID.isSelected() ? "MaNV" : rdoInvoiceIn_InvoiceID.isSelected() ? "MaHDM" : rdoInvoiceIn_ProductID.isSelected() ? "MaSP" : "MaNCC");
            }
        });

        /*
         Button Invoice out
         */
 /* button Insert Invoices OUt*/
        btnInvoiceSold_Insert.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                HoaDonBan hdb = new HoaDonBan(txtInvoiceSold_ID.getText(), txtInvoiceSold_ImplID.getText(), txtInvoiceSold_DateCreated.getText(), txtInvoiceSold_TotalMoney.getText(), HoaDonBan.INSERT);
                new CTHDB(txtInvoiceSold_ID.getText(), txtInvoiceSold_ProductID.getText(), txtInvoiceSold_Price.getText(), Integer.parseInt(txtInvoiceSold_Quantity.getText()), txtInvoiceSold_TotalMoney.getText(), CTHDB.INSERT);
                hdb.GetDataHDB(tblInvoicesOut);
            }
        });
        /* button Update Invoices Out*/
        btnInvoiceSold_Update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                HoaDonBan hdb = new HoaDonBan(txtInvoiceSold_ID.getText(), txtInvoiceSold_ImplID.getText(), txtInvoiceSold_DateCreated.getText(), txtInvoiceSold_TotalMoney.getText(), HoaDonBan.UPDATE);
                new CTHDB(txtInvoiceSold_ID.getText(), txtInvoiceSold_ProductID.getText(), txtInvoiceSold_Price.getText(), Integer.parseInt(txtInvoiceSold_Quantity.getText()), txtInvoiceSold_TotalMoney.getText(), CTHDB.UPDATE);
                hdb.GetDataHDB(tblInvoicesOut);
            }
        });
        /* button Delete Invoices Out*/
        btnInvoiceSold_Delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                int index = tblInvoicesOut.getSelectedRow();
                if (index > -1) {
                    HoaDonBan hdb = new HoaDonBan(tblInvoicesOut.getValueAt(index, 0).toString(), HoaDonBan.DELETE);
                    new CTHDB(tblInvoicesOut.getValueAt(index, 0).toString(), CTHDM.DELETE);
                    hdb.GetDataHDB(tblInvoicesOut);
                } else {
                    HoaDonBan hdb = new HoaDonBan(txtInvoiceSold_ID.getText(), HoaDonBan.DELETE);
                    new CTHDB(txtInvoiceSold_ID.getText(), CTHDM.DELETE);
                    hdb.GetDataHDB(tblInvoicesOut);
                }
            }
        });

        /* button Find Invoices Out */
        btnInvoiceSold_Find.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                new HoaDonBan().GetDataHDB(tblInvoicesOut, rdoInvoiceOut_InvoiceID.isSelected() ? "CTHDB" : rdoInvoieOut_EmplID.isSelected() ? "HoaDonBan" : "CTHDB",
                        txtInvoiceSold_Find.getText(), rdoInvoiceOut_InvoiceID.isSelected() ? "MaHDB" : rdoInvoieOut_EmplID.isSelected() ? "MaNV" : "MaSP");
            }
        });
        /* Keychange Find Invoices Out*/
        txtInvoiceSold_Find.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                int k = e.getKeyCode();
                if (XLDL.checkChar(k)) {
                    textfind += e.getKeyText(k);
                }
                if (k == KeyEvent.VK_BACK_SPACE && textfind.length() > 0) {
                    textfind = textfind.substring(0, textfind.length() >= 1 ? textfind.length() - 1 : 1);
                }
                new HoaDonBan().GetDataHDB(tblInvoicesOut, rdoInvoiceOut_InvoiceID.isSelected() ? "CTHDB" : rdoInvoieOut_EmplID.isSelected() ? "HoaDonBan" : "CTHDB",
                        textfind, rdoInvoiceOut_InvoiceID.isSelected() ? "MaHDB" : rdoInvoieOut_EmplID.isSelected() ? "MaNV" : "MaSP");
            }
        });
        /*
         Button Product
         */

 /* button Insert Product */
        btnProduct_Insert.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                SanPham sp = new SanPham(txtProduct_ID.getText(), txtProduct_Name.getText(), txtProduct_VendorID.getText(), cbbProduct_Unit.getSelectedItem().toString(), txtProduct_Price.getText(), Integer.parseInt(txtProduct_Quantity.getText()), SanPham.INSERT);
                sp.GetDataSP(tblManageProducts_Product);
            }
        });

        /* button Insert Product*/
        btnProduct_Insert.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                SanPham sp = new SanPham(txtProduct_ID.getText(), txtProduct_Name.getText(), txtProduct_VendorID.getText(), cbbProduct_Unit.getSelectedItem().toString(), txtProduct_Price.getText(), Integer.parseInt(txtProduct_Quantity.getText()), SanPham.UPDATE);
                sp.GetDataSP(tblManageProducts_Product);
            }
        });

        /* button insert product*/
        btnProduct_Insert.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                int index = tblManageProducts_Product.getSelectedRow();
                if (index > -1) {
                    SanPham sp = new SanPham(tblManageProducts_Product.getValueAt(index, 0).toString(), SanPham.DELETE);
                    sp.GetDataSP(tblManageProducts_Product);
                } else {
                    SanPham sp = new SanPham(txtProduct_ID.getText(), SanPham.DELETE);
                    sp.GetDataSP(tblManageProducts_Product);
                }

            }
        });
        /* button Find Product */
        btnProduct_Find.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new SanPham().GetDataSP(tblManageProducts_Product, txtProduct_Find.getText(), rdoProduct_ProID.isSelected() ? "MaSP" : rdoProduct_Name.isSelected() ? "TenSP" : "MaNCC");
            }
        });
        /* KeyChange Find Product */
        txtProduct_Find.addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                int k = e.getKeyCode();
                if (XLDL.checkChar(k)) {
                    textfind += e.getKeyText(k);
                }
                if (k == KeyEvent.VK_BACK_SPACE && textfind.length() > 0) {
                    textfind = textfind.substring(0, textfind.length() >= 1 ? textfind.length() - 1 : 1);
                }
                new SanPham().GetDataSP(tblManageProducts_Product, textfind, rdoProduct_ProID.isSelected() ? "MaSP" : rdoProduct_Name.isSelected() ? "TenSP" : "MaNCC");
            }
        });

        /*
         Button Vendor
         */
 /* button Insert Vendor*/
        btnVendor_Insert.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                NhaCungCap ncc = new NhaCungCap(txtVendor_ID.getText(), txtVendor_Name.getText(), txtVendor_Address.getText(), NhaCungCap.INSERT);
                ncc.GetDataNCC(tblManageProducts_Vendors);
            }
        });
        /* button Update Vendor*/
        btnVendor_Update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                NhaCungCap ncc = new NhaCungCap(txtVendor_ID.getText(), txtVendor_Name.getText(), txtVendor_Address.getText(), NhaCungCap.UPDATE);
                ncc.GetDataNCC(tblManageProducts_Vendors);
            }
        });
        /* button Delete Vendor*/
        btnVendor_Delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                int index = tblManageProducts_Vendors.getSelectedRow();
                if (index > -1) {
                    NhaCungCap ncc = new NhaCungCap(tblManageProducts_Vendors.getValueAt(index, 0).toString(), NhaCungCap.DELETE);
                    ncc.GetDataNCC(tblManageProducts_Vendors);
                } else {
                    NhaCungCap ncc = new NhaCungCap(txtVendor_ID.getText(), NhaCungCap.DELETE);
                    ncc.GetDataNCC(tblManageProducts_Vendors);
                }

            }
        });
        /* button Find Vendor*/
        btnVendor_Find.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new NhaCungCap().GetDataNCC(tblManageProducts_Vendors, txtVendor_FInd.getText(), rdoVendor_VendorID.isSelected() ? "MaNCC" : rdoVendor_Name.isSelected() ? "TenNC" : "DiaChi");
            }
        });
        /* KeyChange Find Vendor*/
        txtVendor_FInd.addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                int k = e.getKeyCode();
                if (XLDL.checkChar(k)) {
                    textfind += e.getKeyText(k);
                }
                if (k == KeyEvent.VK_BACK_SPACE && textfind.length() > 0) {
                    textfind = textfind.substring(0, textfind.length() >= 1 ? textfind.length() - 1 : 1);
                }
                new NhaCungCap().GetDataNCC(tblManageProducts_Vendors, textfind, rdoVendor_VendorID.isSelected() ? "MaNCC" : rdoVendor_Name.isSelected() ? "TenNC" : "DiaChi");
            }
        });

        /*
         Button User
         */
 /* button Insert User*/
        btnUser_Insert.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                Users us = new Users(txtUser_EmplID.getText(), txtUser_Username.getText(), txtUser_Password.getText(), cbbUser_role.getSelectedItem().toString(), !Path.equals("") ? Path : "account.png", Users.INSERT);
                us.GetDataUsers(tblUser, cbbUser_role);
                Path = "";
            }
        });
        /* button Update User*/
        btnUser_Update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                Users us = new Users(txtUser_EmplID.getText(), txtUser_Username.getText(), txtUser_Password.getText(), cbbUser_role.getSelectedItem().toString(), !Path.equals("") ? Path : "account.png", Users.UPDATE);
                us.GetDataUsers(tblUser, cbbUser_role);
                Path = "";
            }
        });
        /* button Delete User*/
        btnUser_Delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                int index = tblUser.getSelectedRow();
                if (index > -1) {
                    Users us = new Users(tblUser.getValueAt(index, 0).toString(), Users.DELETE);
                    us.GetDataUsers(tblUser, cbbUser_role);
                } else {
                    Users us = new Users(txtUser_EmplID.getText(), Users.DELETE);
                    us.GetDataUsers(tblUser, cbbUser_role);
                }

            }
        });
        /* button Find User Login*/
        btnUser_Find.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Users().GetDataUsers(tblUser, txtUser_Find.getText(), rdoUser_EmplID.isSelected() ? "MaNV" : rdoRole.isSelected() ? "role" : "Username");
            }
        });
        /* KeyChange Find User Login*/
        txtUser_Find.addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                int k = e.getKeyCode();
                if (XLDL.checkChar(k)) {
                    textfind += e.getKeyText(k);
                }
                if (k == KeyEvent.VK_BACK_SPACE && textfind.length() > 0) {
                    textfind = textfind.substring(0, textfind.length() >= 1 ? textfind.length() - 1 : 1);
                }
                new Users().GetDataUsers(tblUser, textfind, rdoUser_EmplID.isSelected() ? "MaNV" : rdoRole.isSelected() ? "role" : "Username");
            }
        });

        /*
         Button Financial 
         */
 /* button Insert Financial*/
        btnFinacial_Insert.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                LuongNV lnv = new LuongNV(txtIFinalcial_PositionID.getText(), txtFinacial_Position.getText(), txtIFinacial_Salary.getText(), LuongNV.INSERT);
                lnv.GetDataLuongNV(tblManageFinacial);
            }
        });

        /* button Update Financial */
        btnFinacial_Update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                LuongNV lnv = new LuongNV(txtIFinalcial_PositionID.getText(), txtFinacial_Position.getText(), txtIFinacial_Salary.getText(), LuongNV.UPDATE);
                lnv.GetDataLuongNV(tblManageFinacial);
            }
        });
        /* button Delete Financial*/
        btnFinacial_Delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                int index = tblManageFinacial.getSelectedRow();
                if (index > -1) {
                    LuongNV lnv = new LuongNV(tblManageFinacial.getValueAt(index, 0).toString(), LuongNV.DELETE);
                    lnv.GetDataLuongNV(tblManageFinacial);
                } else {
                    LuongNV lnv = new LuongNV(txtIFinalcial_PositionID.getText(), LuongNV.DELETE);
                    lnv.GetDataLuongNV(tblManageFinacial);
                }

            }
        });
        /* button Find Financial */
        btnFinacial_Find.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new LuongNV().GetDataLuongNV(tblManageFinacial, txtFianancial_Find.getText(), rdoFinancial_PosiID.isSelected() ? "MaCV" : "ChucVu");
            }
        });
        /* KeyChange Find  Financial */
        txtFianancial_Find.addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                int k = e.getKeyCode();
                if (XLDL.checkChar(k)) {
                    textfind += e.getKeyText(k);
                }
                if (k == KeyEvent.VK_BACK_SPACE && textfind.length() > 0) {
                    textfind = textfind.substring(0, textfind.length() >= 1 ? textfind.length() - 1 : 1);
                }
                new LuongNV().GetDataLuongNV(tblManageFinacial, textfind, rdoFinancial_PosiID.isSelected() ? "MaCV" : "ChucVu");
            }
        });
        /*-----------------------------Các nút Navigator ---------------------------------------------------*/

 /* Navigator Manage Employee Invoice Sold */
        btnInvoiceSold_Next.addActionListener(new ActionListener() {  // Điều hướng Next
            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblInvoicesOut, btnInvoiceSold_Next, index);
                index++;
                if (index >= tblInvoicesOut.getRowCount()) {
                    index = tblInvoicesOut.getRowCount() - 1;
                }
                Navigation.showInfor_InvoicesOut(tblInvoicesOut, txtInvoiceSold_ID, txtInvoiceSold_ImplID, txtInvoiceSold_DateCreated, txtInvoiceSold_ProductID, txtInvoiceSold_Price, txtInvoiceSold_Quantity, txtInvoiceSold_TotalMoney, index);

            }
        });

        btnInvoiceSold_Pre.addActionListener(new ActionListener() {   // Điều hướng Previous

            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblInvoicesOut, btnInvoiceSold_Pre, index);
                index--;
                if (index < 0) {
                    index = 0;
                }
                Navigation.showInfor_InvoicesOut(tblInvoicesOut, txtInvoiceSold_ID, txtInvoiceSold_ImplID, txtInvoiceSold_DateCreated, txtInvoiceSold_ProductID, txtInvoiceSold_Price, txtInvoiceSold_Quantity, txtInvoiceSold_TotalMoney, index);
            }
        });
        btnInvoiceSold_Top.addActionListener(new ActionListener() {   // Điều hướng First

            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblInvoicesOut, btnInvoiceSold_Top, index);
                index = 0;
                Navigation.showInfor_InvoicesOut(tblInvoicesOut, txtInvoiceSold_ID, txtInvoiceSold_ImplID, txtInvoiceSold_DateCreated, txtInvoiceSold_ProductID, txtInvoiceSold_Price, txtInvoiceSold_Quantity, txtInvoiceSold_TotalMoney, index);
            }
        });
        btnInvoiceSold_Bottom.addActionListener(new ActionListener() {  // Điều hướng Last

            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblInvoicesOut, btnInvoiceSold_Bottom, index);
                index = tblInvoicesOut.getRowCount() - 1;
                Navigation.showInfor_InvoicesOut(tblInvoicesOut, txtInvoiceSold_ID, txtInvoiceSold_ImplID, txtInvoiceSold_DateCreated, txtInvoiceSold_ProductID, txtInvoiceSold_Price, txtInvoiceSold_Quantity, txtInvoiceSold_TotalMoney, index);
            }
        });
        /* Navigator Manage Employees User Login*/
        btnUser_Next.addActionListener(new ActionListener() {    // Điều hướng Next
            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblUser, btnUser_Next, index);
                index++;
                if (index >= tblUser.getRowCount()) {
                    index = tblUser.getRowCount() - 1;
                }
                Navigation.showInfor_UserLogin(tblUser, txtUser_EmplID, txtUser_Username, txtUser_Password, txtUser_Confirm, cbbUser_role, lblIconUser, index);
            }
        });
        btnUser_Pre.addActionListener(new ActionListener() {    // Điều hướng Previous
            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblUser, btnUser_Pre, index);
                index--;
                if (index < 0) {
                    index = 0;
                }
                Navigation.showInfor_UserLogin(tblUser, txtUser_EmplID, txtUser_Username, txtUser_Password, txtUser_Confirm, cbbUser_role, lblIconUser, index);
            }
        });
        btnUser_Top.addActionListener(new ActionListener() {   // Điều hướng First
            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblUser, btnUser_Top, index);
                index = 0;
                Navigation.showInfor_UserLogin(tblUser, txtUser_EmplID, txtUser_Username, txtUser_Password, txtUser_Confirm, cbbUser_role, lblIconUser, index);
            }
        });
        btnUser_Bottom.addActionListener(new ActionListener() {         // Điều hướng Last
            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblUser, btnUser_Bottom, index);
                index = tblUser.getRowCount() - 1;
                Navigation.showInfor_UserLogin(tblUser, txtUser_EmplID, txtUser_Username, txtUser_Password, txtUser_Confirm, cbbUser_role, lblIconUser, index);
            }
        });

        /*Navigator Manage Employees Persional*/
        btnEmployee_Next.addActionListener(new ActionListener() {         // Điều hướng Next
            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblStatusEmployees, btnEmployee_Next, index);
                index++;
                if (index >= tblStatusEmployees.getRowCount()) {
                    index = tblStatusEmployees.getRowCount() - 1;
                }
                Navigation.showInfor_persional(tblStatusEmployees, txtEmployee_ID, txtEmployee_Name, txtEployee_PersonalID, txtEmployee_Address, cbbEmployee_Position, lblEmloyee_Avatar, index);
            }
        });
        btnEmployee_Pre.addActionListener(new ActionListener() {         // Điều hướng Previous
            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblStatusEmployees, btnEmployee_Pre, index);
                index--;
                if (index < 0) {
                    index = 0;
                }
                Navigation.showInfor_persional(tblStatusEmployees, txtEmployee_ID, txtEmployee_Name, txtEployee_PersonalID, txtEmployee_Address, cbbEmployee_Position, lblEmloyee_Avatar, index);
            }
        });
        btnEmployee_Top.addActionListener(new ActionListener() {         // Điều hướng First
            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblStatusEmployees, btnEmployee_Top, index);
                index = 0;
                Navigation.showInfor_persional(tblStatusEmployees, txtEmployee_ID, txtEmployee_Name, txtEployee_PersonalID, txtEmployee_Address, cbbEmployee_Position, lblEmloyee_Avatar, index);
            }
        });
        btnEmployee_Bottom.addActionListener(new ActionListener() {         // Điều hướng Last
            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblStatusEmployees, btnEmployee_Bottom, index);
                index = tblStatusEmployees.getRowCount() - 1;
                Navigation.showInfor_persional(tblStatusEmployees, txtEmployee_ID, txtEmployee_Name, txtEployee_PersonalID, txtEmployee_Address, cbbEmployee_Position, lblEmloyee_Avatar, index);
            }
        });

        /*Navigator Products Vendors*/
        btnVendor_Next.addActionListener(new ActionListener() {         // Điều hướng Next
            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblManageProducts_Vendors, btnVendor_Next, index);
                index++;
                if (index >= tblManageProducts_Vendors.getRowCount()) {
                    index = tblManageProducts_Vendors.getRowCount() - 1;
                }
                Navigation.showInfor_vendors(tblManageProducts_Vendors, txtVendor_ID, txtVendor_Name, txtVendor_Address, index);
            }
        });
        btnVendor_Pre.addActionListener(new ActionListener() {         // Điều hướng Previous
            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblManageProducts_Vendors, btnVendor_Pre, index);
                index--;
                if (index < 0) {
                    index = 0;
                }
                Navigation.showInfor_vendors(tblManageProducts_Vendors, txtVendor_ID, txtVendor_Name, txtVendor_Address, index);
            }
        });
        btnVendor_Top.addActionListener(new ActionListener() {         // Điều hướng First
            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblManageProducts_Vendors, btnVendor_Top, index);
                index = 0;
                Navigation.showInfor_vendors(tblManageProducts_Vendors, txtVendor_ID, txtVendor_Name, txtVendor_Address, index);
            }

        });
        btnVendor_Bottom.addActionListener(new ActionListener() {         // Điều hướng Last
            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblManageProducts_Vendors, btnVendor_Bottom, index);
                index = tblManageProducts_Vendors.getRowCount() - 1;
                Navigation.showInfor_vendors(tblManageProducts_Vendors, txtVendor_ID, txtVendor_Name, txtVendor_Address, index);
            }
        });

        /*  Navigator Manage Products Product  */
        btnProduct_Next.addActionListener(new ActionListener() {         // Điều hướng Next
            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblManageProducts_Product, btnProduct_Next, index);
                index++;
                if (index >= tblManageProducts_Product.getRowCount()) {
                    index = tblManageProducts_Product.getRowCount() - 1;
                }
                Navigation.showInfor_products(tblManageProducts_Product, txtProduct_ID, txtProduct_VendorID, txtProduct_Name, txtProduct_Price, txtProduct_Quantity, cbbProduct_Unit, index);
            }
        });
        btnProduct_Pre.addActionListener(new ActionListener() {         // Điều hướng Previous
            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblManageProducts_Product, btnProduct_Pre, index);
                index--;
                if (index < 0) {
                    index = 0;
                }
                Navigation.showInfor_products(tblManageProducts_Product, txtProduct_ID, txtProduct_VendorID, txtProduct_Name, txtProduct_Price, txtProduct_Quantity, cbbProduct_Unit, index);
            }
        });
        btnProduct_Top.addActionListener(new ActionListener() {         // Điều hướng First
            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblManageProducts_Product, btnProduct_Top, index);
                index = 0;
                Navigation.showInfor_products(tblManageProducts_Product, txtProduct_ID, txtProduct_VendorID, txtProduct_Name, txtProduct_Price, txtProduct_Quantity, cbbProduct_Unit, index);
            }
        });
        btnProduct_Bottom.addActionListener(new ActionListener() {         // Điều hướng Last
            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblManageProducts_Product, btnProduct_Bottom, index);
                index = tblManageProducts_Product.getRowCount() - 1;
                Navigation.showInfor_products(tblManageProducts_Product, txtProduct_ID, txtProduct_VendorID, txtProduct_Name, txtProduct_Price, txtProduct_Quantity, cbbProduct_Unit, index);
            }
        });

        /* Navigator Manage Products Invoices In */
        btnInvoiceBought_Next.addActionListener(new ActionListener() {         // Điều hướng Next
            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblManageProducts_InvoicesIn, btnInvoiceBought_Next, index);
                index++;
                if (index >= tblManageProducts_InvoicesIn.getRowCount()) {
                    index = tblManageProducts_InvoicesIn.getRowCount() - 1;
                }
                Navigation.showInfor_InvoicesIn(tblManageProducts_InvoicesIn, txtInvoiceBought_ID1, txtInvoiceBought_ImplID, txtInvoiceBought_DateCreated, txtInvoiceBought_Quantity, txtInvoiceBought_TotalMoney, txtInvoiceBought_VendorID, txtInvoiceBought_ProductID, txtInvoiceBought_Price, index);
            }
        });
        btnInvoiceBought_Pre.addActionListener(new ActionListener() {         // Điều hướng Previous
            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblManageProducts_InvoicesIn, btnInvoiceBought_Pre, index);
                index--;
                if (index < 0) {
                    index = 0;
                }
                Navigation.showInfor_InvoicesIn(tblManageProducts_InvoicesIn, txtInvoiceBought_ID1, txtInvoiceBought_ImplID, txtInvoiceBought_DateCreated, txtInvoiceBought_Quantity, txtInvoiceBought_TotalMoney, txtInvoiceBought_VendorID, txtInvoiceBought_ProductID, txtInvoiceBought_Price, index);
            }
        });
        btnInvoiceBought_Top.addActionListener(new ActionListener() {         // Điều hướng First
            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblManageProducts_InvoicesIn, btnInvoiceBought_Top, index);
                index = 0;
                Navigation.showInfor_InvoicesIn(tblManageProducts_InvoicesIn, txtInvoiceBought_ID1, txtInvoiceBought_ImplID, txtInvoiceBought_DateCreated, txtInvoiceBought_Quantity, txtInvoiceBought_TotalMoney, txtInvoiceBought_VendorID, txtInvoiceBought_ProductID, txtInvoiceBought_Price, index);
            }
        });
        btnInvoiceBought_Bottom.addActionListener(new ActionListener() {         // Điều hướng Last
            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblManageProducts_InvoicesIn, btnInvoiceBought_Bottom, index);
                index = tblManageProducts_InvoicesIn.getRowCount() - 1;
                Navigation.showInfor_InvoicesIn(tblManageProducts_InvoicesIn, txtInvoiceBought_ID1, txtInvoiceBought_ImplID, txtInvoiceBought_DateCreated, txtInvoiceBought_Quantity, txtInvoiceBought_TotalMoney, txtInvoiceBought_VendorID, txtInvoiceBought_ProductID, txtInvoiceBought_Price, index);
            }
        });

        /* Navigator Fianacial */
        btnFinacial_Next.addActionListener(new ActionListener() {         // Điều hướng Next
            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblManageFinacial, btnFinacial_Next, index);
                index++;
                if (index >= tblManageFinacial.getRowCount()) {
                    index = tblManageFinacial.getRowCount() - 1;
                }
                Navigation.showInfor_Financial(tblManageFinacial, txtIFinalcial_PositionID, txtFinacial_Position, txtIFinacial_Salary, index);
            }
        });
        btnFinacial_Pre.addActionListener(new ActionListener() {         // Điều hướng Previous
            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblManageFinacial, btnFinacial_Pre, index);
                index--;
                if (index < 0) {
                    index = 0;
                }
                Navigation.showInfor_Financial(tblManageFinacial, txtIFinalcial_PositionID, txtFinacial_Position, txtIFinacial_Salary, index);
            }
        });
        btnFinacial_Top.addActionListener(new ActionListener() {         // Điều hướng First
            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblManageFinacial, btnFinacial_Top, index);
                index = 0;
                Navigation.showInfor_Financial(tblManageFinacial, txtIFinalcial_PositionID, txtFinacial_Position, txtIFinacial_Salary, index);
            }
        });
        btnFinacial_Bottom.addActionListener(new ActionListener() {         // Điều hướng Last
            @Override
            public void actionPerformed(ActionEvent e) {
                Navigation.Navigator(tblManageFinacial, btnFinacial_Bottom, index);
                index = tblManageFinacial.getRowCount() - 1;
                Navigation.showInfor_Financial(tblManageFinacial, txtIFinalcial_PositionID, txtFinacial_Position, txtIFinacial_Salary, index);
            }
        });

    }

    private void showchartall() {
        rdbStatictis_Finacial_All.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pnlStatictis_Chart.removeAll();
                pnlStatictis_Chart.repaint();
                arr = Statictis_DAO.StatictisData(tblStatictis_Finacial_report, Statictis_DAO.all, Statictis_DAO.LN);
                value = new Object[arr.size()];
                name = new Object[arr.size()];
                for (int i = 0; i < arr.size(); i++) {
                    value[i] = arr.get(i).getFN().getMoney();
                    name[i] = arr.get(i).getFN().getMonth();
                }
                chart = new Chart(pnlStatictis_Chart.getWidth(), pnlStatictis_Chart.getHeight(), "Finacial", "Month", "USD", "Month", name, value);
                pnlStatictis_Chart.add(chart);

            }
        });
        rdbStatictis_InvoiceIn_All.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pnlStatictis_Chart.removeAll();
                pnlStatictis_Chart.repaint();
                arr = Statictis_DAO.StatictisData(tblStatictis_InvoiceIn, Statictis_DAO.all, Statictis_DAO.DT);
                value = new Object[arr.size()];
                name = new Object[arr.size()];
                for (int i = 0; i < arr.size(); i++) {
                    value[i] = arr.get(i).getII().getMoney();
                    name[i] = arr.get(i).getII().getMonth();
                }
                chart = new Chart(pnlStatictis_Chart.getWidth(), pnlStatictis_Chart.getHeight(), "Invoice In Statictis", "Month", "USD", "Month", name, value);
                pnlStatictis_Chart.add(chart);

            }
        });
        rdbStatictis_InvoiceOut_All.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pnlStatictis_Chart.removeAll();
                pnlStatictis_Chart.repaint();
                arr = Statictis_DAO.StatictisData(tblStatictis_InvoiceOut, Statictis_DAO.all, Statictis_DAO.CT);
                value = new Object[arr.size()];
                name = new Object[arr.size()];
                for (int i = 0; i < arr.size(); i++) {
                    value[i] = arr.get(i).getIO().getMoney();
                    name[i] = arr.get(i).getIO().getMonth();
                }
                chart = new Chart(pnlStatictis_Chart.getWidth(), pnlStatictis_Chart.getHeight(), "Invoice Out Statictis", "Month", "USD", "Month", name, value);
                pnlStatictis_Chart.add(chart);
            }
        });
    }

    private void cbbaction() {
        cbbStatictis_Finacial.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                int month = cbbStatictis_Finacial.getSelectedIndex() + 1;
                Statictis_DAO.Statictis(tblStatictis_Finacial_report, month, Statictis_DAO.LN);
            }
        });
        cbbStatictis_InvoiceIn.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                int month = cbbStatictis_InvoiceIn.getSelectedIndex() + 1;
                Statictis_DAO.Statictis(tblStatictis_InvoiceIn, month, Statictis_DAO.DT);
                pnlStatictis_Chart.removeAll();
                pnlStatictis_Chart.repaint();
                arr = Statictis_DAO.StatictisData(tblStatictis_InvoiceIn, month, Statictis_DAO.DT);
                value = new Object[arr.size()];
                name = new Object[arr.size()];
                for (int i = 0; i < arr.size(); i++) {
                    value[i] = arr.get(i).getII().getMoney();
                    name[i] = arr.get(i).getII().getProduct();
                }
                chart = new Chart(pnlStatictis_Chart.getWidth(), pnlStatictis_Chart.getHeight(), "Invoice January", "Product", "USD", "Product", name, value);
                pnlStatictis_Chart.add(chart);
            }
        });
        cbbStatictis_InvoiceOutl.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                int month = cbbStatictis_InvoiceOutl.getSelectedIndex() + 1;
                Statictis_DAO.Statictis(tblStatictis_InvoiceOut, month, Statictis_DAO.CT);
                pnlStatictis_Chart.removeAll();
                pnlStatictis_Chart.repaint();
                arr = Statictis_DAO.StatictisData(tblStatictis_InvoiceIn, month, Statictis_DAO.CT);
                value = new Object[arr.size()];
                name = new Object[arr.size()];
                for (int i = 0; i < arr.size(); i++) {
                    value[i] = arr.get(i).getIO().getMoney();
                    name[i] = arr.get(i).getIO().getInvoiceID();
                }
                chart = new Chart(pnlStatictis_Chart.getWidth(), pnlStatictis_Chart.getHeight(), "Invoice January", "Invoice ID", "USD", "Invoice ID", name, value);
                pnlStatictis_Chart.add(chart);
            }
        });
    }

    private void showchartmonth() {

        rdbStatictis_Finalcial_Month.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Statictis_DAO.Statictis(tblStatictis_Finacial_report, Statictis_DAO.T1, Statictis_DAO.LN);

            }
        });
        rdbStatictis_InvoiceIn_Month.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Statictis_DAO.Statictis(tblStatictis_InvoiceIn, Statictis_DAO.T1, Statictis_DAO.DT);
                pnlStatictis_Chart.removeAll();
                pnlStatictis_Chart.repaint();
                arr = Statictis_DAO.StatictisData(tblStatictis_InvoiceIn, Statictis_DAO.T1, Statictis_DAO.DT);
                value = new Object[arr.size()];
                name = new Object[arr.size()];
                for (int i = 0; i < arr.size(); i++) {
                    value[i] = arr.get(i).getII().getMoney();
                    name[i] = arr.get(i).getII().getProduct();
                }
                chart = new Chart(pnlStatictis_Chart.getWidth(), pnlStatictis_Chart.getHeight(), "Invoice January", "Product", "USD", "Product", name, value);
                pnlStatictis_Chart.add(chart);
            }
        });
        rdbStatictis_InvoiceOut_Month.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Statictis_DAO.Statictis(tblStatictis_InvoiceOut, Statictis_DAO.T1, Statictis_DAO.CT);
                pnlStatictis_Chart.removeAll();
                pnlStatictis_Chart.repaint();
                arr = Statictis_DAO.StatictisData(tblStatictis_InvoiceIn, Statictis_DAO.T1, Statictis_DAO.CT);
                value = new Object[arr.size()];
                name = new Object[arr.size()];
                for (int i = 0; i < arr.size(); i++) {
                    value[i] = arr.get(i).getIO().getMoney();
                    name[i] = arr.get(i).getIO().getInvoiceID();
                }
                chart = new Chart(pnlStatictis_Chart.getWidth(), pnlStatictis_Chart.getHeight(), "Invoice January", "Invoice ID", "USD", "Invoice ID", name, value);
                pnlStatictis_Chart.add(chart);
            }
        });

    }

    private void showchart() {
        /*
            filldata on table all
         */
        Statictis_DAO.Statictis(tblStatictis_Finacial_report, Statictis_DAO.all, Statictis_DAO.LN);
        Statictis_DAO.Statictis(tblStatictis_InvoiceIn, Statictis_DAO.all, Statictis_DAO.DT);
        Statictis_DAO.Statictis(tblStatictis_InvoiceOut, Statictis_DAO.all, Statictis_DAO.CT);
        /*
            draw chart all
         */
        arr = Statictis_DAO.StatictisData(tblStatictis_InvoiceOut, Statictis_DAO.all, Statictis_DAO.CT);
        value = new Object[arr.size()];
        name = new Object[arr.size()];
        for (int i = 0; i < arr.size(); i++) {
            value[i] = arr.get(i).getIO().getMoney();
            name[i] = arr.get(i).getIO().getMonth();
        }
        chart = new Chart(pnlStatictis_Chart.getWidth(), pnlStatictis_Chart.getHeight(), "Invoice Out Statictis", "Month", "USD", "Month", name, value);
        pnlStatictis_Chart.add(chart);
        pnlStatictis_Table.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent ce) {
                int n = pnlStatictis_Table.getSelectedIndex();
                switch (n) {
                    case 0:
                        pnlStatictis_Chart.removeAll();
                        pnlStatictis_Chart.repaint();
                        arr = Statictis_DAO.StatictisData(tblStatictis_InvoiceOut, Statictis_DAO.all, Statictis_DAO.CT);
                        value = new Object[arr.size()];
                        name = new Object[arr.size()];
                        for (int i = 0; i < arr.size(); i++) {
                            value[i] = arr.get(i).getIO().getMoney();
                            name[i] = arr.get(i).getIO().getMonth();
                        }
                        chart = new Chart(pnlStatictis_Chart.getWidth(), pnlStatictis_Chart.getHeight(), "Invoice Out Statictis", "Month", "USD", "Month", name, value);
                        pnlStatictis_Chart.add(chart);
                        break;
                    case 1:
                        pnlStatictis_Chart.removeAll();
                        pnlStatictis_Chart.repaint();
                        arr = Statictis_DAO.StatictisData(tblStatictis_InvoiceIn, Statictis_DAO.all, Statictis_DAO.DT);
                        value = new Object[arr.size()];
                        name = new Object[arr.size()];
                        for (int i = 0; i < arr.size(); i++) {
                            value[i] = arr.get(i).getII().getMoney();
                            name[i] = arr.get(i).getII().getMonth();
                        }
                        chart = new Chart(pnlStatictis_Chart.getWidth(), pnlStatictis_Chart.getHeight(), "Invoice In Statictis", "Month", "USD", "Month", name, value);
                        pnlStatictis_Chart.add(chart);
                        break;
                    case 2:
                        pnlStatictis_Chart.removeAll();
                        pnlStatictis_Chart.repaint();
                        arr = Statictis_DAO.StatictisData(tblStatictis_Finacial_report, Statictis_DAO.all, Statictis_DAO.LN);
                        value = new Object[arr.size()];
                        name = new Object[arr.size()];
                        for (int i = 0; i < arr.size(); i++) {
                            value[i] = arr.get(i).getFN().getMoney();
                            name[i] = arr.get(i).getFN().getMonth();
                        }
                        chart = new Chart(pnlStatictis_Chart.getWidth(), pnlStatictis_Chart.getHeight(), "Finacial", "Month", "USD", "Month", name, value);
                        pnlStatictis_Chart.add(chart);
                        break;
                }
            }
        });
    }

    private void Edit() {

        lblFogot.requestFocus();
        FrameDragListener fr = new FrameDragListener(this);
        this.addMouseListener(fr);
        this.addMouseMotionListener(fr);
        Image i = Toolkit.getDefaultToolkit().getImage("src/Icon/fpt.png");
        setIconImage(i);
        lblMedia.setLocation(lblMedia.getX() - lblMedia.getWidth() - 10, lblMedia.getY());
        lblWeb.setLocation(lblWeb.getX() - lblWeb.getWidth() - 10, lblWeb.getY());
        lblMenu_System.setLocation(lblMenu_System.getX(), -lblMenu_System.getHeight());
        lblMenu_Statictis.setLocation(lblMenu_Statictis.getX(), -lblMenu_Statictis.getHeight());
        lblMenu_Manage.setLocation(lblMenu_Manage.getX(), -lblMenu_Manage.getHeight());
        lblMenu_Help.setLocation(lblMenu_Help.getX(), -lblMenu_Help.getHeight());
        lblMenu_About.setLocation(lblMenu_About.getX(), -lblMenu_About.getHeight());
        pnlMenu_System.setLocation(-pnlMenu_System.getWidth(), pnlMenu_System.getY());
        lblManageEmployee_Switch.setVisible(false);

    }

    private void GetTableData() {
        new LuongNV().GetDataLuongNV(tblManageFinacial);
        new HoaDonBan().GetDataHDB(tblInvoicesOut);
        new HoaDonMua().GetDataHDM(tblManageProducts_InvoicesIn);
        new SanPham().GetDataSP(tblManageProducts_Product, cbbProduct_Unit);
        new TTNS().GetDataNS(tblStatusEmployees, cbbEmployee_Position);
        new NhaCungCap().GetDataNCC(tblManageProducts_Vendors);
        new Users().GetDataUsers(tblUser, cbbUser_role);
    }

    private void CheckLength() {
        LimitText.SetLimit(txtIFinalcial_PositionID, 2);
        LimitText.SetLimit(txtFinacial_Position, 30);
        LimitText.SetLimit(txtInvoiceBought_ID1, 6);
        LimitText.SetLimit(txtInvoiceBought_ImplID, 5);
        LimitText.SetLimit(txtInvoiceBought_ProductID, 5);
        LimitText.SetLimit(txtInvoiceBought_VendorID, 5);
        LimitText.SetLimit(txtProduct_ID, 5);
        LimitText.SetLimit(txtProduct_VendorID, 5);
        LimitText.SetLimit(txtProduct_Name, 30);
        LimitText.SetLimit(txtVendor_Name, 30);
        LimitText.SetLimit(txtVendor_ID, 5);
        LimitText.SetLimit(txtEmployee_ID, 5);
        LimitText.SetLimit(txtEployee_PersonalID, 9);
        LimitText.SetLimit(txtEmployee_Name, 30);
        LimitText.SetLimit(txtUser_Username, 20);
        LimitText.SetLimit(txtUser_Password, 15);
        LimitText.SetLimit(txtUser_Confirm, 15);
        LimitText.SetLimit(txtUser_EmplID, 5);
        LimitText.SetLimit(txtInvoiceSold_ID, 6);
        LimitText.SetLimit(txtInvoiceSold_ImplID, 5);
        LimitText.SetLimit(txtInvoiceSold_ProductID, 5);

    }

    private void ShowWorks() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = pnlMenuUser.getX(); i <= 0; i += 10) {
                        Thread.sleep(5);
                        pnlMenuUser.setLocation(i, pnlMenuUser.getY());
                        pnlMenuMain.setLocation(pnlMenuUser.getX() + pnlMenuUser.getWidth(), pnlMenuMain.getY());
                    }
                } catch (Exception e) {
                }
            }
        });
        t.start();
    }

    private void LabelClick() {
        mitFinacial.addActionListener(new Action(Action.FINACIAL));
        mitInvoidInMonth.addActionListener(new Action(Action.CHART));
        mitInvoicesIn.addActionListener(new Action(Action.INVOICESIN));
        mitInvoicesOut.addActionListener(new Action(Action.INVOICESOUT));
        mitProduct.addActionListener(new Action(Action.PRODUCTS));
        mitStatus.addActionListener(new Action(Action.INFOMATION));
        mitUser.addActionListener(new Action(Action.USERS));
        mitVendors.addActionListener(new Action(Action.VENDOR));
        mitchangeColorBaclground.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                JPanel[] p = new JPanel[]{pnlManageEmployees_InfomationController, pnlManageEmployees_InvoicesOutController, pnlManageProducts_InvoicesInController, pnlManageProducts_ProductController, pnlManageProducts_VendorsController, pnlManageProducts_VendorsController};
                ColorHelper.PanelColor(p);
            }
        });
        mitChangeColorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {

                JButton b[] = new JButton[]{btnEmloyee_Find,
                    btnProduct_Find,
                    btnEmploye_New,
                    btnEmployee_Bottom,
                    btnEmployee_Delete,
                    btnEmployee_Insert,
                    btnEmployee_Next,
                    btnEmployee_Pre,
                    btnEmployee_Top,
                    btnEmployee_Update,
                    btnProduct_Insert,
                    btnInvoiceBought_Bottom,
                    btnInvoiceBought_Delete,
                    btnInvoiceBought_Find,
                    btnInvoiceBought_Insert,
                    btnInvoiceBought_New,
                    btnInvoiceBought_Next,
                    btnInvoiceBought_Pre,
                    btnInvoiceBought_Top,
                    btnInvoiceBought_Update,
                    btnInvoiceSold_Bottom,
                    btnInvoiceSold_Delete,
                    btnInvoiceSold_Find,
                    btnInvoiceSold_Insert,
                    btnInvoiceSold_New,
                    btnInvoiceSold_Next,
                    btnInvoiceSold_Pre,
                    btnInvoiceSold_Top,
                    btnInvoiceSold_Update,
                    btnProduct_Bottom,
                    btnProduct_Delete,
                    btnProduct_New,
                    btnProduct_Next,
                    btnProduct_Pre,
                    btnProduct_Top,
                    btnProduct_Update,
                    btnUser_Bottom,
                    btnUser_Delete,
                    btnUser_Find,
                    btnUser_Insert,
                    btnUser_New,
                    btnUser_Next,
                    btnUser_Pre,
                    btnUser_Top,
                    btnUser_Update,
                    btnVendor_Bottom,
                    btnVendor_Delete,
                    btnVendor_Find,
                    btnVendor_Insert,
                    btnVendor_New,
                    btnVendor_Next,
                    btnVendor_Pre,
                    btnVendor_Top,
                    btnVendor_Update};
                ColorHelper.ButtonColor(b);
            }
        });
        lblEmloyee_Avatar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                Path = SolveIMG.OpenIMG(lblEmloyee_Avatar);
            }
        });
        lblUser_Avatar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                Path = SolveIMG.OpenIMG(lblUser_Avatar);
            }
        });
        pnlManageEmployees_ChoicesMain.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent me) {
                super.mouseMoved(me); //To change body of generated methods, choose Tools | Templates.
                Point p = me.getPoint();
                boolean kt1 = false;
                boolean kt2 = false;
                if (p.x >= lblManageEmployee_Switch.getX() && p.x <= lblManageEmployee_Switch.getWidth() + lblManageEmployee_Switch.getX()) {
                    kt1 = true;
                } else {
                    kt1 = false;
                }
                if (p.y >= lblManageEmployee_Switch.getY() && p.y <= lblManageEmployee_Switch.getHeight() + lblManageEmployee_Switch.getY()) {
                    kt2 = true;
                } else {
                    kt2 = false;
                }
                if (kt1 && kt2) {
                    lblManageEmployee_Switch.setVisible(true);
                } else {
                    lblManageEmployee_Switch.setVisible(false);
                }
            }
        });
        pnlManageProducts_ChoicesMain.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent me) {
                super.mouseMoved(me); //To change body of generated methods, choose Tools | Templates.
                Point p = me.getPoint();
                boolean kt1 = false;
                boolean kt2 = false;
                if (p.x >= lblManageProducts_Switch.getX() && p.x <= lblManageProducts_Switch.getWidth() + lblManageProducts_Switch.getX()) {
                    kt1 = true;
                } else {
                    kt1 = false;
                }
                if (p.y >= lblManageProducts_Switch.getY() && p.y <= lblManageProducts_Switch.getHeight() + lblManageProducts_Switch.getY()) {
                    kt2 = true;
                } else {
                    kt2 = false;
                }
                if (kt1 && kt2) {
                    lblManageProducts_Switch.setVisible(true);
                } else {
                    lblManageProducts_Switch.setVisible(false);
                }
            }
        });
        lblManageProducts_Switch.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                super.mouseClicked(me); //To change body of generated methods, choose Tools | Templates.
                if (checkManageProducts) {
                    XLICon.getBackgroundIcon("src/Icon/holderClose.png", lblManageProducts_Switch, lblManageProducts_Switch.getWidth(), lblManageProducts_Switch.getHeight());
                    Thread close = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                for (int i = pnlManageProducts_Choices.getX(); i >= -pnlManageProducts_Choices.getWidth(); i -= 10) {
                                    pnlManageProducts_ChoicesMain.setLocation(i, pnlManageProducts_ChoicesMain.getY());
                                    pnlManageProducts_View.setLocation(pnlManageProducts_ChoicesMain.getX() + pnlManageProducts_ChoicesMain.getWidth() - (pnlManageProducts_ChoicesMain.getWidth() - pnlManageProducts_Choices.getWidth() + pnlManageProducts_Choices.getX()), pnlManageProducts_View.getY());
                                    pnlManageProducts_View.setSize(pnlManageProducts_View.getWidth() + (pnlManageProducts_View.getWidth() <= f.getWidth() ? 9 : 0), pnlManageProducts_View.getHeight());
                                    Thread.sleep(5);
                                }
                                pnlManageProducts_View.setVisible(false);
                                pnlManageProducts_View.setVisible(true);
                            } catch (Exception e) {
                            }
                        }
                    });
                    close.start();
                    checkManageProducts = false;
                } else {
                    XLICon.getBackgroundIcon("src/Icon/holderOpen.png", lblManageProducts_Switch, lblManageProducts_Switch.getWidth(), lblManageProducts_Switch.getHeight());
                    Thread close = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                for (int i = -pnlManageProducts_Choices.getWidth(); i <= 0; i += 10) {
                                    pnlManageProducts_ChoicesMain.setLocation(i, pnlManageProducts_ChoicesMain.getY());
                                    pnlManageProducts_View.setLocation(pnlManageProducts_ChoicesMain.getX() + pnlManageProducts_ChoicesMain.getWidth() - (pnlManageProducts_ChoicesMain.getWidth() - pnlManageProducts_Choices.getWidth() + pnlManageProducts_Choices.getX()), pnlManageProducts_View.getY());
                                    pnlManageProducts_View.setSize(pnlManageProducts_View.getWidth() - (pnlManageProducts_View.getWidth() <= f.getWidth() ? 9 : 0), pnlManageProducts_View.getHeight());

                                    Thread.sleep(5);
                                }

                                pnlManageProducts_View.setVisible(false);
                                pnlManageProducts_View.setVisible(true);
                            } catch (Exception e) {
                            }
                        }
                    });
                    close.start();
                    checkManageProducts = true;
                }
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                super.mouseEntered(me); //To change body of generated methods, choose Tools | Templates.
                if (pnlManageProducts_ChoicesMain.getX() >= 0) {
                    XLICon.getBackgroundIcon("src/Icon/holderOpen.png", lblManageProducts_Switch, lblManageProducts_Switch.getWidth(), lblManageProducts_Switch.getHeight());

                } else {
                    XLICon.getBackgroundIcon("src/Icon/holderClose.png", lblManageProducts_Switch, lblManageProducts_Switch.getWidth(), lblManageProducts_Switch.getHeight());

                }
            }

        });

        lblManageEmployee_Switch.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                super.mouseClicked(me); //To change body of generated methods, choose Tools | Templates.
                if (checkManageEmployee) {
                    XLICon.getBackgroundIcon("src/Icon/holderClose.png", lblManageEmployee_Switch, lblManageEmployee_Switch.getWidth(), lblManageEmployee_Switch.getHeight());
                    Thread close = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                for (int i = pnlManageEmployees_Choices.getX(); i >= -pnlManageEmployees_Choices.getWidth(); i -= 10) {
                                    pnlManageEmployees_ChoicesMain.setLocation(i, pnlManageEmployees_ChoicesMain.getY());
                                    pnlManageEmployees_View.setLocation(pnlManageEmployees_ChoicesMain.getX() + pnlManageEmployees_ChoicesMain.getWidth() - (pnlManageEmployees_ChoicesMain.getWidth() - pnlManageEmployees_Choices.getWidth() + pnlManageEmployees_Choices.getX()), pnlManageEmployees_View.getY());
                                    pnlManageEmployees_View.setSize(pnlManageEmployees_View.getWidth() + (pnlManageEmployees_View.getWidth() <= f.getWidth() ? 9 : 0), pnlManageEmployees_View.getHeight());
                                    Thread.sleep(5);
                                }
                                pnlManageEmployees_View.setVisible(false);
                                pnlManageEmployees_View.setVisible(true);
                            } catch (Exception e) {
                            }
                        }
                    });
                    close.start();
                    checkManageEmployee = false;
                } else {
                    XLICon.getBackgroundIcon("src/Icon/holderOpen.png", lblManageEmployee_Switch, lblManageEmployee_Switch.getWidth(), lblManageEmployee_Switch.getHeight());
                    Thread close = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                for (int i = -pnlManageEmployees_Choices.getWidth(); i <= 0; i += 10) {
                                    pnlManageEmployees_ChoicesMain.setLocation(i, pnlManageEmployees_ChoicesMain.getY());
                                    pnlManageEmployees_View.setLocation(pnlManageEmployees_ChoicesMain.getX() + pnlManageEmployees_ChoicesMain.getWidth() - (pnlManageEmployees_ChoicesMain.getWidth() - pnlManageEmployees_Choices.getWidth() + pnlManageEmployees_Choices.getX()), pnlManageEmployees_View.getY());
                                    pnlManageEmployees_View.setSize(pnlManageEmployees_View.getWidth() - (pnlManageEmployees_View.getWidth() >= f.getWidth() - pnlManageEmployees_Choices.getWidth() ? 9 : 0), pnlManageEmployees_View.getHeight());

                                    Thread.sleep(5);
                                }

                                pnlManageEmployees_View.setVisible(false);
                                pnlManageEmployees_View.setVisible(true);
                            } catch (Exception e) {
                            }
                        }
                    });
                    close.start();
                    checkManageEmployee = true;
                }
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                super.mouseEntered(me); //To change body of generated methods, choose Tools | Templates.
                if (pnlManageEmployees_ChoicesMain.getX() >= 0) {
                    XLICon.getBackgroundIcon("src/Icon/holderOpen.png", lblManageEmployee_Switch, lblManageEmployee_Switch.getWidth(), lblManageEmployee_Switch.getHeight());

                } else {
                    XLICon.getBackgroundIcon("src/Icon/holderClose.png", lblManageEmployee_Switch, lblManageEmployee_Switch.getWidth(), lblManageEmployee_Switch.getHeight());

                }
            }

        });
        lblShowMenu.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                if (animateSet) {
                    XLICon.getBackgroundIcon("src/Icon/show.png", lblShowMenu, lblShowMenu.getWidth(), lblShowMenu.getHeight());
                    animation.jLabelXRight(lblMedia.getX(), lblMedia.getX() + lblMedia.getWidth() + 10, 2, 1, lblMedia);
                    animation.jLabelXRight(lblWeb.getX(), lblWeb.getX() + lblWeb.getWidth() + 10, 2, 1, lblWeb);
                    animateSet = false;
                } else {
                    XLICon.getBackgroundIcon("src/Icon/close.png", lblShowMenu, lblShowMenu.getWidth(), lblShowMenu.getHeight());
                    animation.jLabelXLeft(lblMedia.getX(), lblMedia.getX() - lblMedia.getWidth() - 10, 2, 1, lblMedia);
                    animation.jLabelXLeft(lblWeb.getX(), lblWeb.getX() - lblWeb.getWidth() - 10, 2, 1, lblWeb);
                    animateSet = true;
                }
            }
        });
        lblExit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                int choose = JOptionPane.showConfirmDialog(rootPane, "Do you wanna exit program?", "Exit", JOptionPane.YES_NO_OPTION);
                if (choose == JOptionPane.YES_OPTION) {
                    System.exit(0);
                } else {

                }
            }
        });
        lblHidden.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                f.setState(f.ICONIFIED);
            }
        });
        lblMenu_System.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                pomSystem.show(lblMenu_System, 0, lblMenu_System.getHeight());
            }
        });
        lblMenu_Statictis.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                pomStatictis.show(lblMenu_Statictis, 0, lblMenu_Statictis.getHeight());
            }
        });
        lblMenu_Manage.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                if (chklblmenusystem_manageclick) {
                    pomManage.show(lblMenu_Manage, 0, lblMenu_Manage.getHeight());
                } else {
                    Dialog.showMessageDialog(f, "Access is denied");
                }
            }
        });
        lblWeb.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                try {
                    Desktop.getDesktop().browse(URI.create("www.google.com"));
                } catch (Exception e) {
                }
            }
        });
        lblMedia.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                try {
                    Desktop.getDesktop().open(new File("C:\\Program Files (x86)\\Windows Media Player\\wmplayer.exe"));

                } catch (Exception e) {
                }
            }
        });

    }

    private void LabelHover() {

        lblManageEmployee_Information.addMouseListener(new LabelHeper() {
            @Override
            public void mouseEntered(MouseEvent me) {
                XLICon.getBackground("src/Icon/information.png", lblManageEmployee_Information, 30, 30);
                lblManageEmployee_Information.setForeground(new Color(32, 168, 18));
            }

            @Override
            public void mouseExited(MouseEvent me) {
                if (chklblmanageemployee_status) {
                    XLICon.getBackground("src/Icon/informationnone.png", lblManageEmployee_Information, 30, 30);
                } else {

                }
            }
        });

        lblManageEmployee_User.addMouseListener(new LabelHeper() {
            @Override
            public void mouseEntered(MouseEvent me) {
                super.mouseEntered(me); //To change body of generated methods, choose Tools | Templates.
                XLICon.getBackground("src/Icon/user.png", lblManageEmployee_User, 30, 30);
                lblManageEmployee_User.setForeground(new Color(32, 168, 18));
            }

            @Override
            public void mouseExited(MouseEvent me) {
                super.mouseExited(me); //To change body of generated methods, choose Tools | Templates.
                if (chklblmanageemployee_user) {
                    XLICon.getBackground("src/Icon/usernone.png", lblManageEmployee_User, 30, 30);
                    lblManageEmployee_User.setForeground(Color.black);
                } else {

                }
            }

        });
        lblManageEmployee_InvoicesOut.addMouseListener(new LabelHeper() {
            @Override
            public void mouseExited(MouseEvent me) {
                super.mouseExited(me); //To change body of generated methods, choose Tools | Templates.
                if (chklblmanageemployee_invoiceout) {
                    XLICon.getBackground("src/Icon/invoicenone.png", lblManageEmployee_InvoicesOut, 30, 30);
                    lblManageEmployee_InvoicesOut.setForeground(Color.black);
                } else {

                }
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                super.mouseEntered(me); //To change body of generated methods, choose Tools | Templates.
                XLICon.getBackground("src/Icon/invoice.png", lblManageEmployee_InvoicesOut, 30, 30);
                lblManageEmployee_InvoicesOut.setForeground(new Color(32, 168, 18));
            }
        });

        lblManageProducts_InvoiceIn.addMouseListener(new LabelHeper() {
            @Override
            public void mouseEntered(MouseEvent me) {
                XLICon.getBackground("src/Icon/invoice.png", lblManageProducts_InvoiceIn, 30, 30);
                lblManageProducts_InvoiceIn.setForeground(new Color(32, 168, 18));
            }

            @Override
            public void mouseExited(MouseEvent me) {
                if (chklblmanageproducts_invoicein) {
                    XLICon.getBackground("src/Icon/invoicenone.png", lblManageProducts_InvoiceIn, 30, 30);
                    lblManageProducts_InvoiceIn.setForeground(Color.BLACK);
                } else {

                }
            }
        });

        lblManageProducts_Product.addMouseListener(new LabelHeper() {
            @Override
            public void mouseEntered(MouseEvent me) {
                super.mouseEntered(me); //To change body of generated methods, choose Tools | Templates.
                XLICon.getBackground("src/Icon/product.png", lblManageProducts_Product, 30, 30);
                lblManageProducts_Product.setForeground(new Color(32, 168, 18));
            }

            @Override
            public void mouseExited(MouseEvent me) {
                super.mouseExited(me); //To change body of generated methods, choose Tools | Templates.
                if (chklblmanageproducts_product) {
                    XLICon.getBackground("src/Icon/productnone.png", lblManageProducts_Product, 30, 30);
                    lblManageProducts_Product.setForeground(Color.black);
                } else {

                }
            }

        });
        lblManageProducts_Vendors.addMouseListener(new LabelHeper() {
            @Override
            public void mouseExited(MouseEvent me) {
                super.mouseExited(me); //To change body of generated methods, choose Tools | Templates.
                if (chklblmanageproducts_vendors) {
                    XLICon.getBackground("src/Icon/vendornone.png", lblManageProducts_Vendors, 30, 30);
                    lblManageProducts_Vendors.setForeground(Color.black);
                } else {

                }
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                super.mouseEntered(me); //To change body of generated methods, choose Tools | Templates.
                XLICon.getBackground("src/Icon/vendor.png", lblManageProducts_Vendors, 30, 30);
                lblManageProducts_Vendors.setForeground(new Color(32, 168, 18));
            }
        });

        LabelHeper.LabelActionHover("src/Icon/exitHover.png", "src/Icon/exitNonehover.png", lblExit, lblExit.getWidth(), lblExit.getHeight(), Color.BLACK, Color.BLACK);
        LabelHeper.LabelActionHover("src/Icon/hiddenHover.png", "src/Icon/hiddenNonehover.png", lblHidden, lblHidden.getWidth(), lblHidden.getHeight(), Color.BLACK, Color.BLACK);
        LabelHeper.LabelActionHover("src/Icon/system.png", "src/Icon/systemnone.png", lblMenu_System, lblMenu_System.getHeight(), lblMenu_System.getHeight(), new Color(32, 204, 215), Color.BLACK);
        LabelHeper.LabelActionHover("src/Icon/statictis.png", "src/Icon/statictisnone.png", lblMenu_Statictis, lblMenu_Statictis.getHeight(), lblMenu_Statictis.getHeight(), new Color(233, 79, 116), Color.BLACK);
        LabelHeper.LabelActionHover("src/Icon/manage.png", "src/Icon/managenone.png", lblMenu_Manage, lblMenu_Manage.getHeight(), lblMenu_Manage.getHeight(), new Color(66, 136, 215), Color.BLACK);
        LabelHeper.LabelActionHover("src/Icon/help.png", "src/Icon/helpnone.png", lblMenu_Help, lblMenu_Help.getHeight(), lblMenu_Help.getHeight(), new Color(238, 176, 43), Color.BLACK);

        lblMenu_Help.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                super.mouseClicked(me); //To change body of generated methods, choose Tools | Templates.
                try {
                    Desktop.getDesktop().open(new File("src/Others/about.pdf"));
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(f, "File not found!!");
                }
            }

        });
        LabelHeper.LabelActionHover("src/Icon/about.png", "src/Icon/aboutnone.png", lblMenu_About, lblMenu_About.getHeight(), lblMenu_About.getHeight(), new Color(200, 96, 212), Color.BLACK);
        lblMenu_About.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                super.mouseClicked(me); //To change body of generated methods, choose Tools | Templates.
                FrameAbout.setLocationRelativeTo(lblMenu_About);
                FrameAbout.setResizable(false);
                FrameAbout.setSize(211, 288);
                FrameAbout.setVisible(true);
            }

        });
        lblShowMenu.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseExited(MouseEvent me) {
                super.mouseExited(me); //To change body of generated methods, choose Tools | Templates.
                if (animateSet) {
                    XLICon.getBackgroundIcon("src/Icon/closenone.png", lblShowMenu, lblShowMenu.getHeight(), lblShowMenu.getHeight());
                } else {
                    XLICon.getBackgroundIcon("src/Icon/shownone.png", lblShowMenu, lblShowMenu.getHeight(), lblShowMenu.getHeight());

                }
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                super.mouseEntered(me); //To change body of generated methods, choose Tools | Templates.
                if (animateSet) {
                    XLICon.getBackgroundIcon("src/Icon/close.png", lblShowMenu, lblShowMenu.getHeight(), lblShowMenu.getHeight());
                } else {
                    XLICon.getBackgroundIcon("src/Icon/show.png", lblShowMenu, lblShowMenu.getHeight(), lblShowMenu.getHeight());

                }
            }

        });
        LabelHeper.LabelActionHover("src/Icon/web.png", "src/Icon/webnone.png", lblWeb, lblWeb.getHeight(), lblWeb.getHeight(), Color.BLACK, Color.BLACK);
        LabelHeper.LabelActionHover("src/Icon/media.png", "src/Icon/medianone.png", lblMedia, lblMedia.getHeight(), lblMedia.getHeight(), Color.BLACK, Color.BLACK);
        btnLogin.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseExited(MouseEvent me) {
                super.mouseExited(me); //To change body of generated methods, choose Tools | Templates.
                btnLogin.setBackground(new Color(255, 51, 0));
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                btnLogin.setBackground(new Color(47, 157, 25));
            }

        });

        lblManageEmployee_User.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                if (!pnlManageEmployee_User.isVisible()) {
                    UserShow();
                }

            }
        });

        lblManageEmployee_InvoicesOut.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                if (chklblmanageemployee_invoiceoutclick) {
                    if (!pnlManageEmployee_InvoicesOut.isVisible()) {
                        InvoiceOutShow();
                    }
                } else {
                    Dialog.showMessageDialog(f, "Access is denied!!");
                }

            }
        });

        lblManageEmployee_Information.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                if (!pnlManageEmployee_Infomation.isVisible()) {
                    InfomationShow();
                }

            }

        });

        lblManageProducts_Product.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                if (!pnlManageProducts_Product.isVisible()) {
                    ProductShow();
                }

            }
        });

        lblManageProducts_InvoiceIn.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                if (!pnlManageProducts_InvoicesIn.isVisible()) {
                    InvoiceInShow();
                }

            }
        });

        lblManageProducts_Vendors.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                if (!pnlManageProducts_Vendors.isVisible()) {
                    VendorsShow();
                }

            }

        });

    }

    private void UserShow() {
        if (!pnlManageEmployee_User.isVisible()) {
            chklblmanageemployee_user = false;
            chklblmanageemployee_status = true;
            chklblmanageemployee_invoiceout = true;
            XLICon.getBackground("src/Icon/user.png", lblManageEmployee_User, 30, 30);
            lblManageEmployee_User.setForeground(new Color(32, 168, 18));
            XLICon.getBackground("src/Icon/invoicenone.png", lblManageEmployee_InvoicesOut, 30, 30);
            lblManageEmployee_InvoicesOut.setForeground(Color.BLACK);
            XLICon.getBackground("src/Icon/informationnone.png", lblManageEmployee_Information, 30, 30);
            lblManageEmployee_Information.setForeground(Color.BLACK);
            AnimationHelper.OneLeftOneRight(pnlManageEmployees_ChoicesMain, pnlManageEmployees_Choices, f, pnlManageEmployees_View, pnlManageEmployee_User, pnlManageEmployee_InvoicesOut, pnlManageEmployee_Infomation, pnlManageEmployees_Controller, pnlManageEmployees_UserController, pnlManageEmployees_InvoicesOutController, pnlManageEmployees_InfomationController);
        }
    }

    private void InvoiceOutShow() {
        if (!pnlManageEmployee_InvoicesOut.isVisible()) {
            chklblmanageemployee_user = true;
            chklblmanageemployee_status = true;
            chklblmanageemployee_invoiceout = false;
            XLICon.getBackground("src/Icon/invoice.png", lblManageEmployee_InvoicesOut, 30, 30);
            lblManageEmployee_InvoicesOut.setForeground(new Color(32, 168, 18));
            XLICon.getBackgroundIcon("src/Icon/informationnone.png", lblManageEmployee_Information, 30, 30);
            lblManageEmployee_Information.setForeground(Color.black);
            XLICon.getBackgroundIcon("src/Icon/usernone.png", lblManageEmployee_User, 30, 30);
            lblManageEmployee_User.setForeground(Color.black);
            AnimationHelper.OneLeftOneRight(pnlManageEmployees_ChoicesMain, pnlManageEmployees_Choices, f, pnlManageEmployees_View, pnlManageEmployee_InvoicesOut, pnlManageEmployee_Infomation, pnlManageEmployee_User, pnlManageEmployees_Controller, pnlManageEmployees_InvoicesOutController, pnlManageEmployees_InfomationController, pnlManageEmployees_UserController);
        }
    }

    private void InfomationShow() {
        if (!pnlManageEmployee_Infomation.isVisible()) {
            chklblmanageemployee_user = true;
            chklblmanageemployee_status = false;
            chklblmanageemployee_invoiceout = true;
            XLICon.getBackground("src/Icon/invoicenone.png", lblManageEmployee_InvoicesOut, 30, 30);
            lblManageEmployee_InvoicesOut.setForeground(Color.BLACK);
            XLICon.getBackgroundIcon("src/Icon/information.png", lblManageEmployee_Information, 30, 30);
            lblManageEmployee_Information.setForeground(new Color(32, 168, 18));
            XLICon.getBackgroundIcon("src/Icon/usernone.png", lblManageEmployee_User, 30, 30);
            lblManageEmployee_User.setForeground(Color.black);
            AnimationHelper.OneLeftOneRight(pnlManageEmployees_ChoicesMain, pnlManageEmployees_Choices, f, pnlManageEmployees_View, pnlManageEmployee_Infomation, pnlManageEmployee_InvoicesOut, pnlManageEmployee_User, pnlManageEmployees_Controller, pnlManageEmployees_InfomationController, pnlManageEmployees_InvoicesOutController, pnlManageEmployees_UserController);
        }
    }

    private void ProductShow() {
        if (!pnlManageProducts_Product.isVisible()) {
            chklblmanageproducts_product = false;
            chklblmanageproducts_vendors = true;
            chklblmanageproducts_invoicein = true;
            XLICon.getBackground("src/Icon/product.png", lblManageProducts_Product, 30, 30);
            lblManageProducts_Product.setForeground(new Color(32, 168, 18));
            XLICon.getBackground("src/Icon/invoicenone.png", lblManageProducts_InvoiceIn, 30, 30);
            lblManageProducts_InvoiceIn.setForeground(Color.BLACK);
            XLICon.getBackground("src/Icon/vendornone.png", lblManageProducts_Vendors, 30, 30);
            lblManageProducts_Vendors.setForeground(Color.BLACK);
            AnimationHelper.OneLeftOneRight(pnlManageProducts_ChoicesMain, pnlManageProducts_Choices, f, pnlManageProducts_View, pnlManageProducts_Product, pnlManageProducts_InvoicesIn, pnlManageProducts_Vendors, pnlManageProducts_Controller, pnlManageProducts_ProductController, pnlManageProducts_InvoicesInController, pnlManageProducts_VendorsController);
        }
    }

    private void InvoiceInShow() {
        if (!pnlManageProducts_InvoicesIn.isVisible()) {
            chklblmanageproducts_vendors = true;
            chklblmanageproducts_product = true;
            chklblmanageproducts_invoicein = false;
            XLICon.getBackground("src/Icon/invoice.png", lblManageProducts_InvoiceIn, 30, 30);
            lblManageProducts_InvoiceIn.setForeground(new Color(32, 168, 18));
            XLICon.getBackgroundIcon("src/Icon/productnone.png", lblManageProducts_Product, 30, 30);
            lblManageProducts_Product.setForeground(Color.black);
            XLICon.getBackgroundIcon("src/Icon/vendornone.png", lblManageProducts_Vendors, 30, 30);
            lblManageProducts_Vendors.setForeground(Color.black);
            AnimationHelper.OneLeftOneRight(pnlManageProducts_ChoicesMain, pnlManageProducts_Choices, f, pnlManageProducts_View, pnlManageProducts_InvoicesIn, pnlManageProducts_Product, pnlManageProducts_Vendors, pnlManageProducts_Controller, pnlManageProducts_InvoicesInController, pnlManageProducts_ProductController, pnlManageProducts_VendorsController);
        }
    }

    private void VendorsShow() {
        if (!pnlManageProducts_Vendors.isVisible()) {
            chklblmanageproducts_invoicein = true;
            chklblmanageproducts_vendors = false;
            chklblmanageproducts_product = true;
            XLICon.getBackground("src/Icon/invoicenone.png", lblManageProducts_InvoiceIn, 30, 30);
            lblManageProducts_InvoiceIn.setForeground(Color.BLACK);
            XLICon.getBackgroundIcon("src/Icon/vendor.png", lblManageProducts_Vendors, 30, 30);
            lblManageProducts_Vendors.setForeground(new Color(32, 168, 18));
            XLICon.getBackgroundIcon("src/Icon/productnone.png", lblManageProducts_Product, 30, 30);
            lblManageProducts_Product.setForeground(Color.black);
            AnimationHelper.OneLeftOneRight(pnlManageProducts_ChoicesMain, pnlManageProducts_Choices, f, pnlManageProducts_View, pnlManageProducts_Vendors, pnlManageProducts_Product, pnlManageProducts_InvoicesIn, pnlManageProducts_Controller, pnlManageProducts_VendorsController, pnlManageProducts_ProductController, pnlManageProducts_InvoicesInController);
        }
    }

    public void Login() {

        Thread change = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    for (int i = f.getWidth() - pnlMenu_start.getWidth(); i <= f.getWidth(); i += 10) {
                        pnlMenu_start.setLocation(i, pnlMenu_start.getY());
                        Thread.sleep(5);
                    }

                    for (int i = -f.getWidth(); i <= 0; i += 10) {
                        Thread.sleep(5);
                        pnlMenu_System.setLocation(i, pnlMenu_System.getY());

                    }
                } catch (Exception e) {
                }
            }
        });

        Thread move = new Thread(new Runnable() {
            @Override
            public void run() {

                if (checkrun) {
                    try {

                        for (int i = 0; i >= -pnlMenuUser.getWidth(); i -= 10) {
                            Thread.sleep(10);
                            pnlMenuUser.setLocation(i, pnlMenuUser.getY());
                            pnlMenuMain.setLocation(pnlMenuUser.getX() + pnlMenuUser.getWidth(), pnlMenuMain.getY());
                            pnlMenuMain.setSize(f.getWidth() - pnlMenuMain.getX(), pnlMenuMain.getHeight());
                            pnlMenuSystem.setSize(f.getWidth() - pnlMenuMain.getX(), pnlMenuSystem.getHeight());
                            lblExit.setLocation(pnlMenuMain.getWidth() - 30, lblExit.getY());
                            lblHidden.setLocation(pnlMenuMain.getWidth() - 60, lblExit.getY());
                            checkrun = false;
                        }
                        change.start();

                        for (int i = lblMenu_System.getY(); i <= -lblMenu_System.getY(); i++) {
                            Thread.sleep(5);
                            lblMenu_System.setLocation(lblMenu_System.getX(), i);
                        }
                        for (int i = lblMenu_Manage.getY(); i <= -lblMenu_Manage.getY(); i++) {
                            Thread.sleep(5);
                            lblMenu_Manage.setLocation(lblMenu_Manage.getX(), i);
                        }
                        for (int i = lblMenu_Statictis.getY(); i <= -lblMenu_Statictis.getY(); i++) {
                            Thread.sleep(5);
                            lblMenu_Statictis.setLocation(lblMenu_Statictis.getX(), i);
                        }
                        for (int i = lblMenu_Help.getY(); i <= -lblMenu_Help.getY(); i++) {
                            Thread.sleep(5);
                            lblMenu_Help.setLocation(lblMenu_Help.getX(), i);
                        }
                        for (int i = lblMenu_About.getY(); i <= -lblMenu_About.getY(); i++) {
                            Thread.sleep(5);
                            lblMenu_About.setLocation(lblMenu_About.getX(), i);
                        }

                        checkrun = true;
                    } catch (Exception e) {
                    }
                } else {

                }
            }
        });
        move.start();

    }

    public void Button() {
        // btnem

        btnLogin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                Thread td = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        VisibleHelper.SetVisible(false, pnlManageProducts_ViewMain, pnlManageProducts_Controller, pnlManageEmployees_ViewMain, pnlManageEmployees_Controller, pnlManageFinacial_ViewMain, pnlManageFinacial_Controller, pnlManageProducts_Product, pnlManageProducts_Vendors, pnlManageProducts_InvoicesIn, pnlManageProducts_InvoicesInController, pnlManageProducts_ProductController, pnlManageProducts_VendorsController, pnlManageProducts_ChoicesMain, pnlManageEmployee_Infomation, pnlManageEmployee_InvoicesOut, pnlManageEmployee_User, pnlManageEmployees_UserController, pnlManageEmployees_InfomationController, pnlManageEmployees_InvoicesOutController, pnlManageEmployees_ChoicesMain, pnlManageFinacial_ViewMain, pnlManageFinacial_Controller);
                        VisibleHelper.SetEnable(false, mitInvoidInMonth, mitFinacial, mitInvoicesIn, mitInvoicesOut, mitInvoidInMonth, mitProduct, mitStatus, mitUser, mitVendors);
                        VisibleHelper.SetEnable(false, menProducts, menEmployee);

                    }
                });
                td.start();
                Users_DAO ud = new Users_DAO();
                String role = ud.checkUser(txtUser.getText(), txtPass.getText());
                // VisibleHelper.SetVisible(false, new JPanel[]{pnlManageProducts_ViewMain, pnlManageEmployees_ViewMain, pnlManageFinacial_ViewMain, pnlManageProducts_Controller, pnlFinacialControll, pnlManageEmployees_Controller, pnlManageProducts_Product, pnlManageProducts_InvoicesIn, pnlManageProducts_Vendors, pnlManageProducts_ProductController, pnlManageProducts_InvoicesInController, pnlManageProducts_VendorsController, pnlManageProducts_ChoicesMain, pnlManageEmployee_Infomation, pnlManageEmployee_InvoicesOut, pnlManageEmployee_User, pnlManageEmployees_UserController, pnlManageEmployees_InvoicesOutController, pnlManageEmployees_InfomationController, pnlManageEmployees_ChoicesMain,pnlManageFinacial_View,pnlManageFinacial_Controller});
                if (role != null) {
                    if (role.equals("GD")) {
                        Thread t = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                VisibleHelper.SetVisible(true, pnlManageProducts_ViewMain, pnlManageProducts_ProductController, pnlManageProducts_Product, pnlManageProducts_ProductController, pnlManageProducts_ChoicesMain, pnlManageEmployees_ChoicesMain, pnlManageFinacial_ViewMain, pnlManageFinacial_Controller);
                                VisibleHelper.SetEnable(true, mitFinacial, mitInvoicesIn, mitInvoicesOut, mitInvoidInMonth, mitProduct, mitStatus, mitUser, mitVendors);
                                VisibleHelper.SetEnable(true, menProducts, menEmployee, menOptions);

                            }
                        });
                        t.start();
                        Login();

                    } else if (role.equals("NS")) {
                        Thread t = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                VisibleHelper.SetVisible(true, pnlManageEmployees_ViewMain, pnlManageEmployees_Controller, pnlManageEmployee_Infomation, pnlManageEmployees_InfomationController, pnlManageEmployees_ChoicesMain);
                                VisibleHelper.SetEnable(true, menEmployee);
                                VisibleHelper.SetEnable(true, mitUser, mitStatus, mitInvoidInMonth);
                                chklblmanageemployee_invoiceoutclick = false;

                            }
                        });
                        pnlManageEmployees_ChoicesMain.setVisible(true);
                        t.start();
                        Login();
                    } else if (role.equals("NV")) {
                        Thread close = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    for (int i = pnlManageEmployees_Choices.getX(); i >= -pnlManageEmployees_Choices.getWidth(); i -= 10) {
                                        pnlManageEmployees_ChoicesMain.setLocation(i, pnlManageEmployees_ChoicesMain.getY());
                                        pnlManageEmployees_View.setLocation(pnlManageEmployees_ChoicesMain.getX() + pnlManageEmployees_ChoicesMain.getWidth() - (pnlManageEmployees_ChoicesMain.getWidth() - pnlManageEmployees_Choices.getWidth() + pnlManageEmployees_Choices.getX()), pnlManageEmployees_View.getY());
                                        pnlManageEmployees_View.setSize(pnlManageEmployees_View.getWidth() + (pnlManageEmployees_View.getWidth() <= f.getWidth() ? 9 : 0), pnlManageEmployees_View.getHeight());
                                        Thread.sleep(5);
                                    }
                                    pnlManageEmployees_View.setVisible(false);
                                    pnlManageEmployees_View.setVisible(true);
                                } catch (Exception e) {
                                }
                            }
                        });
                        close.start();
                        chklblmenusystem_manageclick = true;
                        menEmployee.setVisible(true);
                        mitInvoicesOut.setVisible(true);
                        VisibleHelper.SetVisible(true, pnlManageEmployees_ViewMain, pnlManageEmployees_Controller, pnlManageEmployee_InvoicesOut, pnlManageEmployees_InvoicesOutController);
                        Login();
                    } else if (role.equals("QK")) {
                        VisibleHelper.SetVisible(true, pnlManageProducts_ChoicesMain);
                        VisibleHelper.SetEnable(true, menProducts);
                        VisibleHelper.SetEnable(true, mitProduct, mitVendors, mitInvoicesIn);
                        Login();
                    } else if (role.equals("TC")) {
                        VisibleHelper.SetEnable(true, mitFinacial, mitInvoidInMonth);
                        VisibleHelper.SetVisible(true, pnlManageFinacial_ViewMain, pnlManageFinacial_Controller);
                        Login();
                    }
                } else {
                    new Dialog().Showmess(rootPane, "Login fail", Dialog.INFORMATION_MESSAGE, null);
                }
            }
        });

        mitLogout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                Thread td = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        VisibleHelper.SetVisible(false, pnlManageProducts_ViewMain, pnlManageProducts_Controller, pnlManageEmployees_ViewMain, pnlManageEmployees_Controller, pnlManageFinacial_ViewMain, pnlManageFinacial_Controller, pnlManageProducts_Product, pnlManageProducts_Vendors, pnlManageProducts_InvoicesIn, pnlManageProducts_InvoicesInController, pnlManageProducts_ProductController, pnlManageProducts_VendorsController, pnlManageProducts_ChoicesMain, pnlManageEmployee_Infomation, pnlManageEmployee_InvoicesOut, pnlManageEmployee_User, pnlManageEmployees_UserController, pnlManageEmployees_InfomationController, pnlManageEmployees_InvoicesOutController, pnlManageEmployees_ChoicesMain, pnlManageFinacial_ViewMain, pnlManageFinacial_Controller);
                        VisibleHelper.SetEnable(false, mitInvoidInMonth, mitFinacial, mitInvoicesIn, mitInvoicesOut, mitInvoidInMonth, mitProduct, mitStatus, mitUser, mitVendors);
                        VisibleHelper.SetEnable(false, menProducts, menEmployee);

                    }
                });
                td.start();
                Thread change = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            for (int i = 0; i >= -f.getWidth(); i -= 10) {
                                Thread.sleep(5);
                                pnlMenu_System.setLocation(i, pnlMenu_System.getY());

                            }
                            for (int i = f.getWidth(); i >= 0; i -= 10) {
                                pnlMenu_start.setLocation(i, pnlMenu_start.getY());
                                Thread.sleep(5);
                            }

                        } catch (Exception e) {
                        }
                    }
                });

                Thread move = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Thread move = new Thread(new Runnable() {
                            @Override
                            public void run() {

                                if (checkrun) {
                                    try {
                                        for (int i = -pnlMenuUser.getWidth(); i <= 0; i += 10) {
                                            Thread.sleep(10);
                                            pnlMenuUser.setLocation(i, pnlMenuUser.getY());
                                            pnlMenuMain.setLocation(pnlMenuUser.getX() + pnlMenuUser.getWidth(), pnlMenuMain.getY());
                                            pnlMenuMain.setSize(f.getWidth() - pnlMenuMain.getX(), pnlMenuMain.getHeight());
                                            pnlMenuSystem.setSize(f.getWidth() - pnlMenuMain.getX(), pnlMenuSystem.getHeight());
                                            lblExit.setLocation(pnlMenuMain.getWidth() - 30, lblExit.getY());
                                            lblHidden.setLocation(pnlMenuMain.getWidth() - 60, lblExit.getY());
                                            checkrun = false;
                                        }
                                        change.start();
                                        for (int i = lblMenu_About.getY(); i >= -lblMenu_About.getHeight(); i--) {
                                            Thread.sleep(5);
                                            lblMenu_About.setLocation(lblMenu_About.getX(), i);
                                        }
                                        for (int i = lblMenu_Help.getY(); i >= -lblMenu_Help.getHeight(); i--) {
                                            Thread.sleep(5);
                                            lblMenu_Help.setLocation(lblMenu_Help.getX(), i);
                                        }
                                        for (int i = lblMenu_Statictis.getY(); i >= -lblMenu_Statictis.getHeight(); i--) {
                                            Thread.sleep(5);
                                            lblMenu_Statictis.setLocation(lblMenu_Statictis.getX(), i);
                                        }
                                        for (int i = lblMenu_Manage.getY(); i >= -lblMenu_Manage.getHeight(); i--) {
                                            Thread.sleep(5);
                                            lblMenu_Manage.setLocation(lblMenu_Manage.getX(), i);
                                        }

                                        for (int i = lblMenu_System.getY(); i >= -lblMenu_System.getHeight(); i--) {
                                            Thread.sleep(5);
                                            lblMenu_System.setLocation(lblMenu_System.getX(), i);
                                        }
                                        checkrun = true;
                                    } catch (Exception e) {
                                    }
                                } else {

                                }
                            }
                        });
                        move.start();
                    }
                });
                move.start();
            }
        });
        mitCal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    Runtime rt = Runtime.getRuntime();
                    Process pr = rt.exec("calc");
                    pr.waitFor();
                } catch (Exception e) {
                }
            }
        });
        mitExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                int choose = JOptionPane.showConfirmDialog(rootPane, "Do you wanna exit program?", "Exit", JOptionPane.YES_NO_OPTION);
                if (choose == JOptionPane.YES_OPTION) {
                    System.exit(0);
                } else {

                }
            }
        });
    }

    private void Icon() {
        Image i = Toolkit.getDefaultToolkit().getImage("src/Icon/giphy.gif");
        ImageIcon im = new ImageIcon(i);
        lblMenuStart.setIcon(im);
        XLICon.getBackgroundIcon("src/Icon/closenone.png", lblShowMenu, lblShowMenu.getWidth(), lblShowMenu.getHeight());
        XLICon.getBackgroundIcon("src/Icon/webnone.png", lblWeb, lblWeb.getWidth(), lblWeb.getHeight());
        XLICon.getBackgroundIcon("src/Icon/medianone.png", lblMedia, lblMedia.getWidth(), lblMedia.getHeight());
        XLICon.getBackgroundIcon("src/Icon/login.png", lblIconUser, lblIconUser.getWidth(), lblIconUser.getHeight());
        XLICon.getBackgroundIcon("src/Icon/password.png", lblIconPass, lblIconPass.getWidth(), lblIconPass.getHeight());
        XLICon.getBackgroundIcon("src/Icon/exitNonehover.png", lblExit, lblExit.getWidth(), lblExit.getHeight());
        XLICon.getBackgroundIcon("src/Icon/hiddenNonehover.png", lblHidden, lblHidden.getWidth(), lblHidden.getHeight());
        XLICon.getBackgroundIcon("src/Icon/systemNone.png", lblMenu_System, lblMenu_System.getHeight(), lblMenu_System.getHeight());
        XLICon.getBackgroundIcon("src/Icon/manageNone.png", lblMenu_Manage, lblMenu_Manage.getHeight(), lblMenu_Manage.getHeight());
        XLICon.getBackgroundIcon("src/Icon/statictisNone.png", lblMenu_Statictis, lblMenu_Statictis.getHeight(), lblMenu_Statictis.getHeight());
        XLICon.getBackgroundIcon("src/Icon/helpNone.png", lblMenu_Help, lblMenu_Help.getHeight(), lblMenu_Help.getHeight());
        XLICon.getBackgroundIcon("src/Icon/aboutNone.png", lblMenu_About, lblMenu_About.getHeight(), lblMenu_About.getHeight());
        XLICon.getBackgroundIcon("src/Icon/vendornone.png", lblManageProducts_Vendors, 30, 30);
        XLICon.getBackgroundIcon("src/Icon/invoicenone.png", lblManageProducts_InvoiceIn, 30, 30);
        XLICon.getBackgroundIcon("src/Icon/productnone.png", lblManageProducts_Product, 30, 30);
        XLICon.getBackgroundIcon("src/Icon//holderOpen.png", lblManageProducts_Switch, lblManageProducts_Switch.getWidth(), lblManageProducts_Switch.getHeight());
        XLICon.getBackgroundIcon("src/Icon/usernone.png", lblManageEmployee_User, 30, 30);
        XLICon.getBackgroundIcon("src/Icon/informationnone.png", lblManageEmployee_Information, 30, 30);
        XLICon.getBackgroundIcon("src/Icon/invoicenone.png", lblManageEmployee_InvoicesOut, 30, 30);
        XLICon.getBackgroundIcon("src/Icon/holderOpen.png", lblManageEmployee_Switch, lblManageEmployee_Switch.getWidth(), lblManageEmployee_Switch.getHeight());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pomSystem = new javax.swing.JPopupMenu();
        mitLogout = new javax.swing.JMenuItem();
        mitCal = new javax.swing.JMenuItem();
        menOptions = new javax.swing.JMenu();
        mitChangeColorLabel = new javax.swing.JMenuItem();
        mitChangeColorButton = new javax.swing.JMenuItem();
        mitchangeColorBaclground = new javax.swing.JMenuItem();
        mitExit = new javax.swing.JMenuItem();
        pomManage = new javax.swing.JPopupMenu();
        mitFinacial = new javax.swing.JMenuItem();
        menProducts = new javax.swing.JMenu();
        mitInvoicesIn = new javax.swing.JMenuItem();
        mitProduct = new javax.swing.JMenuItem();
        mitVendors = new javax.swing.JMenuItem();
        menEmployee = new javax.swing.JMenu();
        mitStatus = new javax.swing.JMenuItem();
        mitUser = new javax.swing.JMenuItem();
        mitInvoicesOut = new javax.swing.JMenuItem();
        pomStatictis = new javax.swing.JPopupMenu();
        mitInvoidInMonth = new javax.swing.JMenuItem();
        FrameAbout = new javax.swing.JFrame();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        invoiceOutGroup = new javax.swing.ButtonGroup();
        invoiceInGroup = new javax.swing.ButtonGroup();
        finalcial = new javax.swing.ButtonGroup();
        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();
        buttonGroup4 = new javax.swing.ButtonGroup();
        buttonGroup5 = new javax.swing.ButtonGroup();
        buttonGroup6 = new javax.swing.ButtonGroup();
        buttonGroup7 = new javax.swing.ButtonGroup();
        buttonGroup8 = new javax.swing.ButtonGroup();
        pnlFrame = new javax.swing.JPanel();
        pnlMenuMain = new javax.swing.JPanel();
        lblHidden = new javax.swing.JLabel();
        lblExit = new javax.swing.JLabel();
        pnlMenuSystem = new javax.swing.JPanel();
        lblMenu_About = new javax.swing.JLabel();
        lblMenu_Help = new javax.swing.JLabel();
        lblMenu_Statictis = new javax.swing.JLabel();
        lblMenu_Manage = new javax.swing.JLabel();
        lblMenu_System = new javax.swing.JLabel();
        pnlMenu_start = new javax.swing.JPanel();
        lblMenuStart = new javax.swing.JLabel();
        pnlMenu_System = new javax.swing.JPanel();
        pnlMenu_SystemView = new javax.swing.JPanel();
        pnlManageProducts_ViewMain = new javax.swing.JPanel();
        pnlManageProducts_ChoicesMain = new javax.swing.JPanel();
        lblManageProducts_Switch = new javax.swing.JLabel();
        pnlManageProducts_Choices = new javax.swing.JPanel();
        lblManageProducts_Product = new javax.swing.JLabel();
        lblManageProducts_InvoiceIn = new javax.swing.JLabel();
        lblManageProducts_Vendors = new javax.swing.JLabel();
        pnlManageProducts_View = new javax.swing.JPanel();
        pnlManageProducts_Product = new javax.swing.JPanel();
        srcProducts = new javax.swing.JScrollPane();
        tblManageProducts_Product = new javax.swing.JTable();
        pnlManageProducts_InvoicesIn = new javax.swing.JPanel();
        srcManageProducts_InvoicesIn = new javax.swing.JScrollPane();
        tblManageProducts_InvoicesIn = new javax.swing.JTable();
        pnlManageProducts_Vendors = new javax.swing.JPanel();
        srcManageProducts_Vendors = new javax.swing.JScrollPane();
        tblManageProducts_Vendors = new javax.swing.JTable();
        pnlManageEmployees_ViewMain = new javax.swing.JPanel();
        pnlManageEmployees_ChoicesMain = new javax.swing.JPanel();
        lblManageEmployee_Switch = new javax.swing.JLabel();
        pnlManageEmployees_Choices = new javax.swing.JPanel();
        lblManageEmployee_InvoicesOut = new javax.swing.JLabel();
        lblManageEmployee_Information = new javax.swing.JLabel();
        lblManageEmployee_User = new javax.swing.JLabel();
        pnlManageEmployees_View = new javax.swing.JPanel();
        pnlManageEmployee_Infomation = new javax.swing.JPanel();
        scrStatusEmployees = new javax.swing.JScrollPane();
        tblStatusEmployees = new javax.swing.JTable();
        pnlManageEmployee_User = new javax.swing.JPanel();
        scrUser = new javax.swing.JScrollPane();
        tblUser = new javax.swing.JTable();
        pnlManageEmployee_InvoicesOut = new javax.swing.JPanel();
        scrInvoicesOut = new javax.swing.JScrollPane();
        tblInvoicesOut = new javax.swing.JTable();
        pnlManageFinacial_ViewMain = new javax.swing.JPanel();
        pnlManageFinacial_View = new javax.swing.JPanel();
        scrManageFinacial = new javax.swing.JScrollPane();
        tblManageFinacial = new javax.swing.JTable();
        pnlMenu_SystemController = new javax.swing.JPanel();
        pnlManageProducts_Controller = new javax.swing.JPanel();
        pnlManageProducts_ProductController = new javax.swing.JPanel();
        txtProduct_ID = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        txtProduct_Name = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        txtProduct_VendorID = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        cbbProduct_Unit = new javax.swing.JComboBox<>();
        btnProduct_New = new javax.swing.JButton();
        btnProduct_Delete = new javax.swing.JButton();
        btnProduct_Update = new javax.swing.JButton();
        btnProduct_Insert = new javax.swing.JButton();
        btnProduct_Top = new javax.swing.JButton();
        btnProduct_Bottom = new javax.swing.JButton();
        btnProduct_Pre = new javax.swing.JButton();
        btnProduct_Next = new javax.swing.JButton();
        txtProduct_Find = new javax.swing.JTextField();
        btnProduct_Find = new javax.swing.JButton();
        jSeparator8 = new javax.swing.JSeparator();
        txtProduct_Price = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        txtProduct_Quantity = new javax.swing.JTextField();
        rdoProduct_ProID = new javax.swing.JRadioButton();
        rdoProduct_VendorID = new javax.swing.JRadioButton();
        rdoProduct_Name = new javax.swing.JRadioButton();
        pnlManageProducts_InvoicesInController = new javax.swing.JPanel();
        txtInvoiceBought_VendorID = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        txtInvoiceBought_TotalMoney = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        txtInvoiceBought_ImplID = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        txtInvoiceBought_DateCreated = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        btnInvoiceBought_New = new javax.swing.JButton();
        btnInvoiceBought_Delete = new javax.swing.JButton();
        btnInvoiceBought_Update = new javax.swing.JButton();
        btnInvoiceBought_Insert = new javax.swing.JButton();
        btnInvoiceBought_Top = new javax.swing.JButton();
        btnInvoiceBought_Bottom = new javax.swing.JButton();
        btnInvoiceBought_Pre = new javax.swing.JButton();
        btnInvoiceBought_Next = new javax.swing.JButton();
        txtInvoicebought_Find = new javax.swing.JTextField();
        btnInvoiceBought_Find = new javax.swing.JButton();
        jSeparator6 = new javax.swing.JSeparator();
        txtInvoiceBought_ID1 = new javax.swing.JTextField();
        jLabel38 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        txtInvoiceBought_ProductID = new javax.swing.JTextField();
        jLabel47 = new javax.swing.JLabel();
        txtInvoiceBought_Price = new javax.swing.JTextField();
        jLabel48 = new javax.swing.JLabel();
        txtInvoiceBought_Quantity = new javax.swing.JTextField();
        rdoInvoiceIn_InvoiceID = new javax.swing.JRadioButton();
        rdoInvoiceIn_VendorID = new javax.swing.JRadioButton();
        rdoInvoiceIn_ProductID = new javax.swing.JRadioButton();
        rdoInvoiceIn_EmplID = new javax.swing.JRadioButton();
        pnlManageProducts_VendorsController = new javax.swing.JPanel();
        txtVendor_ID = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        txtVendor_Name = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtVendor_Address = new javax.swing.JTextArea();
        jLabel29 = new javax.swing.JLabel();
        lblEmloyee_Avatar1 = new javax.swing.JLabel();
        btnVendor_New = new javax.swing.JButton();
        btnVendor_Delete = new javax.swing.JButton();
        btnVendor_Update = new javax.swing.JButton();
        btnVendor_Insert = new javax.swing.JButton();
        btnVendor_Top = new javax.swing.JButton();
        btnVendor_Bottom = new javax.swing.JButton();
        btnVendor_Pre = new javax.swing.JButton();
        btnVendor_Next = new javax.swing.JButton();
        txtVendor_FInd = new javax.swing.JTextField();
        btnVendor_Find = new javax.swing.JButton();
        jSeparator9 = new javax.swing.JSeparator();
        rdoVendor_VendorID = new javax.swing.JRadioButton();
        rdoVendor_Name = new javax.swing.JRadioButton();
        rdoVendor_Address = new javax.swing.JRadioButton();
        pnlManageEmployees_Controller = new javax.swing.JPanel();
        pnlManageEmployees_InfomationController = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        txtEmployee_ID = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtEmployee_Name = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txtEployee_PersonalID = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtEmployee_Address = new javax.swing.JTextArea();
        jLabel11 = new javax.swing.JLabel();
        cbbEmployee_Position = new javax.swing.JComboBox<>();
        lblEmloyee_Avatar = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        btnEmploye_New = new javax.swing.JButton();
        btnEmployee_Delete = new javax.swing.JButton();
        btnEmployee_Update = new javax.swing.JButton();
        btnEmployee_Insert = new javax.swing.JButton();
        btnEmployee_Top = new javax.swing.JButton();
        btnEmployee_Bottom = new javax.swing.JButton();
        btnEmployee_Pre = new javax.swing.JButton();
        btnEmployee_Next = new javax.swing.JButton();
        txtEmployee_Find = new javax.swing.JTextField();
        btnEmloyee_Find = new javax.swing.JButton();
        jSeparator5 = new javax.swing.JSeparator();
        rdoPersonal_EmplID = new javax.swing.JRadioButton();
        rdoPersonal_Name = new javax.swing.JRadioButton();
        rdoPersonal_PersonID = new javax.swing.JRadioButton();
        rdoPersonal_Position = new javax.swing.JRadioButton();
        pnlManageEmployees_UserController = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        txtUser_EmplID = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        txtUser_Username = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        cbbUser_role = new javax.swing.JComboBox<>();
        lblUser_Avatar = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        btnUser_New = new javax.swing.JButton();
        btnUser_Delete = new javax.swing.JButton();
        btnUser_Update = new javax.swing.JButton();
        btnUser_Insert = new javax.swing.JButton();
        btnUser_Top = new javax.swing.JButton();
        btnUser_Bottom = new javax.swing.JButton();
        btnUser_Pre = new javax.swing.JButton();
        btnUser_Next = new javax.swing.JButton();
        txtUser_Find = new javax.swing.JTextField();
        btnUser_Find = new javax.swing.JButton();
        txtUser_Password = new javax.swing.JPasswordField();
        jSeparator4 = new javax.swing.JSeparator();
        jLabel42 = new javax.swing.JLabel();
        txtUser_Confirm = new javax.swing.JPasswordField();
        rdoUser_EmplID = new javax.swing.JRadioButton();
        rdoUsername = new javax.swing.JRadioButton();
        rdoRole = new javax.swing.JRadioButton();
        pnlManageEmployees_InvoicesOutController = new javax.swing.JPanel();
        txtInvoiceSold_ID = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txtInvoiceSold_TotalMoney = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        txtInvoiceSold_ImplID = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        txtInvoiceSold_DateCreated = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        btnInvoiceSold_New = new javax.swing.JButton();
        btnInvoiceSold_Delete = new javax.swing.JButton();
        btnInvoiceSold_Update = new javax.swing.JButton();
        btnInvoiceSold_Insert = new javax.swing.JButton();
        btnInvoiceSold_Top = new javax.swing.JButton();
        btnInvoiceSold_Bottom = new javax.swing.JButton();
        btnInvoiceSold_Pre = new javax.swing.JButton();
        btnInvoiceSold_Next = new javax.swing.JButton();
        txtInvoiceSold_Find = new javax.swing.JTextField();
        btnInvoiceSold_Find = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel43 = new javax.swing.JLabel();
        txtInvoiceSold_ProductID = new javax.swing.JTextField();
        txtInvoiceSold_Quantity = new javax.swing.JTextField();
        jLabel44 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        txtInvoiceSold_Price = new javax.swing.JTextField();
        rdoInvoiceOut_InvoiceID = new javax.swing.JRadioButton();
        rdoInvoiceOut_ProID = new javax.swing.JRadioButton();
        rdoInvoieOut_EmplID = new javax.swing.JRadioButton();
        pnlManageFinacial_Controller = new javax.swing.JPanel();
        pnlFinacialControll = new javax.swing.JPanel();
        txtIFinalcial_PositionID = new javax.swing.JTextField();
        jLabel39 = new javax.swing.JLabel();
        txtFinacial_Position = new javax.swing.JTextField();
        jLabel40 = new javax.swing.JLabel();
        txtIFinacial_Salary = new javax.swing.JTextField();
        jLabel41 = new javax.swing.JLabel();
        btnFinacial_New = new javax.swing.JButton();
        btnFinacial_Delete = new javax.swing.JButton();
        btnFinacial_Update = new javax.swing.JButton();
        btnFinacial_Insert = new javax.swing.JButton();
        btnFinacial_Top = new javax.swing.JButton();
        btnFinacial_Bottom = new javax.swing.JButton();
        btnFinacial_Pre = new javax.swing.JButton();
        btnFinacial_Next = new javax.swing.JButton();
        txtFianancial_Find = new javax.swing.JTextField();
        btnFinacial_Find = new javax.swing.JButton();
        jSeparator7 = new javax.swing.JSeparator();
        rdoFinancial_PosiID = new javax.swing.JRadioButton();
        rdoFinancial_Posi = new javax.swing.JRadioButton();
        pnlStatitis_View = new javax.swing.JPanel();
        pnlStatictis_Table = new javax.swing.JTabbedPane();
        pnlStatictis_InvoiceOut = new javax.swing.JPanel();
        rdbStatictis_InvoiceOut_All = new javax.swing.JRadioButton();
        rdbStatictis_InvoiceOut_Month = new javax.swing.JRadioButton();
        cbbStatictis_InvoiceOutl = new javax.swing.JComboBox<>();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblStatictis_InvoiceOut = new javax.swing.JTable();
        pnlStatictis_InvoiceIn = new javax.swing.JPanel();
        rdbStatictis_InvoiceIn_All = new javax.swing.JRadioButton();
        rdbStatictis_InvoiceIn_Month = new javax.swing.JRadioButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        tblStatictis_InvoiceIn = new javax.swing.JTable();
        cbbStatictis_InvoiceIn = new javax.swing.JComboBox<>();
        pnlStatictis_Finacial = new javax.swing.JPanel();
        rdbStatictis_Finacial_All = new javax.swing.JRadioButton();
        rdbStatictis_Finalcial_Month = new javax.swing.JRadioButton();
        jScrollPane7 = new javax.swing.JScrollPane();
        tblStatictis_Finacial_report = new javax.swing.JTable();
        cbbStatictis_Finacial = new javax.swing.JComboBox<>();
        pnlStatictis_Chart = new javax.swing.JPanel();
        pnlMenuUser = new javax.swing.JPanel();
        pnlLogin = new javax.swing.JPanel();
        txtUser = new javax.swing.JTextField();
        txtPass = new javax.swing.JPasswordField();
        btnLogin = new javax.swing.JButton();
        lblFogot = new javax.swing.JLabel();
        lblIconUser = new javax.swing.JLabel();
        lblIconPass = new javax.swing.JLabel();
        lblWeb = new javax.swing.JLabel();
        lblMedia = new javax.swing.JLabel();
        lblShowMenu = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();

        pomSystem.setBackground(null);

        mitLogout.setText("Log Out");
        mitLogout.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        pomSystem.add(mitLogout);

        mitCal.setText("Calculator");
        mitCal.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        pomSystem.add(mitCal);

        menOptions.setBackground(null);
        menOptions.setText("Options");

        mitChangeColorLabel.setBackground(new java.awt.Color(255, 255, 255));
        mitChangeColorLabel.setForeground(new java.awt.Color(51, 51, 51));
        mitChangeColorLabel.setText("Color Label");
        menOptions.add(mitChangeColorLabel);

        mitChangeColorButton.setBackground(new java.awt.Color(255, 255, 255));
        mitChangeColorButton.setForeground(new java.awt.Color(51, 51, 51));
        mitChangeColorButton.setText("Color Button");
        menOptions.add(mitChangeColorButton);

        mitchangeColorBaclground.setBackground(new java.awt.Color(255, 255, 255));
        mitchangeColorBaclground.setForeground(new java.awt.Color(51, 51, 51));
        mitchangeColorBaclground.setText("Background color");
        menOptions.add(mitchangeColorBaclground);

        pomSystem.add(menOptions);

        mitExit.setText("Exit");
        pomSystem.add(mitExit);

        pomManage.setBackground(null);

        mitFinacial.setBackground(new java.awt.Color(255, 255, 255));
        mitFinacial.setForeground(new java.awt.Color(51, 51, 51));
        mitFinacial.setText("Manage Finacial");
        mitFinacial.setToolTipText("");
        pomManage.add(mitFinacial);

        menProducts.setBackground(null);
        menProducts.setText("Manage Products");

        mitInvoicesIn.setBackground(new java.awt.Color(255, 255, 255));
        mitInvoicesIn.setForeground(new java.awt.Color(51, 51, 51));
        mitInvoicesIn.setText("Invoices In");
        menProducts.add(mitInvoicesIn);

        mitProduct.setBackground(new java.awt.Color(255, 255, 255));
        mitProduct.setForeground(new java.awt.Color(51, 51, 51));
        mitProduct.setText("Products");
        menProducts.add(mitProduct);

        mitVendors.setBackground(new java.awt.Color(255, 255, 255));
        mitVendors.setForeground(new java.awt.Color(51, 51, 51));
        mitVendors.setText("Vendors");
        menProducts.add(mitVendors);

        pomManage.add(menProducts);

        menEmployee.setBackground(null);
        menEmployee.setText("Manage Employees");

        mitStatus.setBackground(new java.awt.Color(255, 255, 255));
        mitStatus.setForeground(new java.awt.Color(51, 51, 51));
        mitStatus.setText("Persional ");
        menEmployee.add(mitStatus);

        mitUser.setBackground(new java.awt.Color(255, 255, 255));
        mitUser.setForeground(new java.awt.Color(51, 51, 51));
        mitUser.setText("User Login");
        menEmployee.add(mitUser);

        mitInvoicesOut.setBackground(new java.awt.Color(255, 255, 255));
        mitInvoicesOut.setForeground(new java.awt.Color(51, 51, 51));
        mitInvoicesOut.setText("Invoices Out");
        menEmployee.add(mitInvoicesOut);

        pomManage.add(menEmployee);

        pomStatictis.setBackground(null);

        mitInvoidInMonth.setBackground(new java.awt.Color(255, 255, 255));
        mitInvoidInMonth.setForeground(new java.awt.Color(51, 51, 51));
        mitInvoidInMonth.setText("Chart");
        pomStatictis.add(mitInvoidInMonth);

        FrameAbout.setType(java.awt.Window.Type.UTILITY);

        jLabel1.setText("Version:1.1.0");

        jLabel2.setText("Product by: Group 1.");

        jLabel3.setText("Coder: Nguyễn Xuân Sang");

        jLabel4.setText("Coder: Huỳnh Ngọc Anh");

        jLabel5.setText("Coder: Võ Tuấn Kiệt");

        jLabel6.setText("Member: Phan Thành Lộc");

        jLabel7.setText("Project_Demo");

        javax.swing.GroupLayout FrameAboutLayout = new javax.swing.GroupLayout(FrameAbout.getContentPane());
        FrameAbout.getContentPane().setLayout(FrameAboutLayout);
        FrameAboutLayout.setHorizontalGroup(
            FrameAboutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FrameAboutLayout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(FrameAboutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(FrameAboutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(FrameAboutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6))
                        .addGroup(FrameAboutLayout.createSequentialGroup()
                            .addGap(25, 25, 25)
                            .addComponent(jLabel2)))
                    .addGroup(FrameAboutLayout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(jLabel7)))
                .addContainerGap(47, Short.MAX_VALUE))
        );
        FrameAboutLayout.setVerticalGroup(
            FrameAboutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FrameAboutLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel7)
                .addContainerGap(54, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(950, 530));
        setUndecorated(true);

        pnlFrame.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED, java.awt.Color.darkGray, java.awt.Color.darkGray));
        pnlFrame.setLayout(null);

        pnlMenuMain.setBackground(new java.awt.Color(255, 255, 255));
        pnlMenuMain.setForeground(new java.awt.Color(255, 255, 255));
        pnlMenuMain.setLayout(null);
        pnlMenuMain.add(lblHidden);
        lblHidden.setBounds(640, 5, 25, 25);
        pnlMenuMain.add(lblExit);
        lblExit.setBounds(670, 5, 25, 25);

        pnlMenuSystem.setOpaque(false);
        pnlMenuSystem.setLayout(null);

        lblMenu_About.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 14)); // NOI18N
        lblMenu_About.setText("About");
        pnlMenuSystem.add(lblMenu_About);
        lblMenu_About.setBounds(446, 0, 100, 30);

        lblMenu_Help.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 14)); // NOI18N
        lblMenu_Help.setText("Help");
        pnlMenuSystem.add(lblMenu_Help);
        lblMenu_Help.setBounds(354, 0, 74, 30);

        lblMenu_Statictis.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 14)); // NOI18N
        lblMenu_Statictis.setText("Statictis");
        pnlMenuSystem.add(lblMenu_Statictis);
        lblMenu_Statictis.setBounds(236, 0, 100, 30);

        lblMenu_Manage.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 14)); // NOI18N
        lblMenu_Manage.setText("Manage");
        pnlMenuSystem.add(lblMenu_Manage);
        lblMenu_Manage.setBounds(118, 0, 100, 30);

        lblMenu_System.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 14)); // NOI18N
        lblMenu_System.setText("System");
        pnlMenuSystem.add(lblMenu_System);
        lblMenu_System.setBounds(0, 0, 100, 30);

        pnlMenuMain.add(pnlMenuSystem);
        pnlMenuSystem.setBounds(0, 0, 540, 30);

        pnlMenu_start.setOpaque(false);
        pnlMenu_start.setLayout(null);

        lblMenuStart.setBackground(new java.awt.Color(255, 255, 255));
        pnlMenu_start.add(lblMenuStart);
        lblMenuStart.setBounds(0, 0, 700, 500);

        pnlMenuMain.add(pnlMenu_start);
        pnlMenu_start.setBounds(0, 30, 700, 500);

        pnlMenu_System.setPreferredSize(new java.awt.Dimension(950, 500));
        pnlMenu_System.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pnlMenu_SystemView.setPreferredSize(new java.awt.Dimension(950, 250));
        pnlMenu_SystemView.setLayout(new java.awt.CardLayout());

        pnlManageProducts_ViewMain.setLayout(null);

        pnlManageProducts_ChoicesMain.setOpaque(false);
        pnlManageProducts_ChoicesMain.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblManageProducts_Switch.setForeground(new java.awt.Color(0, 0, 255));
        pnlManageProducts_ChoicesMain.add(lblManageProducts_Switch, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 70, 20, 70));

        pnlManageProducts_Choices.setBackground(new java.awt.Color(255, 255, 255));
        pnlManageProducts_Choices.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblManageProducts_Product.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 11)); // NOI18N
        lblManageProducts_Product.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblManageProducts_Product.setText("Product");
        pnlManageProducts_Choices.add(lblManageProducts_Product, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 70, 43));

        lblManageProducts_InvoiceIn.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 11)); // NOI18N
        lblManageProducts_InvoiceIn.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblManageProducts_InvoiceIn.setText("Bill");
        pnlManageProducts_Choices.add(lblManageProducts_InvoiceIn, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 80, 70, 43));

        lblManageProducts_Vendors.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 11)); // NOI18N
        lblManageProducts_Vendors.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblManageProducts_Vendors.setText("Vendor");
        pnlManageProducts_Choices.add(lblManageProducts_Vendors, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 150, 70, 43));

        pnlManageProducts_ChoicesMain.add(pnlManageProducts_Choices, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 110, 250));

        pnlManageProducts_ViewMain.add(pnlManageProducts_ChoicesMain);
        pnlManageProducts_ChoicesMain.setBounds(0, 0, 150, 250);

        pnlManageProducts_View.setLayout(new java.awt.CardLayout());

        pnlManageProducts_Product.setLayout(new javax.swing.OverlayLayout(pnlManageProducts_Product));

        srcProducts.setPreferredSize(new java.awt.Dimension(840, 250));

        tblManageProducts_Product.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Product ID", "Name", "Vendor ID", "Unit","Price","Quantity"
            }
        ));
        srcProducts.setViewportView(tblManageProducts_Product);

        pnlManageProducts_Product.add(srcProducts);

        pnlManageProducts_View.add(pnlManageProducts_Product, "card2");

        pnlManageProducts_InvoicesIn.setLayout(new javax.swing.OverlayLayout(pnlManageProducts_InvoicesIn));

        srcManageProducts_InvoicesIn.setPreferredSize(new java.awt.Dimension(840, 250));

        tblManageProducts_InvoicesIn.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null,null, null, null, null, null},
                {null, null, null,null, null, null, null, null},
                {null, null, null,null, null, null, null, null},
                {null, null, null,null, null, null, null, null}
            },
            new String [] {
                "ID", "Empl ID","Product ID", "VendorID","Price","Quantity", "Date created","Total money"
            }
        ));
        srcManageProducts_InvoicesIn.setViewportView(tblManageProducts_InvoicesIn);

        pnlManageProducts_InvoicesIn.add(srcManageProducts_InvoicesIn);

        pnlManageProducts_View.add(pnlManageProducts_InvoicesIn, "card3");

        pnlManageProducts_Vendors.setLayout(new javax.swing.OverlayLayout(pnlManageProducts_Vendors));

        srcManageProducts_Vendors.setPreferredSize(new java.awt.Dimension(840, 250));

        tblManageProducts_Vendors.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                { null, null, null},
                { null, null, null},
                { null, null, null},
                { null, null, null}
            },
            new String [] {
                "Vendor ID", "Vendor Name", "Address"
            }
        ));
        srcManageProducts_Vendors.setViewportView(tblManageProducts_Vendors);

        pnlManageProducts_Vendors.add(srcManageProducts_Vendors);

        pnlManageProducts_View.add(pnlManageProducts_Vendors, "card2");

        pnlManageProducts_ViewMain.add(pnlManageProducts_View);
        pnlManageProducts_View.setBounds(110, 0, 840, 250);

        pnlMenu_SystemView.add(pnlManageProducts_ViewMain, "card3");

        pnlManageEmployees_ViewMain.setPreferredSize(new java.awt.Dimension(950, 250));
        pnlManageEmployees_ViewMain.setLayout(null);

        pnlManageEmployees_ChoicesMain.setOpaque(false);
        pnlManageEmployees_ChoicesMain.setPreferredSize(new java.awt.Dimension(140, 250));
        pnlManageEmployees_ChoicesMain.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        pnlManageEmployees_ChoicesMain.add(lblManageEmployee_Switch, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 70, 20, 70));

        pnlManageEmployees_Choices.setBackground(new java.awt.Color(255, 255, 255));
        pnlManageEmployees_Choices.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblManageEmployee_InvoicesOut.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 11)); // NOI18N
        lblManageEmployee_InvoicesOut.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblManageEmployee_InvoicesOut.setText("Bill");
        pnlManageEmployees_Choices.add(lblManageEmployee_InvoicesOut, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 150, 70, 43));

        lblManageEmployee_Information.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 11)); // NOI18N
        lblManageEmployee_Information.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblManageEmployee_Information.setText("Info");
        pnlManageEmployees_Choices.add(lblManageEmployee_Information, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 70, 43));

        lblManageEmployee_User.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 11)); // NOI18N
        lblManageEmployee_User.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblManageEmployee_User.setText("User");
        pnlManageEmployees_Choices.add(lblManageEmployee_User, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 80, 70, 43));

        pnlManageEmployees_ChoicesMain.add(pnlManageEmployees_Choices, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 110, 250));

        pnlManageEmployees_ViewMain.add(pnlManageEmployees_ChoicesMain);
        pnlManageEmployees_ChoicesMain.setBounds(0, 0, 150, 250);

        pnlManageEmployees_View.setPreferredSize(new java.awt.Dimension(840, 250));
        pnlManageEmployees_View.setLayout(new java.awt.CardLayout());

        pnlManageEmployee_Infomation.setPreferredSize(new java.awt.Dimension(840, 250));
        pnlManageEmployee_Infomation.setLayout(new javax.swing.OverlayLayout(pnlManageEmployee_Infomation));

        scrStatusEmployees.setPreferredSize(new java.awt.Dimension(840, 250));

        tblStatusEmployees.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Code", "Name", "Address", "Persional ID","Position","Avatar"
            }
        ));
        scrStatusEmployees.setViewportView(tblStatusEmployees);

        pnlManageEmployee_Infomation.add(scrStatusEmployees);

        pnlManageEmployees_View.add(pnlManageEmployee_Infomation, "card2");

        pnlManageEmployee_User.setPreferredSize(new java.awt.Dimension(840, 250));
        pnlManageEmployee_User.setLayout(new javax.swing.OverlayLayout(pnlManageEmployee_User));

        scrUser.setPreferredSize(new java.awt.Dimension(840, 250));

        tblUser.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Code Empl", "User Name", "PassWords", "Role","Avatar"
            }
        ));
        tblUser.setVerifyInputWhenFocusTarget(false);
        scrUser.setViewportView(tblUser);
        tblUser.getAccessibleContext().setAccessibleDescription("");

        pnlManageEmployee_User.add(scrUser);

        pnlManageEmployees_View.add(pnlManageEmployee_User, "card2");

        pnlManageEmployee_InvoicesOut.setLayout(new javax.swing.OverlayLayout(pnlManageEmployee_InvoicesOut));

        scrInvoicesOut.setPreferredSize(new java.awt.Dimension(840, 250));

        tblInvoicesOut.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Invoice ID", "Empl ID", "Date Created", "Product ID","Price","Quantity","Total"
            }
        ));
        scrInvoicesOut.setViewportView(tblInvoicesOut);

        pnlManageEmployee_InvoicesOut.add(scrInvoicesOut);

        pnlManageEmployees_View.add(pnlManageEmployee_InvoicesOut, "card3");

        pnlManageEmployees_ViewMain.add(pnlManageEmployees_View);
        pnlManageEmployees_View.setBounds(110, 0, 840, 250);

        pnlMenu_SystemView.add(pnlManageEmployees_ViewMain, "card3");

        pnlManageFinacial_ViewMain.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pnlManageFinacial_View.setPreferredSize(new java.awt.Dimension(950, 250));
        pnlManageFinacial_View.setLayout(new java.awt.CardLayout());

        tblManageFinacial.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Position ID", "Position", "Salary"
            }
        ));
        scrManageFinacial.setViewportView(tblManageFinacial);

        pnlManageFinacial_View.add(scrManageFinacial, "card2");

        pnlManageFinacial_ViewMain.add(pnlManageFinacial_View, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 950, 250));

        pnlMenu_SystemView.add(pnlManageFinacial_ViewMain, "card3");

        pnlMenu_System.add(pnlMenu_SystemView, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 950, 250));

        pnlMenu_SystemController.setLayout(new java.awt.CardLayout());

        pnlManageProducts_Controller.setLayout(new java.awt.CardLayout());

        pnlManageProducts_ProductController.setMinimumSize(new java.awt.Dimension(950, 250));
        pnlManageProducts_ProductController.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        pnlManageProducts_ProductController.add(txtProduct_ID, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 40, 70, 30));

        jLabel23.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel23.setText("Unit:");
        pnlManageProducts_ProductController.add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 40, 60, 30));

        jLabel28.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel28.setText("Product Name:");
        pnlManageProducts_ProductController.add(jLabel28, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 180, 100, 30));
        pnlManageProducts_ProductController.add(txtProduct_Name, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 180, 160, 30));

        jLabel31.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel31.setText("Vendor ID:");
        pnlManageProducts_ProductController.add(jLabel31, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 100, 30));
        pnlManageProducts_ProductController.add(txtProduct_VendorID, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 110, 50, 30));

        jLabel32.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel32.setText("Product ID:");
        pnlManageProducts_ProductController.add(jLabel32, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 100, 30));

        cbbProduct_Unit.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        cbbProduct_Unit.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        pnlManageProducts_ProductController.add(cbbProduct_Unit, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 40, 100, 30));

        btnProduct_New.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnProduct_New.setText("New");
        pnlManageProducts_ProductController.add(btnProduct_New, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 200, -1, -1));

        btnProduct_Delete.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnProduct_Delete.setText("Delete");
        pnlManageProducts_ProductController.add(btnProduct_Delete, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 200, -1, -1));

        btnProduct_Update.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnProduct_Update.setText("Update");
        pnlManageProducts_ProductController.add(btnProduct_Update, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 200, -1, -1));

        btnProduct_Insert.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnProduct_Insert.setText("Insert");
        pnlManageProducts_ProductController.add(btnProduct_Insert, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 200, -1, -1));

        btnProduct_Top.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnProduct_Top.setText("Top");
        pnlManageProducts_ProductController.add(btnProduct_Top, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 20, -1, -1));

        btnProduct_Bottom.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnProduct_Bottom.setText("Bottom");
        pnlManageProducts_ProductController.add(btnProduct_Bottom, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 20, -1, -1));

        btnProduct_Pre.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnProduct_Pre.setText("Preview");
        pnlManageProducts_ProductController.add(btnProduct_Pre, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 20, -1, -1));

        btnProduct_Next.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnProduct_Next.setText("Next");
        pnlManageProducts_ProductController.add(btnProduct_Next, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 20, -1, -1));
        pnlManageProducts_ProductController.add(txtProduct_Find, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 100, 220, 30));

        btnProduct_Find.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnProduct_Find.setText("Find");
        pnlManageProducts_ProductController.add(btnProduct_Find, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 100, 70, 30));

        jSeparator8.setOrientation(javax.swing.SwingConstants.VERTICAL);
        pnlManageProducts_ProductController.add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 0, 10, 250));
        pnlManageProducts_ProductController.add(txtProduct_Price, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 110, 160, 30));

        jLabel33.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel33.setText("Price:");
        pnlManageProducts_ProductController.add(jLabel33, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 110, 50, 30));

        jLabel30.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel30.setText("Quantity:");
        pnlManageProducts_ProductController.add(jLabel30, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 180, 60, 30));
        pnlManageProducts_ProductController.add(txtProduct_Quantity, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 180, 50, 30));

        buttonGroup6.add(rdoProduct_ProID);
        rdoProduct_ProID.setSelected(true);
        rdoProduct_ProID.setText("Product_ID");
        pnlManageProducts_ProductController.add(rdoProduct_ProID, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 150, -1, -1));

        buttonGroup6.add(rdoProduct_VendorID);
        rdoProduct_VendorID.setText("Vendor_ID");
        pnlManageProducts_ProductController.add(rdoProduct_VendorID, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 150, -1, -1));

        buttonGroup6.add(rdoProduct_Name);
        rdoProduct_Name.setText("Name");
        pnlManageProducts_ProductController.add(rdoProduct_Name, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 150, -1, -1));

        pnlManageProducts_Controller.add(pnlManageProducts_ProductController, "card2");

        pnlManageProducts_InvoicesInController.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        pnlManageProducts_InvoicesInController.add(txtInvoiceBought_VendorID, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 20, 70, 30));

        jLabel34.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel34.setText("Total Money:");
        pnlManageProducts_InvoicesInController.add(jLabel34, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 200, 80, 30));
        pnlManageProducts_InvoicesInController.add(txtInvoiceBought_TotalMoney, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 200, 140, 30));

        jLabel35.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel35.setText("Impl ID:");
        pnlManageProducts_InvoicesInController.add(jLabel35, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 60, 50, 30));
        pnlManageProducts_InvoicesInController.add(txtInvoiceBought_ImplID, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 60, 70, 30));

        jLabel36.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel36.setText("Date Created:");
        pnlManageProducts_InvoicesInController.add(jLabel36, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 100, 90, 30));
        pnlManageProducts_InvoicesInController.add(txtInvoiceBought_DateCreated, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 100, 100, 30));

        jLabel37.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel37.setText("Vendor ID:");
        pnlManageProducts_InvoicesInController.add(jLabel37, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 20, 90, 30));

        btnInvoiceBought_New.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnInvoiceBought_New.setText("New");
        pnlManageProducts_InvoicesInController.add(btnInvoiceBought_New, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 200, -1, -1));

        btnInvoiceBought_Delete.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnInvoiceBought_Delete.setText("Delete");
        pnlManageProducts_InvoicesInController.add(btnInvoiceBought_Delete, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 200, -1, -1));

        btnInvoiceBought_Update.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnInvoiceBought_Update.setText("Update");
        pnlManageProducts_InvoicesInController.add(btnInvoiceBought_Update, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 200, -1, -1));

        btnInvoiceBought_Insert.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnInvoiceBought_Insert.setText("Insert");
        pnlManageProducts_InvoicesInController.add(btnInvoiceBought_Insert, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 200, -1, -1));

        btnInvoiceBought_Top.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnInvoiceBought_Top.setText("Top");
        pnlManageProducts_InvoicesInController.add(btnInvoiceBought_Top, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 20, -1, -1));

        btnInvoiceBought_Bottom.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnInvoiceBought_Bottom.setText("Bottom");
        pnlManageProducts_InvoicesInController.add(btnInvoiceBought_Bottom, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 20, -1, -1));

        btnInvoiceBought_Pre.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnInvoiceBought_Pre.setText("Preview");
        pnlManageProducts_InvoicesInController.add(btnInvoiceBought_Pre, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 20, -1, -1));

        btnInvoiceBought_Next.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnInvoiceBought_Next.setText("Next");
        pnlManageProducts_InvoicesInController.add(btnInvoiceBought_Next, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 20, -1, -1));
        pnlManageProducts_InvoicesInController.add(txtInvoicebought_Find, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 100, 220, 30));

        btnInvoiceBought_Find.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnInvoiceBought_Find.setText("Find");
        pnlManageProducts_InvoicesInController.add(btnInvoiceBought_Find, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 100, 70, 30));

        jSeparator6.setOrientation(javax.swing.SwingConstants.VERTICAL);
        pnlManageProducts_InvoicesInController.add(jSeparator6, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 0, 10, 250));
        pnlManageProducts_InvoicesInController.add(txtInvoiceBought_ID1, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 20, 70, 30));

        jLabel38.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel38.setText("Invoice ID bought:");
        pnlManageProducts_InvoicesInController.add(jLabel38, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 120, 30));

        jLabel46.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel46.setText("Product ID:");
        pnlManageProducts_InvoicesInController.add(jLabel46, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 70, 90, 30));
        pnlManageProducts_InvoicesInController.add(txtInvoiceBought_ProductID, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 70, 70, 30));

        jLabel47.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel47.setText("Price :");
        pnlManageProducts_InvoicesInController.add(jLabel47, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 120, 80, 30));
        pnlManageProducts_InvoicesInController.add(txtInvoiceBought_Price, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 120, 70, 30));

        jLabel48.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel48.setText("Quantity:");
        pnlManageProducts_InvoicesInController.add(jLabel48, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 150, 60, 30));
        pnlManageProducts_InvoicesInController.add(txtInvoiceBought_Quantity, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 150, 70, 30));

        buttonGroup7.add(rdoInvoiceIn_InvoiceID);
        rdoInvoiceIn_InvoiceID.setSelected(true);
        rdoInvoiceIn_InvoiceID.setText("Invoice_ID");
        rdoInvoiceIn_InvoiceID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdoInvoiceIn_InvoiceIDActionPerformed(evt);
            }
        });
        pnlManageProducts_InvoicesInController.add(rdoInvoiceIn_InvoiceID, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 150, -1, -1));

        buttonGroup7.add(rdoInvoiceIn_VendorID);
        rdoInvoiceIn_VendorID.setText("Vendor_ID");
        pnlManageProducts_InvoicesInController.add(rdoInvoiceIn_VendorID, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 150, -1, -1));

        buttonGroup7.add(rdoInvoiceIn_ProductID);
        rdoInvoiceIn_ProductID.setText("Product_ID");
        pnlManageProducts_InvoicesInController.add(rdoInvoiceIn_ProductID, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 150, -1, -1));

        buttonGroup7.add(rdoInvoiceIn_EmplID);
        rdoInvoiceIn_EmplID.setText("Empl_ID");
        pnlManageProducts_InvoicesInController.add(rdoInvoiceIn_EmplID, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 150, -1, -1));

        pnlManageProducts_Controller.add(pnlManageProducts_InvoicesInController, "card3");

        pnlManageProducts_VendorsController.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        pnlManageProducts_VendorsController.add(txtVendor_ID, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 30, 70, 30));

        jLabel26.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel26.setText("Vendor Name:");
        pnlManageProducts_VendorsController.add(jLabel26, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, 100, 30));
        pnlManageProducts_VendorsController.add(txtVendor_Name, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 90, 180, 30));

        jLabel27.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel27.setText("Address:");
        pnlManageProducts_VendorsController.add(jLabel27, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 160, 100, 30));

        txtVendor_Address.setColumns(20);
        txtVendor_Address.setRows(5);
        jScrollPane2.setViewportView(txtVendor_Address);

        pnlManageProducts_VendorsController.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 160, 180, 70));

        jLabel29.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel29.setText("Vendor ID:");
        pnlManageProducts_VendorsController.add(jLabel29, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 100, 30));

        lblEmloyee_Avatar1.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        pnlManageProducts_VendorsController.add(lblEmloyee_Avatar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 50, 100, 120));

        btnVendor_New.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnVendor_New.setText("New");
        btnVendor_New.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVendor_NewActionPerformed(evt);
            }
        });
        pnlManageProducts_VendorsController.add(btnVendor_New, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 200, -1, -1));

        btnVendor_Delete.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnVendor_Delete.setText("Delete");
        pnlManageProducts_VendorsController.add(btnVendor_Delete, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 200, -1, -1));

        btnVendor_Update.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnVendor_Update.setText("Update");
        pnlManageProducts_VendorsController.add(btnVendor_Update, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 200, -1, -1));

        btnVendor_Insert.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnVendor_Insert.setText("Insert");
        btnVendor_Insert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVendor_InsertActionPerformed(evt);
            }
        });
        pnlManageProducts_VendorsController.add(btnVendor_Insert, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 200, -1, -1));

        btnVendor_Top.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnVendor_Top.setText("Top");
        pnlManageProducts_VendorsController.add(btnVendor_Top, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 20, -1, -1));

        btnVendor_Bottom.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnVendor_Bottom.setText("Bottom");
        pnlManageProducts_VendorsController.add(btnVendor_Bottom, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 20, -1, -1));

        btnVendor_Pre.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnVendor_Pre.setText("Preview");
        pnlManageProducts_VendorsController.add(btnVendor_Pre, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 20, -1, -1));

        btnVendor_Next.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnVendor_Next.setText("Next");
        pnlManageProducts_VendorsController.add(btnVendor_Next, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 20, -1, -1));
        pnlManageProducts_VendorsController.add(txtVendor_FInd, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 100, 220, 30));

        btnVendor_Find.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnVendor_Find.setText("Find");
        pnlManageProducts_VendorsController.add(btnVendor_Find, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 100, 70, 30));

        jSeparator9.setOrientation(javax.swing.SwingConstants.VERTICAL);
        pnlManageProducts_VendorsController.add(jSeparator9, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 0, 10, 240));

        buttonGroup5.add(rdoVendor_VendorID);
        rdoVendor_VendorID.setSelected(true);
        rdoVendor_VendorID.setText("Vendor_ID");
        pnlManageProducts_VendorsController.add(rdoVendor_VendorID, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 150, -1, -1));

        buttonGroup5.add(rdoVendor_Name);
        rdoVendor_Name.setText("Name");
        pnlManageProducts_VendorsController.add(rdoVendor_Name, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 150, -1, -1));

        buttonGroup5.add(rdoVendor_Address);
        rdoVendor_Address.setText("Address");
        pnlManageProducts_VendorsController.add(rdoVendor_Address, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 150, -1, -1));

        pnlManageProducts_Controller.add(pnlManageProducts_VendorsController, "card2");

        pnlMenu_SystemController.add(pnlManageProducts_Controller, "card3");

        pnlManageEmployees_Controller.setLayout(new java.awt.CardLayout());

        pnlManageEmployees_InfomationController.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel8.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel8.setText("Avatar:");
        pnlManageEmployees_InfomationController.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 90, 50, 30));
        pnlManageEmployees_InfomationController.add(txtEmployee_ID, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 10, 70, 30));

        jLabel10.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel10.setText("Name:");
        pnlManageEmployees_InfomationController.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 50, 40, 30));
        pnlManageEmployees_InfomationController.add(txtEmployee_Name, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 50, 180, 30));

        jLabel12.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel12.setText("Address:");
        pnlManageEmployees_InfomationController.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 170, 60, 30));

        jLabel13.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel13.setText("Personal ID:");
        pnlManageEmployees_InfomationController.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 90, 80, 30));
        pnlManageEmployees_InfomationController.add(txtEployee_PersonalID, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 90, 180, 30));

        txtEmployee_Address.setColumns(20);
        txtEmployee_Address.setRows(5);
        jScrollPane1.setViewportView(txtEmployee_Address);

        pnlManageEmployees_InfomationController.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 170, 180, 70));

        jLabel11.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel11.setText("Empl ID:");
        pnlManageEmployees_InfomationController.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 10, 60, 30));

        cbbEmployee_Position.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        pnlManageEmployees_InfomationController.add(cbbEmployee_Position, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 130, 100, 30));
        pnlManageEmployees_InfomationController.add(lblEmloyee_Avatar, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 100, 100, 120));

        jLabel15.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel15.setText("Position:");
        pnlManageEmployees_InfomationController.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 130, 50, 30));

        btnEmploye_New.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnEmploye_New.setText("New");
        pnlManageEmployees_InfomationController.add(btnEmploye_New, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 200, -1, -1));

        btnEmployee_Delete.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnEmployee_Delete.setText("Delete");
        pnlManageEmployees_InfomationController.add(btnEmployee_Delete, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 200, -1, -1));

        btnEmployee_Update.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnEmployee_Update.setText("Update");
        pnlManageEmployees_InfomationController.add(btnEmployee_Update, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 200, -1, -1));

        btnEmployee_Insert.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnEmployee_Insert.setText("Insert");
        pnlManageEmployees_InfomationController.add(btnEmployee_Insert, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 200, -1, -1));

        btnEmployee_Top.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnEmployee_Top.setText("Top");
        pnlManageEmployees_InfomationController.add(btnEmployee_Top, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 20, -1, -1));

        btnEmployee_Bottom.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnEmployee_Bottom.setText("Bottom");
        pnlManageEmployees_InfomationController.add(btnEmployee_Bottom, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 20, -1, -1));

        btnEmployee_Pre.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnEmployee_Pre.setText("Preview");
        pnlManageEmployees_InfomationController.add(btnEmployee_Pre, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 20, -1, -1));

        btnEmployee_Next.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnEmployee_Next.setText("Next");
        pnlManageEmployees_InfomationController.add(btnEmployee_Next, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 20, -1, -1));
        pnlManageEmployees_InfomationController.add(txtEmployee_Find, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 100, 220, 30));

        btnEmloyee_Find.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnEmloyee_Find.setText("Find");
        pnlManageEmployees_InfomationController.add(btnEmloyee_Find, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 100, 70, 30));

        jSeparator5.setOrientation(javax.swing.SwingConstants.VERTICAL);
        pnlManageEmployees_InfomationController.add(jSeparator5, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 0, 10, 250));

        buttonGroup4.add(rdoPersonal_EmplID);
        rdoPersonal_EmplID.setSelected(true);
        rdoPersonal_EmplID.setText("Empl_ID");
        pnlManageEmployees_InfomationController.add(rdoPersonal_EmplID, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 150, -1, -1));

        buttonGroup4.add(rdoPersonal_Name);
        rdoPersonal_Name.setText("Name");
        pnlManageEmployees_InfomationController.add(rdoPersonal_Name, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 150, -1, -1));

        buttonGroup4.add(rdoPersonal_PersonID);
        rdoPersonal_PersonID.setText("Personal_ID");
        pnlManageEmployees_InfomationController.add(rdoPersonal_PersonID, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 150, -1, -1));

        buttonGroup4.add(rdoPersonal_Position);
        rdoPersonal_Position.setText("Position");
        pnlManageEmployees_InfomationController.add(rdoPersonal_Position, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 150, 70, -1));

        pnlManageEmployees_Controller.add(pnlManageEmployees_InfomationController, "card2");

        pnlManageEmployees_UserController.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel14.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel14.setText("Avatar");
        pnlManageEmployees_UserController.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 70, 50, 30));
        pnlManageEmployees_UserController.add(txtUser_EmplID, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 30, 70, 30));

        jLabel17.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel17.setText("User Name:");
        pnlManageEmployees_UserController.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, 100, 30));
        pnlManageEmployees_UserController.add(txtUser_Username, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 90, 180, 30));

        jLabel19.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel19.setText("Password:");
        pnlManageEmployees_UserController.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, 100, 30));

        jLabel20.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel20.setText("Empl ID:");
        pnlManageEmployees_UserController.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 100, 30));

        cbbUser_role.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        cbbUser_role.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        pnlManageEmployees_UserController.add(cbbUser_role, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 10, 100, 30));

        lblUser_Avatar.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        lblUser_Avatar.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, java.awt.Color.darkGray, null));
        lblUser_Avatar.setRequestFocusEnabled(false);
        pnlManageEmployees_UserController.add(lblUser_Avatar, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 110, 100, 120));

        jLabel21.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel21.setText("Role:");
        pnlManageEmployees_UserController.add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 10, 50, 30));

        btnUser_New.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnUser_New.setText("New");
        pnlManageEmployees_UserController.add(btnUser_New, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 200, -1, -1));

        btnUser_Delete.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnUser_Delete.setText("Delete");
        pnlManageEmployees_UserController.add(btnUser_Delete, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 200, -1, -1));

        btnUser_Update.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnUser_Update.setText("Update");
        pnlManageEmployees_UserController.add(btnUser_Update, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 200, -1, -1));

        btnUser_Insert.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnUser_Insert.setText("Insert");
        pnlManageEmployees_UserController.add(btnUser_Insert, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 200, -1, -1));

        btnUser_Top.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnUser_Top.setText("Top");
        pnlManageEmployees_UserController.add(btnUser_Top, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 20, -1, -1));

        btnUser_Bottom.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnUser_Bottom.setText("Bottom");
        pnlManageEmployees_UserController.add(btnUser_Bottom, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 20, -1, -1));

        btnUser_Pre.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnUser_Pre.setText("Preview");
        pnlManageEmployees_UserController.add(btnUser_Pre, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 20, -1, -1));

        btnUser_Next.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnUser_Next.setText("Next");
        pnlManageEmployees_UserController.add(btnUser_Next, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 20, -1, -1));
        pnlManageEmployees_UserController.add(txtUser_Find, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 100, 220, 30));

        btnUser_Find.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnUser_Find.setText("Find");
        pnlManageEmployees_UserController.add(btnUser_Find, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 100, 70, 30));

        txtUser_Password.setText("jPasswordField1");
        pnlManageEmployees_UserController.add(txtUser_Password, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 150, 180, 30));

        jSeparator4.setOrientation(javax.swing.SwingConstants.VERTICAL);
        pnlManageEmployees_UserController.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 0, 10, 250));

        jLabel42.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel42.setText("Confirm :");
        pnlManageEmployees_UserController.add(jLabel42, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 200, 100, 30));

        txtUser_Confirm.setText("jPasswordField1");
        pnlManageEmployees_UserController.add(txtUser_Confirm, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 200, 180, 30));

        buttonGroup1.add(rdoUser_EmplID);
        rdoUser_EmplID.setSelected(true);
        rdoUser_EmplID.setText("Empl_ID");
        pnlManageEmployees_UserController.add(rdoUser_EmplID, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 150, -1, -1));

        buttonGroup1.add(rdoUsername);
        rdoUsername.setText("Username");
        pnlManageEmployees_UserController.add(rdoUsername, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 150, -1, -1));

        buttonGroup1.add(rdoRole);
        rdoRole.setText("Role");
        pnlManageEmployees_UserController.add(rdoRole, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 150, -1, -1));

        pnlManageEmployees_Controller.add(pnlManageEmployees_UserController, "card2");

        pnlManageEmployees_InvoicesOutController.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        pnlManageEmployees_InvoicesOutController.add(txtInvoiceSold_ID, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 20, 70, 30));

        jLabel18.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel18.setText("Total Money:");
        pnlManageEmployees_InvoicesOutController.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 200, 90, 30));
        pnlManageEmployees_InvoicesOutController.add(txtInvoiceSold_TotalMoney, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 200, 120, 30));

        jLabel22.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel22.setText("Impl ID:");
        pnlManageEmployees_InvoicesOutController.add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 80, 60, 30));
        pnlManageEmployees_InvoicesOutController.add(txtInvoiceSold_ImplID, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 80, 70, 30));

        jLabel24.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel24.setText("Date Created:");
        pnlManageEmployees_InvoicesOutController.add(jLabel24, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, 100, 30));
        pnlManageEmployees_InvoicesOutController.add(txtInvoiceSold_DateCreated, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 140, 110, 30));

        jLabel25.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel25.setText("Invoice ID sold:");
        pnlManageEmployees_InvoicesOutController.add(jLabel25, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 100, 30));

        btnInvoiceSold_New.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnInvoiceSold_New.setText("New");
        pnlManageEmployees_InvoicesOutController.add(btnInvoiceSold_New, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 200, -1, -1));

        btnInvoiceSold_Delete.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnInvoiceSold_Delete.setText("Delete");
        pnlManageEmployees_InvoicesOutController.add(btnInvoiceSold_Delete, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 200, -1, -1));

        btnInvoiceSold_Update.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnInvoiceSold_Update.setText("Update");
        pnlManageEmployees_InvoicesOutController.add(btnInvoiceSold_Update, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 200, -1, -1));

        btnInvoiceSold_Insert.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnInvoiceSold_Insert.setText("Insert");
        pnlManageEmployees_InvoicesOutController.add(btnInvoiceSold_Insert, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 200, -1, -1));

        btnInvoiceSold_Top.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnInvoiceSold_Top.setText("Top");
        pnlManageEmployees_InvoicesOutController.add(btnInvoiceSold_Top, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 20, -1, -1));

        btnInvoiceSold_Bottom.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnInvoiceSold_Bottom.setText("Bottom");
        pnlManageEmployees_InvoicesOutController.add(btnInvoiceSold_Bottom, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 20, -1, -1));

        btnInvoiceSold_Pre.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnInvoiceSold_Pre.setText("Preview");
        pnlManageEmployees_InvoicesOutController.add(btnInvoiceSold_Pre, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 20, -1, -1));

        btnInvoiceSold_Next.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnInvoiceSold_Next.setText("Next");
        pnlManageEmployees_InvoicesOutController.add(btnInvoiceSold_Next, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 20, -1, -1));
        pnlManageEmployees_InvoicesOutController.add(txtInvoiceSold_Find, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 100, 220, 30));

        btnInvoiceSold_Find.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnInvoiceSold_Find.setText("Find");
        pnlManageEmployees_InvoicesOutController.add(btnInvoiceSold_Find, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 100, 70, 30));

        jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);
        pnlManageEmployees_InvoicesOutController.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 0, 10, 250));

        jLabel43.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel43.setText("Product ID :");
        pnlManageEmployees_InvoicesOutController.add(jLabel43, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 20, -1, 30));
        pnlManageEmployees_InvoicesOutController.add(txtInvoiceSold_ProductID, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 20, 70, 30));
        pnlManageEmployees_InvoicesOutController.add(txtInvoiceSold_Quantity, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 80, 70, 30));

        jLabel44.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel44.setText("Quantity:");
        pnlManageEmployees_InvoicesOutController.add(jLabel44, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 80, 60, 30));

        jLabel45.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel45.setText("Price:");
        pnlManageEmployees_InvoicesOutController.add(jLabel45, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 140, 40, 30));
        pnlManageEmployees_InvoicesOutController.add(txtInvoiceSold_Price, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 140, 70, 30));

        buttonGroup3.add(rdoInvoiceOut_InvoiceID);
        rdoInvoiceOut_InvoiceID.setSelected(true);
        rdoInvoiceOut_InvoiceID.setText("Invoice_ID");
        pnlManageEmployees_InvoicesOutController.add(rdoInvoiceOut_InvoiceID, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 150, -1, -1));

        buttonGroup3.add(rdoInvoiceOut_ProID);
        rdoInvoiceOut_ProID.setText("Product_ID");
        pnlManageEmployees_InvoicesOutController.add(rdoInvoiceOut_ProID, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 150, -1, -1));

        buttonGroup3.add(rdoInvoieOut_EmplID);
        rdoInvoieOut_EmplID.setText("Empl_ID");
        pnlManageEmployees_InvoicesOutController.add(rdoInvoieOut_EmplID, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 150, -1, -1));

        pnlManageEmployees_Controller.add(pnlManageEmployees_InvoicesOutController, "card3");

        pnlMenu_SystemController.add(pnlManageEmployees_Controller, "card3");

        pnlManageFinacial_Controller.setPreferredSize(new java.awt.Dimension(950, 250));
        pnlManageFinacial_Controller.setLayout(new java.awt.CardLayout());

        pnlFinacialControll.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        pnlFinacialControll.add(txtIFinalcial_PositionID, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 40, 70, 30));

        jLabel39.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel39.setText("Position :");
        pnlFinacialControll.add(jLabel39, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, 100, 30));
        pnlFinacialControll.add(txtFinacial_Position, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 100, 180, 30));

        jLabel40.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel40.setText("Salary :");
        pnlFinacialControll.add(jLabel40, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 160, 100, 30));
        pnlFinacialControll.add(txtIFinacial_Salary, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 160, 180, 30));

        jLabel41.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        jLabel41.setText("Position ID: ");
        pnlFinacialControll.add(jLabel41, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 100, 30));

        btnFinacial_New.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnFinacial_New.setText("New");
        pnlFinacialControll.add(btnFinacial_New, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 200, -1, -1));

        btnFinacial_Delete.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnFinacial_Delete.setText("Delete");
        pnlFinacialControll.add(btnFinacial_Delete, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 200, -1, -1));

        btnFinacial_Update.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnFinacial_Update.setText("Update");
        pnlFinacialControll.add(btnFinacial_Update, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 200, -1, -1));

        btnFinacial_Insert.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnFinacial_Insert.setText("Insert");
        pnlFinacialControll.add(btnFinacial_Insert, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 200, -1, -1));

        btnFinacial_Top.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnFinacial_Top.setText("Top");
        pnlFinacialControll.add(btnFinacial_Top, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 20, -1, -1));

        btnFinacial_Bottom.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnFinacial_Bottom.setText("Bottom");
        pnlFinacialControll.add(btnFinacial_Bottom, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 20, -1, -1));

        btnFinacial_Pre.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnFinacial_Pre.setText("Preview");
        pnlFinacialControll.add(btnFinacial_Pre, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 20, -1, -1));

        btnFinacial_Next.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnFinacial_Next.setText("Next");
        pnlFinacialControll.add(btnFinacial_Next, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 20, -1, -1));
        pnlFinacialControll.add(txtFianancial_Find, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 100, 220, 30));

        btnFinacial_Find.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
        btnFinacial_Find.setText("Find");
        pnlFinacialControll.add(btnFinacial_Find, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 100, 70, 30));

        jSeparator7.setOrientation(javax.swing.SwingConstants.VERTICAL);
        pnlFinacialControll.add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 0, 10, 250));

        buttonGroup8.add(rdoFinancial_PosiID);
        rdoFinancial_PosiID.setSelected(true);
        rdoFinancial_PosiID.setText("Position_ID");
        pnlFinacialControll.add(rdoFinancial_PosiID, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 150, -1, -1));

        buttonGroup8.add(rdoFinancial_Posi);
        rdoFinancial_Posi.setText("Position");
        pnlFinacialControll.add(rdoFinancial_Posi, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 150, -1, -1));

        pnlManageFinacial_Controller.add(pnlFinacialControll, "card2");

        pnlMenu_SystemController.add(pnlManageFinacial_Controller, "card3");

        pnlMenu_System.add(pnlMenu_SystemController, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 250, 950, 250));

        pnlStatitis_View.setLayout(null);

        pnlStatictis_InvoiceOut.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        invoiceOutGroup.add(rdbStatictis_InvoiceOut_All);
        rdbStatictis_InvoiceOut_All.setText("All");
        pnlStatictis_InvoiceOut.add(rdbStatictis_InvoiceOut_All, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 400, -1, -1));

        invoiceOutGroup.add(rdbStatictis_InvoiceOut_Month);
        rdbStatictis_InvoiceOut_Month.setText("Month");
        pnlStatictis_InvoiceOut.add(rdbStatictis_InvoiceOut_Month, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 400, -1, -1));

        cbbStatictis_InvoiceOutl.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" }));
        pnlStatictis_InvoiceOut.add(cbbStatictis_InvoiceOutl, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 400, 110, -1));

        tblStatictis_InvoiceOut.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(tblStatictis_InvoiceOut);

        pnlStatictis_InvoiceOut.add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 380, 380));

        pnlStatictis_Table.addTab("InvoiceOut", pnlStatictis_InvoiceOut);

        pnlStatictis_InvoiceIn.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        invoiceInGroup.add(rdbStatictis_InvoiceIn_All);
        rdbStatictis_InvoiceIn_All.setText("All");
        pnlStatictis_InvoiceIn.add(rdbStatictis_InvoiceIn_All, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 390, -1, -1));

        invoiceInGroup.add(rdbStatictis_InvoiceIn_Month);
        rdbStatictis_InvoiceIn_Month.setText("Month");
        pnlStatictis_InvoiceIn.add(rdbStatictis_InvoiceIn_Month, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 390, -1, -1));

        tblStatictis_InvoiceIn.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane6.setViewportView(tblStatictis_InvoiceIn);

        pnlStatictis_InvoiceIn.add(jScrollPane6, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 380, 380));

        cbbStatictis_InvoiceIn.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" }));
        pnlStatictis_InvoiceIn.add(cbbStatictis_InvoiceIn, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 390, 110, -1));

        pnlStatictis_Table.addTab("InvoiceIn", pnlStatictis_InvoiceIn);

        pnlStatictis_Finacial.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        finalcial.add(rdbStatictis_Finacial_All);
        rdbStatictis_Finacial_All.setText("All");
        pnlStatictis_Finacial.add(rdbStatictis_Finacial_All, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 400, -1, -1));

        finalcial.add(rdbStatictis_Finalcial_Month);
        rdbStatictis_Finalcial_Month.setText("Month");
        pnlStatictis_Finacial.add(rdbStatictis_Finalcial_Month, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 400, -1, -1));

        tblStatictis_Finacial_report.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane7.setViewportView(tblStatictis_Finacial_report);

        pnlStatictis_Finacial.add(jScrollPane7, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 380, 380));

        cbbStatictis_Finacial.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" }));
        pnlStatictis_Finacial.add(cbbStatictis_Finacial, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 400, 110, -1));

        pnlStatictis_Table.addTab("Finacial", pnlStatictis_Finacial);

        pnlStatitis_View.add(pnlStatictis_Table);
        pnlStatictis_Table.setBounds(0, 0, 385, 500);

        pnlStatictis_Chart.setLayout(null);
        pnlStatitis_View.add(pnlStatictis_Chart);
        pnlStatictis_Chart.setBounds(390, 0, 560, 500);

        pnlMenu_System.add(pnlStatitis_View, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 950, 500));

        pnlMenuMain.add(pnlMenu_System);
        pnlMenu_System.setBounds(-950, 30, 950, 500);

        pnlFrame.add(pnlMenuMain);
        pnlMenuMain.setBounds(0, 0, 700, 530);

        pnlMenuUser.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pnlLogin.setBackground(new java.awt.Color(255, 255, 255));
        pnlLogin.setLayout(null);

        txtUser.setBackground(new java.awt.Color(255, 255, 255));
        txtUser.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        txtUser.setForeground(new java.awt.Color(102, 102, 102));
        txtUser.setText("admin");
        txtUser.setBorder(null);
        txtUser.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        pnlLogin.add(txtUser);
        txtUser.setBounds(70, 160, 140, 30);

        txtPass.setBackground(new java.awt.Color(255, 255, 255));
        txtPass.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        txtPass.setForeground(new java.awt.Color(102, 102, 102));
        txtPass.setText("1234567");
        txtPass.setBorder(null);
        txtPass.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        pnlLogin.add(txtPass);
        txtPass.setBounds(70, 220, 140, 30);

        btnLogin.setBackground(new java.awt.Color(255, 51, 0));
        btnLogin.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnLogin.setForeground(new java.awt.Color(255, 255, 255));
        btnLogin.setText("LOGIN");
        btnLogin.setBorder(null);
        btnLogin.setBorderPainted(false);
        pnlLogin.add(btnLogin);
        btnLogin.setBounds(60, 300, 105, 44);

        lblFogot.setForeground(new java.awt.Color(51, 51, 255));
        lblFogot.setText("<html><a href=\"#\" ><i>Forgot password?</i></a></html>");
        pnlLogin.add(lblFogot);
        lblFogot.setBounds(110, 370, 130, 15);

        lblIconUser.setText("ico");
        lblIconUser.setPreferredSize(new java.awt.Dimension(20, 20));
        pnlLogin.add(lblIconUser);
        lblIconUser.setBounds(20, 160, 30, 30);

        lblIconPass.setText("ico");
        lblIconPass.setPreferredSize(new java.awt.Dimension(20, 20));
        pnlLogin.add(lblIconPass);
        lblIconPass.setBounds(20, 220, 30, 30);

        lblWeb.setText("web");
        pnlLogin.add(lblWeb);
        lblWeb.setBounds(10, 50, 30, 30);

        lblMedia.setText("ms");
        pnlLogin.add(lblMedia);
        lblMedia.setBounds(10, 90, 30, 30);

        lblShowMenu.setText("menu");
        pnlLogin.add(lblShowMenu);
        lblShowMenu.setBounds(10, 10, 30, 30);
        pnlLogin.add(jSeparator1);
        jSeparator1.setBounds(70, 250, 140, 10);
        pnlLogin.add(jSeparator2);
        jSeparator2.setBounds(70, 190, 140, 10);

        pnlMenuUser.add(pnlLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 250, 530));

        pnlFrame.add(pnlMenuUser);
        pnlMenuUser.setBounds(-250, 0, 250, 530);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlFrame, javax.swing.GroupLayout.PREFERRED_SIZE, 950, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlFrame, javax.swing.GroupLayout.PREFERRED_SIZE, 530, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnVendor_NewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVendor_NewActionPerformed

    }//GEN-LAST:event_btnVendor_NewActionPerformed

    private void btnVendor_InsertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVendor_InsertActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnVendor_InsertActionPerformed

    private void rdoInvoiceIn_InvoiceIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdoInvoiceIn_InvoiceIDActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rdoInvoiceIn_InvoiceIDActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JFrame FrameAbout;
    private javax.swing.JButton btnEmloyee_Find;
    private javax.swing.JButton btnEmploye_New;
    private javax.swing.JButton btnEmployee_Bottom;
    private javax.swing.JButton btnEmployee_Delete;
    private javax.swing.JButton btnEmployee_Insert;
    private javax.swing.JButton btnEmployee_Next;
    private javax.swing.JButton btnEmployee_Pre;
    private javax.swing.JButton btnEmployee_Top;
    private javax.swing.JButton btnEmployee_Update;
    private javax.swing.JButton btnFinacial_Bottom;
    private javax.swing.JButton btnFinacial_Delete;
    private javax.swing.JButton btnFinacial_Find;
    private javax.swing.JButton btnFinacial_Insert;
    private javax.swing.JButton btnFinacial_New;
    private javax.swing.JButton btnFinacial_Next;
    private javax.swing.JButton btnFinacial_Pre;
    private javax.swing.JButton btnFinacial_Top;
    private javax.swing.JButton btnFinacial_Update;
    private javax.swing.JButton btnInvoiceBought_Bottom;
    private javax.swing.JButton btnInvoiceBought_Delete;
    private javax.swing.JButton btnInvoiceBought_Find;
    private javax.swing.JButton btnInvoiceBought_Insert;
    private javax.swing.JButton btnInvoiceBought_New;
    private javax.swing.JButton btnInvoiceBought_Next;
    private javax.swing.JButton btnInvoiceBought_Pre;
    private javax.swing.JButton btnInvoiceBought_Top;
    private javax.swing.JButton btnInvoiceBought_Update;
    private javax.swing.JButton btnInvoiceSold_Bottom;
    private javax.swing.JButton btnInvoiceSold_Delete;
    private javax.swing.JButton btnInvoiceSold_Find;
    private javax.swing.JButton btnInvoiceSold_Insert;
    private javax.swing.JButton btnInvoiceSold_New;
    private javax.swing.JButton btnInvoiceSold_Next;
    private javax.swing.JButton btnInvoiceSold_Pre;
    private javax.swing.JButton btnInvoiceSold_Top;
    private javax.swing.JButton btnInvoiceSold_Update;
    private javax.swing.JButton btnLogin;
    private javax.swing.JButton btnProduct_Bottom;
    private javax.swing.JButton btnProduct_Delete;
    private javax.swing.JButton btnProduct_Find;
    private javax.swing.JButton btnProduct_Insert;
    private javax.swing.JButton btnProduct_New;
    private javax.swing.JButton btnProduct_Next;
    private javax.swing.JButton btnProduct_Pre;
    private javax.swing.JButton btnProduct_Top;
    private javax.swing.JButton btnProduct_Update;
    private javax.swing.JButton btnUser_Bottom;
    private javax.swing.JButton btnUser_Delete;
    private javax.swing.JButton btnUser_Find;
    private javax.swing.JButton btnUser_Insert;
    private javax.swing.JButton btnUser_New;
    private javax.swing.JButton btnUser_Next;
    private javax.swing.JButton btnUser_Pre;
    private javax.swing.JButton btnUser_Top;
    private javax.swing.JButton btnUser_Update;
    private javax.swing.JButton btnVendor_Bottom;
    private javax.swing.JButton btnVendor_Delete;
    private javax.swing.JButton btnVendor_Find;
    private javax.swing.JButton btnVendor_Insert;
    private javax.swing.JButton btnVendor_New;
    private javax.swing.JButton btnVendor_Next;
    private javax.swing.JButton btnVendor_Pre;
    private javax.swing.JButton btnVendor_Top;
    private javax.swing.JButton btnVendor_Update;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.ButtonGroup buttonGroup4;
    private javax.swing.ButtonGroup buttonGroup5;
    private javax.swing.ButtonGroup buttonGroup6;
    private javax.swing.ButtonGroup buttonGroup7;
    private javax.swing.ButtonGroup buttonGroup8;
    private javax.swing.JComboBox<String> cbbEmployee_Position;
    private javax.swing.JComboBox<String> cbbProduct_Unit;
    private javax.swing.JComboBox<String> cbbStatictis_Finacial;
    private javax.swing.JComboBox<String> cbbStatictis_InvoiceIn;
    private javax.swing.JComboBox<String> cbbStatictis_InvoiceOutl;
    private javax.swing.JComboBox<String> cbbUser_role;
    private javax.swing.ButtonGroup finalcial;
    private javax.swing.ButtonGroup invoiceInGroup;
    private javax.swing.ButtonGroup invoiceOutGroup;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JLabel lblEmloyee_Avatar;
    private javax.swing.JLabel lblEmloyee_Avatar1;
    private javax.swing.JLabel lblExit;
    private javax.swing.JLabel lblFogot;
    private javax.swing.JLabel lblHidden;
    private javax.swing.JLabel lblIconPass;
    private javax.swing.JLabel lblIconUser;
    private javax.swing.JLabel lblManageEmployee_Information;
    private javax.swing.JLabel lblManageEmployee_InvoicesOut;
    private javax.swing.JLabel lblManageEmployee_Switch;
    private javax.swing.JLabel lblManageEmployee_User;
    private javax.swing.JLabel lblManageProducts_InvoiceIn;
    private javax.swing.JLabel lblManageProducts_Product;
    private javax.swing.JLabel lblManageProducts_Switch;
    private javax.swing.JLabel lblManageProducts_Vendors;
    private javax.swing.JLabel lblMedia;
    private javax.swing.JLabel lblMenuStart;
    private javax.swing.JLabel lblMenu_About;
    private javax.swing.JLabel lblMenu_Help;
    private javax.swing.JLabel lblMenu_Manage;
    private javax.swing.JLabel lblMenu_Statictis;
    private javax.swing.JLabel lblMenu_System;
    private javax.swing.JLabel lblShowMenu;
    private javax.swing.JLabel lblUser_Avatar;
    private javax.swing.JLabel lblWeb;
    private javax.swing.JMenu menEmployee;
    private javax.swing.JMenu menOptions;
    private javax.swing.JMenu menProducts;
    private javax.swing.JMenuItem mitCal;
    private javax.swing.JMenuItem mitChangeColorButton;
    private javax.swing.JMenuItem mitChangeColorLabel;
    private javax.swing.JMenuItem mitExit;
    private javax.swing.JMenuItem mitFinacial;
    private javax.swing.JMenuItem mitInvoicesIn;
    private javax.swing.JMenuItem mitInvoicesOut;
    private javax.swing.JMenuItem mitInvoidInMonth;
    private javax.swing.JMenuItem mitLogout;
    private javax.swing.JMenuItem mitProduct;
    private javax.swing.JMenuItem mitStatus;
    private javax.swing.JMenuItem mitUser;
    private javax.swing.JMenuItem mitVendors;
    private javax.swing.JMenuItem mitchangeColorBaclground;
    private javax.swing.JPanel pnlFinacialControll;
    private javax.swing.JPanel pnlFrame;
    private javax.swing.JPanel pnlLogin;
    private javax.swing.JPanel pnlManageEmployee_Infomation;
    private javax.swing.JPanel pnlManageEmployee_InvoicesOut;
    private javax.swing.JPanel pnlManageEmployee_User;
    private javax.swing.JPanel pnlManageEmployees_Choices;
    private javax.swing.JPanel pnlManageEmployees_ChoicesMain;
    protected javax.swing.JPanel pnlManageEmployees_Controller;
    private javax.swing.JPanel pnlManageEmployees_InfomationController;
    private javax.swing.JPanel pnlManageEmployees_InvoicesOutController;
    private javax.swing.JPanel pnlManageEmployees_UserController;
    private javax.swing.JPanel pnlManageEmployees_View;
    protected javax.swing.JPanel pnlManageEmployees_ViewMain;
    protected javax.swing.JPanel pnlManageFinacial_Controller;
    private javax.swing.JPanel pnlManageFinacial_View;
    protected javax.swing.JPanel pnlManageFinacial_ViewMain;
    private javax.swing.JPanel pnlManageProducts_Choices;
    private javax.swing.JPanel pnlManageProducts_ChoicesMain;
    protected javax.swing.JPanel pnlManageProducts_Controller;
    private javax.swing.JPanel pnlManageProducts_InvoicesIn;
    private javax.swing.JPanel pnlManageProducts_InvoicesInController;
    private javax.swing.JPanel pnlManageProducts_Product;
    private javax.swing.JPanel pnlManageProducts_ProductController;
    private javax.swing.JPanel pnlManageProducts_Vendors;
    private javax.swing.JPanel pnlManageProducts_VendorsController;
    private javax.swing.JPanel pnlManageProducts_View;
    protected javax.swing.JPanel pnlManageProducts_ViewMain;
    private javax.swing.JPanel pnlMenuMain;
    private javax.swing.JPanel pnlMenuSystem;
    private javax.swing.JPanel pnlMenuUser;
    private javax.swing.JPanel pnlMenu_System;
    protected javax.swing.JPanel pnlMenu_SystemController;
    protected javax.swing.JPanel pnlMenu_SystemView;
    private javax.swing.JPanel pnlMenu_start;
    private javax.swing.JPanel pnlStatictis_Chart;
    private javax.swing.JPanel pnlStatictis_Finacial;
    private javax.swing.JPanel pnlStatictis_InvoiceIn;
    private javax.swing.JPanel pnlStatictis_InvoiceOut;
    private javax.swing.JTabbedPane pnlStatictis_Table;
    private javax.swing.JPanel pnlStatitis_View;
    private javax.swing.JPopupMenu pomManage;
    private javax.swing.JPopupMenu pomStatictis;
    private javax.swing.JPopupMenu pomSystem;
    private javax.swing.JRadioButton rdbStatictis_Finacial_All;
    private javax.swing.JRadioButton rdbStatictis_Finalcial_Month;
    private javax.swing.JRadioButton rdbStatictis_InvoiceIn_All;
    private javax.swing.JRadioButton rdbStatictis_InvoiceIn_Month;
    private javax.swing.JRadioButton rdbStatictis_InvoiceOut_All;
    private javax.swing.JRadioButton rdbStatictis_InvoiceOut_Month;
    private javax.swing.JRadioButton rdoFinancial_Posi;
    private javax.swing.JRadioButton rdoFinancial_PosiID;
    private javax.swing.JRadioButton rdoInvoiceIn_EmplID;
    private javax.swing.JRadioButton rdoInvoiceIn_InvoiceID;
    private javax.swing.JRadioButton rdoInvoiceIn_ProductID;
    private javax.swing.JRadioButton rdoInvoiceIn_VendorID;
    private javax.swing.JRadioButton rdoInvoiceOut_InvoiceID;
    private javax.swing.JRadioButton rdoInvoiceOut_ProID;
    private javax.swing.JRadioButton rdoInvoieOut_EmplID;
    private javax.swing.JRadioButton rdoPersonal_EmplID;
    private javax.swing.JRadioButton rdoPersonal_Name;
    private javax.swing.JRadioButton rdoPersonal_PersonID;
    private javax.swing.JRadioButton rdoPersonal_Position;
    private javax.swing.JRadioButton rdoProduct_Name;
    private javax.swing.JRadioButton rdoProduct_ProID;
    private javax.swing.JRadioButton rdoProduct_VendorID;
    private javax.swing.JRadioButton rdoRole;
    private javax.swing.JRadioButton rdoUser_EmplID;
    private javax.swing.JRadioButton rdoUsername;
    private javax.swing.JRadioButton rdoVendor_Address;
    private javax.swing.JRadioButton rdoVendor_Name;
    private javax.swing.JRadioButton rdoVendor_VendorID;
    private javax.swing.JScrollPane scrInvoicesOut;
    private javax.swing.JScrollPane scrManageFinacial;
    private javax.swing.JScrollPane scrStatusEmployees;
    private javax.swing.JScrollPane scrUser;
    private javax.swing.JScrollPane srcManageProducts_InvoicesIn;
    private javax.swing.JScrollPane srcManageProducts_Vendors;
    private javax.swing.JScrollPane srcProducts;
    private javax.swing.JTable tblInvoicesOut;
    private javax.swing.JTable tblManageFinacial;
    private javax.swing.JTable tblManageProducts_InvoicesIn;
    private javax.swing.JTable tblManageProducts_Product;
    private javax.swing.JTable tblManageProducts_Vendors;
    private javax.swing.JTable tblStatictis_Finacial_report;
    private javax.swing.JTable tblStatictis_InvoiceIn;
    private javax.swing.JTable tblStatictis_InvoiceOut;
    private javax.swing.JTable tblStatusEmployees;
    private javax.swing.JTable tblUser;
    private javax.swing.JTextArea txtEmployee_Address;
    private javax.swing.JTextField txtEmployee_Find;
    private javax.swing.JTextField txtEmployee_ID;
    private javax.swing.JTextField txtEmployee_Name;
    private javax.swing.JTextField txtEployee_PersonalID;
    private javax.swing.JTextField txtFianancial_Find;
    private javax.swing.JTextField txtFinacial_Position;
    private javax.swing.JTextField txtIFinacial_Salary;
    private javax.swing.JTextField txtIFinalcial_PositionID;
    private javax.swing.JTextField txtInvoiceBought_DateCreated;
    private javax.swing.JTextField txtInvoiceBought_ID1;
    private javax.swing.JTextField txtInvoiceBought_ImplID;
    private javax.swing.JTextField txtInvoiceBought_Price;
    private javax.swing.JTextField txtInvoiceBought_ProductID;
    private javax.swing.JTextField txtInvoiceBought_Quantity;
    private javax.swing.JTextField txtInvoiceBought_TotalMoney;
    private javax.swing.JTextField txtInvoiceBought_VendorID;
    private javax.swing.JTextField txtInvoiceSold_DateCreated;
    private javax.swing.JTextField txtInvoiceSold_Find;
    private javax.swing.JTextField txtInvoiceSold_ID;
    private javax.swing.JTextField txtInvoiceSold_ImplID;
    private javax.swing.JTextField txtInvoiceSold_Price;
    private javax.swing.JTextField txtInvoiceSold_ProductID;
    private javax.swing.JTextField txtInvoiceSold_Quantity;
    private javax.swing.JTextField txtInvoiceSold_TotalMoney;
    private javax.swing.JTextField txtInvoicebought_Find;
    private javax.swing.JPasswordField txtPass;
    private javax.swing.JTextField txtProduct_Find;
    private javax.swing.JTextField txtProduct_ID;
    private javax.swing.JTextField txtProduct_Name;
    private javax.swing.JTextField txtProduct_Price;
    private javax.swing.JTextField txtProduct_Quantity;
    private javax.swing.JTextField txtProduct_VendorID;
    private javax.swing.JTextField txtUser;
    private javax.swing.JPasswordField txtUser_Confirm;
    private javax.swing.JTextField txtUser_EmplID;
    private javax.swing.JTextField txtUser_Find;
    private javax.swing.JPasswordField txtUser_Password;
    private javax.swing.JTextField txtUser_Username;
    private javax.swing.JTextArea txtVendor_Address;
    private javax.swing.JTextField txtVendor_FInd;
    private javax.swing.JTextField txtVendor_ID;
    private javax.swing.JTextField txtVendor_Name;
    // End of variables declaration//GEN-END:variables

    public static class FrameDragListener extends MouseAdapter {

        private final JFrame frame;
        private Point mouseDownCompCoords = null;

        public FrameDragListener(JFrame frame) {
            this.frame = frame;
        }

        public void mouseReleased(MouseEvent e) {
            mouseDownCompCoords = null;
        }

        public void mousePressed(MouseEvent e) {
            mouseDownCompCoords = e.getPoint();
        }

        public void mouseDragged(MouseEvent e) {
            Point currCoords = e.getLocationOnScreen();
            frame.setLocation(currCoords.x - mouseDownCompCoords.x, currCoords.y - mouseDownCompCoords.y);
        }
    }

    class Action implements ActionListener {

        public static final int INVOICESIN = 1;
        public static final int INVOICESOUT = 2;
        public static final int VENDOR = 3;
        public static final int PRODUCTS = 4;
        public static final int USERS = 5;
        public static final int INFOMATION = 6;
        public static final int CHART = 7;
        public static final int FINACIAL = 8;

        private int SHOW;

        public Action(int SHOW) {
            this.SHOW = SHOW;
        }

        public Action() {
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            switch (SHOW) {
                case 1:

                    pnlManageFinacial_Controller.setVisible(false);
                    pnlManageFinacial_ViewMain.setVisible(false);

                    pnlMenu_SystemController.setVisible(true);
                    pnlMenu_SystemView.setVisible(true);
                    if (pnlMenu_SystemView.getX() < 0) {
                        pnlStatitis_View.setVisible(false);
                        AnimationHelper.OneLeftOneRightin(f, pnlMenu_SystemView, pnlMenu_SystemController);
                    }
                    if (pnlManageProducts_ViewMain.isVisible()) {
                        pnlStatitis_View.setVisible(false);
                        InvoiceInShow();
                    } else {
                        pnlStatitis_View.setVisible(false);
                        AnimationHelper.OneLeftOneRight(f, pnlMenu_SystemView, pnlManageProducts_ViewMain, pnlManageEmployees_ViewMain, pnlManageFinacial_ViewMain, pnlMenu_SystemController, pnlManageProducts_Controller, pnlManageEmployees_Controller, pnlManageFinacial_Controller);
                        InvoiceInShow();
                    }
                    break;
                case 2:
                    pnlManageFinacial_Controller.setVisible(false);
                    pnlManageFinacial_ViewMain.setVisible(false);

                    pnlMenu_SystemController.setVisible(true);
                    pnlMenu_SystemView.setVisible(true);
                    if (pnlMenu_SystemView.getX() < 0) {
                        pnlStatitis_View.setVisible(false);
                        AnimationHelper.OneLeftOneRightin(f, pnlMenu_SystemView, pnlMenu_SystemController);
                    }
                    if (pnlManageEmployees_ViewMain.isVisible()) {
                        pnlStatitis_View.setVisible(false);
                        InvoiceOutShow();
                    } else {
                        pnlStatitis_View.setVisible(false);
                        AnimationHelper.OneLeftOneRight(f, pnlMenu_SystemView, pnlManageEmployees_ViewMain, pnlManageProducts_ViewMain, pnlManageFinacial_ViewMain, pnlMenu_SystemController, pnlManageEmployees_Controller, pnlManageProducts_Controller, pnlManageFinacial_Controller);
                        InvoiceOutShow();
                    }
                    break;
                case 3:

                    pnlManageFinacial_Controller.setVisible(false);
                    pnlManageFinacial_ViewMain.setVisible(false);

                    pnlMenu_SystemController.setVisible(true);
                    pnlMenu_SystemView.setVisible(true);
                    if (pnlMenu_SystemView.getX() < 0) {
                        pnlStatitis_View.setVisible(false);
                        AnimationHelper.OneLeftOneRightin(f, pnlMenu_SystemView, pnlMenu_SystemController);

                    }
                    if (pnlManageProducts_ViewMain.isVisible()) {
                        pnlStatitis_View.setVisible(false);
                        VendorsShow();
                    } else {
                        pnlStatitis_View.setVisible(false);
                        AnimationHelper.OneLeftOneRight(f, pnlMenu_SystemView, pnlManageProducts_ViewMain, pnlManageEmployees_ViewMain, pnlManageFinacial_ViewMain, pnlMenu_SystemController, pnlManageProducts_Controller, pnlManageEmployees_Controller, pnlManageFinacial_Controller);
                        VendorsShow();
                    }
                    break;
                case 4:
                    pnlManageFinacial_Controller.setVisible(false);
                    pnlManageFinacial_ViewMain.setVisible(false);

                    pnlMenu_SystemController.setVisible(true);
                    pnlMenu_SystemView.setVisible(true);
                    if (pnlMenu_SystemView.getX() < 0) {
                        pnlStatitis_View.setVisible(false);
                        AnimationHelper.OneLeftOneRightin(f, pnlMenu_SystemView, pnlMenu_SystemController);
                    }
                    if (pnlManageProducts_ViewMain.isVisible()) {
                        pnlStatitis_View.setVisible(false);
                        ProductShow();
                    } else {
                        pnlStatitis_View.setVisible(false);
                        AnimationHelper.OneLeftOneRight(f, pnlMenu_SystemView, pnlManageProducts_ViewMain, pnlManageEmployees_ViewMain, pnlManageFinacial_ViewMain, pnlMenu_SystemController, pnlManageProducts_Controller, pnlManageEmployees_Controller, pnlManageFinacial_Controller);
                        ProductShow();
                    }
                    break;
                case 5:
                    pnlManageFinacial_Controller.setVisible(false);
                    pnlManageFinacial_ViewMain.setVisible(false);

                    pnlMenu_SystemController.setVisible(true);
                    pnlMenu_SystemView.setVisible(true);
                    if (pnlMenu_SystemView.getX() < 0) {
                        pnlStatitis_View.setVisible(false);
                        AnimationHelper.OneLeftOneRightin(f, pnlMenu_SystemView, pnlMenu_SystemController);
                    }
                    if (pnlManageEmployees_ViewMain.isVisible()) {
                        pnlStatitis_View.setVisible(false);
                        UserShow();
                    } else {
                        pnlStatitis_View.setVisible(false);
                        AnimationHelper.OneLeftOneRight(f, pnlMenu_SystemView, pnlManageEmployees_ViewMain, pnlManageProducts_ViewMain, pnlManageFinacial_ViewMain, pnlMenu_SystemController, pnlManageEmployees_Controller, pnlManageProducts_Controller, pnlManageFinacial_Controller);
                        UserShow();
                    }

                    break;
                case 6:

                    pnlManageFinacial_Controller.setVisible(false);
                    pnlManageFinacial_ViewMain.setVisible(false);

                    pnlMenu_SystemController.setVisible(true);
                    pnlMenu_SystemView.setVisible(true);
                    if (pnlMenu_SystemView.getX() < 0) {
                        pnlStatitis_View.setVisible(false);
                        AnimationHelper.OneLeftOneRightin(f, pnlMenu_SystemView, pnlMenu_SystemController);
                    }
                    if (pnlManageEmployees_ViewMain.isVisible()) {
                        pnlStatitis_View.setVisible(false);
                        InfomationShow();
                    } else {
                        pnlStatitis_View.setVisible(false);
                        AnimationHelper.OneLeftOneRight(f, pnlMenu_SystemView, pnlManageEmployees_ViewMain, pnlManageProducts_ViewMain, pnlManageFinacial_ViewMain, pnlMenu_SystemController, pnlManageEmployees_Controller, pnlManageProducts_Controller, pnlManageFinacial_Controller);
                        InfomationShow();
                    }
                    break;
                case 7:
                    if (pnlMenu_SystemView.getX() == 0 && pnlMenu_SystemController.getX() == 0) {
                        pnlStatitis_View.setVisible(true);
                        pnlMenu_SystemController.setVisible(false);
                        pnlMenu_SystemView.setVisible(false);
                        AnimationHelper.OneLeftOneRightOut(f, pnlMenu_SystemView, pnlMenu_SystemController);
                    } else {

                    }
                    break;

                case 8:
                    pnlMenu_SystemController.setVisible(true);
                    pnlMenu_SystemView.setVisible(true);
                    pnlStatitis_View.setVisible(false);
                    if (!pnlManageFinacial_ViewMain.isVisible()) {
                        if (pnlManageEmployees_ViewMain.isVisible()) {

                            pnlManageEmployees_ViewMain.setVisible(false);
                            pnlManageEmployees_Controller.setVisible(false);

                            AnimationHelper.OneLeftOneRight(f, pnlMenu_SystemView, pnlManageFinacial_ViewMain, pnlManageProducts_ViewMain, pnlManageEmployees_ViewMain, pnlMenu_SystemController, pnlManageFinacial_Controller, pnlManageProducts_Controller, pnlManageEmployees_Controller);

                        } else if (pnlManageProducts_ViewMain.isVisible()) {
                            pnlStatitis_View.setVisible(false);
                            pnlManageProducts_ViewMain.setVisible(false);
                            pnlManageProducts_Controller.setVisible(false);

                            AnimationHelper.OneLeftOneRight(f, pnlMenu_SystemView, pnlManageFinacial_ViewMain, pnlManageProducts_ViewMain, pnlManageEmployees_ViewMain, pnlMenu_SystemController, pnlManageFinacial_Controller, pnlManageProducts_Controller, pnlManageEmployees_Controller);

                        } else {

                        }
                    }
                    break;
                default:
                    break;
            }
        }

    }
}
