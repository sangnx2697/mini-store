/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library;

import java.util.List;
import java.util.Scanner;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author xuansang
 */
public class XLDL {
    public static boolean checkChar(int k){
        int [] arr = new int[]{0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4A,0x4B,0x4C,0x4D,0x4E,0x4F,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5A};
         for(int i=0;i<arr.length;i++){
             if(k==arr[i]){
             return true;
             }
         }       
         return false;
    }
    public static String convertString(String chuoi, String valueIn, String valueOut) {
        String[] mangchuoi = new String[chuoi.length()];
        String kq = "";
        for (int i = 0; i < chuoi.length(); i++) {
            if (chuoi.substring(i, i + 1).equals(valueIn)) {
                mangchuoi[i] = valueOut;
            } else {
                mangchuoi[i] = chuoi.substring(i, i + 1);
            }
        }
        for (String b : mangchuoi) {
            kq = kq + b;
        }
        return kq;
    }

    public static String getCodeToFind(String a) {
        String b = a.substring(0, a.lastIndexOf(":") + 1);
        return b;
    }

    //** chuy?n ti?n t? in sang VNÐ
    public static String moneyVN(int a) {
        String b = XLC.change(String.valueOf(a));
        int dem = 0;
        int m = 1;
        for (int i = 0; i < b.length(); i++) {
            if (m == 3) {
                dem++;
                m = 0;
            }
            m++;
        }
        int t[] = new int[dem + 2];
        t[0] = 0;
        t[t.length - 1] = b.length();
        int chay = 1;
        int m2 = 1;
        for (int i = 0; i < b.length(); i++) {
            if (m2 == 3) {
                t[chay] = i + 1;
                chay++;
                m2 = 0;
            }
            m2++;
        }
        String kq = "";
        for (int i = 0; i < t.length - 1; i++) {
            String d = b.substring(t[i], t[i + 1]);
            kq = kq + "." + d;
        }
        String l = XLC.change(kq.substring(1, kq.length()));
        if (l.substring(0, 1).equals(".")) {
            return l.substring(1, l.length()) + "d";
        }
        return l + "d";
    }

    public static String moneyVN(String a) {
        String b = XLC.change(a);
        int dem = 0;
        int m = 1;
        for (int i = 0; i < b.length(); i++) {

            if (m == 3) {
                dem++;
                m = 0;
            }

            m++;
        }
        int t[] = new int[dem + 2];
        t[0] = 0;
        t[t.length - 1] = b.length();
        int chay = 1;
        int m2 = 1;
        for (int i = 0; i < b.length(); i++) {
            if (m2 == 3) {
                t[chay] = i + 1;
                chay++;
                m2 = 0;
            }
            m2++;
        }
        String kq = "";
        for (int i = 0; i < t.length - 1; i++) {
            String d = b.substring(t[i], t[i + 1]);
            kq = kq + "." + d;
        }
        String l = XLC.change(kq.substring(1, kq.length()));
        if (l.substring(0, 1).equals(".")) {
            return l.substring(1, l.length()) + "d";
        }
        return l + "d";
    }

    public static String moneyVN(double a) {
        String b = XLC.change(String.valueOf(a));

        int dem = 0;
        int m = 1;
        for (int i = 0; i < b.length(); i++) {

            if (m == 3) {
                dem++;
                m = 0;
            }

            m++;
        }
        int t[] = new int[dem + 2];
        t[0] = 0;
        t[t.length - 1] = b.length();
        int chay = 1;
        int m2 = 1;
        for (int i = 0; i < b.length(); i++) {
            if (m2 == 3) {
                t[chay] = i + 1;
                chay++;
                m2 = 0;
            }
            m2++;
        }
        String kq = "";
        for (int i = 0; i < t.length - 1; i++) {
            String d = b.substring(t[i], t[i + 1]);
            kq = kq + "." + d;
        }
        String l = XLC.change(kq.substring(1, kq.length()));
        if (l.substring(0, 1).equals(".")) {
            return l.substring(1, l.length()) + "d";
        }
        return l + "d";
    }

    public static boolean ktluong(String a) {
        if (a.matches("[0-9]{6,99}") == true) {
            return true;
        }
        return false;
    }

    public static boolean setluong(String a) {
        int i = Integer.parseInt(a);
        if (i >= 5000000) {
            return true;
        }
        return false;
    }

    public static boolean setsdt(String a) {
        String mau[] = {"070", "079", "077", "076", "078", "083", "084", "085", "081", "082", "032", "033", "034", "035", "036", "037", "038", "039", "056", "058", "059"};
        String mau2[] = {"092", "056", "058", "088", "091", "094", "089", "090", "093", "086", "096", "097", "098"};
        String mau3[] = {"0203", "0204", "0205", "0206", "0207", "0208", "0209", "0212", "0213", "0214", "0215", "0216", "0232", "0233", "0234", "0235", "2036", "2037", "0238", "0239", "0220", "0221", "0222", "0225", "0226", "0227", "0228", "0229", "0290", "0291", "0292", "0293", "0294", "0295", "0296", "0297", "0298", "0299", "0254", "0255", "0256", "0257", "0258", "0259", "0260", "0261", "0262", "0263", "0269", "0270", "0271", "0272", "0273", "0274", "0275", "0276", "0277"};
        String mau5[] = {"024", "028"};
        String mau4[] = {"0123", "0124", "0125", "0127", "0129", "0120", "0121", "0122", "0126", "0128", "0162", "0163", "0164", "0165", "0166", "0167", "0168", "0169"};
        boolean kt1 = false;
        boolean kt6 = false;
        for (int i = 0; i < mau.length; i++) {
            if (a.substring(0, 3).equals(mau[i])) {
                kt1 = true;
            }
        }
        for (int i = 0; i < mau4.length; i++) {
            if (a.substring(0, 4).equals(mau4[i])) {
                kt1 = true;
            }
        }
        for (int i = 0; i < mau2.length; i++) {
            if (a.substring(0, 3).equals(mau2[i])) {
                kt1 = true;
            }
        }
        for (int i = 0; i < mau3.length; i++) {
            if (a.substring(0, 4).equals(mau3[i])) {
                kt1 = true;
            }
        }
        for (int i = 0; i < mau5.length; i++) {
            if (a.substring(0, 3).equals(mau5[i])) {
                kt1 = true;
            }
        }
        if (a.matches("[0-9]{10,11}")) {
            kt6 = true;
        }
        if (kt1 && kt6) {
            return true;
        }
        return false;
    }

    public static boolean SetTen(String a) {
        String mau = "[a-zA-Z]{1,9}(\\s[a-zA-Z]{2,9}){0,4}\\s[a-zA-Z]{1,8}+";
        return a.matches(mau);
    }
    
    public static boolean setmaSP(String a) {
        return a.matches("SP\\d{3}");
    }
    public static boolean checkCharaterByABC(String a){
       boolean kt=true;
        for (int i = 0; i < a.length(); i++) {
            if(a.substring(i, i+1).matches("[a-zA-Z]")){
                kt=false;
                return kt;
            }
        }
        return kt;
    }
    public static boolean setmaSP(List<Object> list, String a, String b) {
        for (int i = 0; i < list.size(); i++) {
            if (b.equalsIgnoreCase(a)) {
                return false;
            }
        }
        return true;
    }

    public static boolean setmaSP(DefaultTableModel list, String a) {
        for (int i = 0; i < list.getRowCount(); i++) {
            if (list.getValueAt(i, 0).toString().equalsIgnoreCase(a)) {
                return false;
            }
        }
        return true;
    }

    public static boolean setmaSP(DefaultTableModel list, String a, int index) {
        for (int i = 0; i < list.getRowCount(); i++) {
            if (list.getValueAt(i, 0).toString().equalsIgnoreCase(a) && i != index) {
                return false;
            }
        }
        return true;
    }
    public static boolean setCMND(String a) {
        return a.matches("[0-9]{9}");
    }

    public static boolean setCMND(List<Object> list, String a, String b) {
        for (int i = 0; i < list.size(); i++) {
            if (b.equalsIgnoreCase(a)) {
                return false;
            }
        }
        return true;
    }

    public static boolean seCMND(DefaultTableModel list, String a) {
        for (int i = 0; i < list.getRowCount(); i++) {
            if (list.getValueAt(i, 0).toString().equalsIgnoreCase(a)) {
                return false;
            }
        }
        return true;
    }

    public static boolean setCMND(DefaultTableModel list, String a, int index) {
        for (int i = 0; i < list.getRowCount(); i++) {
            if (list.getValueAt(i, 0).toString().equalsIgnoreCase(a) && i != index) {
                return false;
            }
        }
        return true;
    }
      public static boolean setmaNS(String a) {
        return a.matches("N[A-Z]\\d{3}");
    }

    public static boolean setmaNS(List<Object> list, String a, String b) {
        for (int i = 0; i < list.size(); i++) {
            if (b.equalsIgnoreCase(a)) {
                return false;
            }
        }
        return true;
    }

    public static boolean setmaNS(DefaultTableModel list, String a) {
        for (int i = 0; i < list.getRowCount(); i++) {
            if (list.getValueAt(i, 0).toString().equalsIgnoreCase(a)) {
                return false;
            }
        }
        return true;
    }

    public static boolean setmaNS(DefaultTableModel list, String a, int index) {
        for (int i = 0; i < list.getRowCount(); i++) {
            if (list.getValueAt(i, 0).toString().equalsIgnoreCase(a) && i != index) {
                return false;
            }
        }
        return true;
    }

    public static boolean settuoi(String a) {
        String mau = "[0-9]{1,2}";
        int max = 55;
        int min = 16;
        return a.matches(mau) && (Integer.parseInt(a) >= min && Integer.parseInt(a) <= max) ? true : false;
    }
    
    public static boolean setSoLuong(String a){
        String mau = "[1-9]{1,2}";    
        if (a.matches(mau)) {
            return true;
        }
        return false;
    }
    
    

    public static boolean setmail(List<Object> list, String a, String b) {
        for (int i = 0; i < list.size(); i++) {
            if (b.equalsIgnoreCase(a)) {
                return false;
            }
        }
        return true;
    }

    public static boolean setmail(DefaultTableModel list, String a, int index) {
        for (int i = 0; i < list.getRowCount(); i++) {
            if (list.getValueAt(i, 2).toString().equalsIgnoreCase(a) && i != index) {
                return false;
            }
        }
        return true;
    }

    public static boolean setmail(DefaultTableModel list, String a) {
        for (int i = 0; i < list.getRowCount(); i++) {
            if (list.getValueAt(i, 2).toString().equalsIgnoreCase(a)) {
                return false;
            }
        }
        return true;
    }

    public static boolean setmail(String a) {
        return a.matches("\\w+@\\w+\\.\\w+");
    }
    
    public static boolean setMaHDB(String a) {
        return a.matches("HDB\\d{3}");
    }

    public static boolean setMaHDB(List<Object> list, String a, String b) {
        for (int i = 0; i < list.size(); i++) {
            if (b.equalsIgnoreCase(a)) {
                return false;
            }
        }
        return true;
    }

    public static boolean setMaHDB(DefaultTableModel list, String a) {
        for (int i = 0; i < list.getRowCount(); i++) {
            if (list.getValueAt(i, 0).toString().equalsIgnoreCase(a)) {
                return false;
            }
        }
        return true;
    }

    public static boolean setMaHDB(DefaultTableModel list, String a, int index) {
        for (int i = 0; i < list.getRowCount(); i++) {
            if (list.getValueAt(i, 0).toString().equalsIgnoreCase(a) && i != index) {
                return false;
            }
        }
        return true;
    }
    
     public static boolean setMaHDM(String a) {
        return a.matches("HDM\\d{3}");
    }

    public static boolean setMaHDM(List<Object> list, String a, String b) {
        for (int i = 0; i < list.size(); i++) {
            if (b.equalsIgnoreCase(a)) {
                return false;
            }
        }
        return true;
    }

    public static boolean setMaHDM(DefaultTableModel list, String a) {
        for (int i = 0; i < list.getRowCount(); i++) {
            if (list.getValueAt(i, 0).toString().equalsIgnoreCase(a)) {
                return false;
            }
        }
        return true;
    }

    public static boolean setMaHDM(DefaultTableModel list, String a, int index) {
        for (int i = 0; i < list.getRowCount(); i++) {
            if (list.getValueAt(i, 0).toString().equalsIgnoreCase(a) && i != index) {
                return false;
            }
        }
        return true;
    }
    
    public static boolean setChucVu(String a ){
        String cv [] = {"GD", "NV", "QK", "QL", "TC"};
        for (int i = 0; i < cv.length; i++) {
            if (a.matches(cv[i])) {
                return true;
            }           
        }
        return false;
    }
     public static boolean setMaNCC(String a) {
        return a.matches("NC\\d{3}");
    }

    public static boolean setMaNCC(List<Object> list, String a, String b) {
        for (int i = 0; i < list.size(); i++) {
            if (b.equalsIgnoreCase(a)) {
                return false;
            }
        }
        return true;
    }

    public static boolean setMaNCC(DefaultTableModel list, String a) {
        for (int i = 0; i < list.getRowCount(); i++) {
            if (list.getValueAt(i, 0).toString().equalsIgnoreCase(a)) {
                return false;
            }
        }
        return true;
    }

    public static boolean setMaNCC(DefaultTableModel list, String a, int index) {
        for (int i = 0; i < list.getRowCount(); i++) {
            if (list.getValueAt(i, 0).toString().equalsIgnoreCase(a) && i != index) {
                return false;
            }
        }
        return true;
    }
    
    public static boolean setUser(String a) {
        return a.matches("\\w");
    }

    public static boolean setUser(List<Object> list, String a, String b) {
        for (int i = 0; i < list.size(); i++) {
            if (b.equalsIgnoreCase(a)) {
                return false;
            }
        }
        return true;
    }

    public static boolean setUser(DefaultTableModel list, String a) {
        for (int i = 0; i < list.getRowCount(); i++) {
            if (list.getValueAt(i, 0).toString().equalsIgnoreCase(a)) {
                return false;
            }
        }
        return true;
    }

    public static boolean setUser(DefaultTableModel list, String a, int index) {
        for (int i = 0; i < list.getRowCount(); i++) {
            if (list.getValueAt(i, 0).toString().equalsIgnoreCase(a) && i != index) {
                return false;
            }
        }
        return true;
    }
    
    public static boolean setPass(String a) {
        return a.matches("\\w");
    }

    public static boolean setPass(List<Object> list, String a, String b) {
        for (int i = 0; i < list.size(); i++) {
            if (b.equalsIgnoreCase(a)) {
                return false;
            }
        }
        return true;
    }

    public static boolean setPass(DefaultTableModel list, String a) {
        for (int i = 0; i < list.getRowCount(); i++) {
            if (list.getValueAt(i, 0).toString().equalsIgnoreCase(a)) {
                return false;
            }
        }
        return true;
    }

    public static boolean setPass(DefaultTableModel list, String a, int index) {
        for (int i = 0; i < list.getRowCount(); i++) {
            if (list.getValueAt(i, 0).toString().equalsIgnoreCase(a) && i != index) {
                return false;
            }
        }
        return true;
    }
    public void ArrayCV(){
        String[] ChucVu = new String[]{"Giám đốc","Nhân Viên","Quản lý kho","Quản lý nhân sự","Tài chính"};
    }
    
    
}
