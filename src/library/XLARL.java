/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author xuansang
 */
public class XLARL {
   
    static ArrayList<Integer> sortInt(ArrayList<Integer> arr){
          Integer tam ; 
          for(int i=0;i<arr.size();i++){
            for(int j=i+1;j<arr.size();j++){
                if(arr.get(i)<arr.get(j)){
                      tam=arr.get(i);
                      arr.set(i,arr.get(j));
                      arr.set(j,tam);
                }
            }    
        }
          return arr; 
   
    }
     static ArrayList<Double> sortDouble(ArrayList<Double> arr){
          Double tam ; 
          for(int i=0;i<arr.size();i++){
            for(int j=i+1;j<arr.size();j++){
                if(arr.get(i)<arr.get(j)){
                      tam=arr.get(i);
                      arr.set(i,arr.get(j));
                      arr.set(j,tam);
                }
            }   
        }
          return arr; 
   
    }
     
     static ArrayList<Integer> sortIntg(ArrayList<Integer> arr){
          Integer tam ; 
          for(int i=0;i<arr.size();i++){
            for(int j=i+1;j<arr.size();j++){
                if(arr.get(i)>arr.get(j)){
                      tam=arr.get(i);
                      arr.set(i,arr.get(j));
                      arr.set(j,tam);
                }
            }    
        }
          return arr; 
   
    }
     static ArrayList<Double> sortDoubleg(ArrayList<Double> arr){
          Double tam ; 
          for(int i=0;i<arr.size();i++){
            for(int j=i+1;j<arr.size();j++){
                if(arr.get(i)>arr.get(j)){
                      tam=arr.get(i);
                      arr.set(i,arr.get(j));
                      arr.set(j,tam);
                }
            }   
        }
          return arr; 
   
    }
     static List<Double> sortDoubleg(List<Double> arr){
          Double tam ; 
          for(int i=0;i<arr.size();i++){
            for(int j=i+1;j<arr.size();j++){
                if(arr.get(i)>arr.get(j)){
                      tam=arr.get(i);
                      arr.set(i,arr.get(j));
                      arr.set(j,tam);
                }
            }   
        }
          return arr; 
   
    }
}
