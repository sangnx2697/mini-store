/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library;

/**
 *
 * @author xuansang
 */
public class XLM {

    public static int[] add(int[] arr, int vitri, int k) {
        int a[] = new int[arr.length + 1];
        byte run = 0;
        byte run1 = 0;
        byte run2 = 0;
        for (int i = 0; i < a.length; i++) {
            if (vitri == 0) {
                a[0] = k;
                if (i > 0) {
                    a[i] = arr[run];
                    run++;
                }
            }
            if (vitri == arr.length) {
                a[arr.length] = k;
                if (i < a.length - 1) {
                    a[i] = arr[run1];
                    run1++;
                }
            }

            if (i == vitri) {
                a[vitri] = k;
            }
            if (i != vitri) {
                a[i] = arr[run2];
                run2++;
            }

        }
        return a;
    }

    public static double[] add(double[] arr, int vitri, double k) {
        double a[] = new double[arr.length + 1];
        byte run = 0;
        byte run1 = 0;
        byte run2 = 0;
        for (int i = 0; i < a.length; i++) {
            if (vitri == 0) {
                a[0] = k;
                if (i > 0) {
                    a[i] = arr[run];
                    run++;
                }
            }
            if (vitri == arr.length) {
                a[arr.length] = k;
                if (i < a.length - 1) {
                    a[i] = arr[run1];
                    run1++;
                }
            }

            if (i == vitri) {
                a[vitri] = k;
            }
            if (i != vitri) {
                a[i] = arr[run2];
                run2++;
            }

        }
        return a;
    }

    public static void main(String[] args) {
        SinhVien[] a = new SinhVien[2];
        a[0] = new SinhVien("tuan", 3);

        a[1] = new SinhVien("sang", 4);
        int vitri[] = new int[]{0, 1};
        SinhVien add[] = new SinhVien[]{
            new SinhVien("tèo", 1), new SinhVien("tí", 2)
        };
        a = add(a, vitri, add);

        for (SinhVien s : a) {
            System.out.println(s.getName() + " " + s.getMark() + " " + a.length);
        }

    }

    public static SinhVien[] add(SinhVien[] arr, int[] vitri, SinhVien[] k) {

        SinhVien a[] = new SinhVien[arr.length + k.length];

        for (int i = 0; i < a.length; i++) {
            byte run = 0;
            byte run1 = 0;
            byte run2 = 0;
            for (int j = 0; j < vitri.length; j++) {
                if (vitri[j] == 0&&i==0) {
                    a[vitri[j]] = k[j];
                    if (i != 0) {
                        a[i] = arr[run];
                        run++;

                    }
                }
                if (vitri[j] == arr.length) {
                    a[vitri[j]] = k[j];
                    if (i < a.length - 1) {
                        a[i] = arr[run1];
                        run1++;

                    }
                }
                if (i == vitri[j]) {
                    a[vitri[j]] = k[j];
                }
                if (i != vitri[j]) {
                    a[i] = arr[run2];
                    run2++;
                }
            }
        }
        return a;
    }

    public static SinhVien[] add(SinhVien[] arr, int vitri, SinhVien k) {

        SinhVien a[] = new SinhVien[arr.length + 1];
        byte run = 0;
        byte run1 = 0;
        byte run2 = 0;
        for (int i = 0; i < a.length; i++) {
            if (vitri == 0) {
                a[0] = k;
                if (i > 0) {
                    a[i] = arr[run];
                    run++;
                }
            }
            if (vitri == arr.length) {
                a[arr.length] = k;
                if (i < a.length - 1) {
                    a[i] = arr[run1];
                    run1++;
                }
            }

            if (i == vitri) {
                a[vitri] = k;
            }
            if (i != vitri) {
                a[i] = arr[run2];
                run2++;
            }

        }
        return a;
    }

    public static float[] add(float[] arr, int vitri, float k) {
        float a[] = new float[arr.length + 1];
        byte run = 0;
        byte run1 = 0;
        byte run2 = 0;
        for (int i = 0; i < a.length; i++) {
            if (vitri == 0) {
                a[0] = k;
                if (i > 0) {
                    a[i] = arr[run];
                    run++;
                }
            }
            if (vitri == arr.length) {
                a[arr.length] = k;
                if (i < a.length - 1) {
                    a[i] = arr[run1];
                    run1++;
                }
            }

            if (i == vitri) {
                a[vitri] = k;
            }
            if (i != vitri) {
                a[i] = arr[run2];
                run2++;
            }

        }
        return a;
    }

    public static int[] remove(int[] arr, int vitri) {
        int a[] = new int[arr.length - 1];
        byte run = 0;
        byte run1 = 0;
        byte run2 = 0;
        for (int i = 0; i < arr.length; i++) {
            if (vitri == 0 && i > 0) {
                a[run] = arr[i];
                run++;
            }
            if (vitri == arr.length - 1 && i < arr.length - 1) {
                a[run1] = arr[i];
                run1++;
            }
            if (i != vitri) {

                a[run2] = arr[i];
                run2++;
            }
        }
        return a;
    }

    public static double[] remove(double[] arr, int vitri) {
        double a[] = new double[arr.length - 1];
        byte run = 0;
        byte run1 = 0;
        byte run2 = 0;
        for (int i = 0; i < arr.length; i++) {
            if (vitri == 0 && i > 0) {
                a[run] = arr[i];
                run++;
            }
            if (vitri == arr.length - 1 && i < arr.length - 1) {
                a[run1] = arr[i];
                run1++;
            }
            if (i != vitri) {

                a[run2] = arr[i];
                run2++;
            }
        }
        return a;
    }

    public static float[] remove(float[] arr, int vitri) {
        float a[] = new float[arr.length - 1];
        byte run = 0;
        byte run1 = 0;
        byte run2 = 0;
        for (int i = 0; i < arr.length; i++) {
            if (vitri == 0 && i > 0) {
                a[run] = arr[i];
                run++;
            }
            if (vitri == arr.length - 1 && i < arr.length - 1) {
                a[run1] = arr[i];
                run1++;
            }
            if (i != vitri) {

                a[run2] = arr[i];
                run2++;
            }
        }
        return a;
    }

    public static SinhVien[] remove(SinhVien[] arr, int vitri) {
        SinhVien a[] = new SinhVien[arr.length - 1];
        byte run = 0;
        byte run1 = 0;
        byte run2 = 0;
        for (int i = 0; i < arr.length; i++) {
            if (vitri == 0 && i > 0) {
                a[run] = arr[i];
                run++;
            }
            if (vitri == arr.length - 1 && i < arr.length - 1) {
                a[run1] = arr[i];
                run1++;
            }
            if (i != vitri) {

                a[run2] = arr[i];
                run2++;
            }
        }
        return a;
    }

}
