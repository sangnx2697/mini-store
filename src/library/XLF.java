/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library;


import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;

/**
 *
 * @author xuansang
 */
public class XLF {

    public static final int OPEN_FILE = 1;
    public static final int SAVE_FILE = 2;

    /**
     * @param args the command line arguments
     */
    public static byte[] read(String path) {
        try {
            FileInputStream fis = new FileInputStream(path);
            int n = fis.available();
            byte[] data = new byte[n];
            fis.read(data);
            fis.close();
            return data;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void write(String path, byte[] data) {
        try {
            FileOutputStream fos = new FileOutputStream(path);
            fos.write(data);
            fos.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public static Object readObject(String path) {
        try {
            ObjectInputStream ois
                    = new ObjectInputStream(new FileInputStream(path));
            Object object = ois.readObject();
            ois.close();
            return object;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public static void writeObject(String path, Object object) {
        try {
            ObjectOutputStream oos
                    = new ObjectOutputStream(new FileOutputStream(path));
            oos.writeObject(object);
            oos.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        FileChoose("Open", OPEN_FILE);
    }
    public static String avataname = "";

    public static Object FileChoose(String string, int type, Object a) {
        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle(string);
        switch (type) {
            case OPEN_FILE:
                fc.showOpenDialog(null);
                break;
            case SAVE_FILE:
                fc.showOpenDialog(null);
                break;
        }
        int choose = -1;
        if (choose == JFileChooser.APPROVE_OPTION) {
            File f = fc.getSelectedFile();
            switch (type) {
                case OPEN_FILE:
                    return Open(f, a);
                case SAVE_FILE:
                    Save(f, a);
                    break;
            }
        }
        return null;
    }

    private static void Save(File file, Object a) {
        try {
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream dos = new ObjectOutputStream(fos);
            dos.writeObject(a);
            fos.close();
            dos.close();
        } catch (Exception ex) {
            Logger.getLogger(XLF.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static Object Open(File file, Object a) {
        Object t;
        try {
            FileInputStream fin = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fin);
            t = ois.readObject();
            fin.close();
            ois.close();
            return t;

        } catch (Exception ex) {
            Logger.getLogger(XLF.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void FileChoose(String string, int type) {
        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle(string);
        switch (type) {
            case OPEN_FILE:
                
                fc.showOpenDialog(null);
                File f1 = fc.getSelectedFile();
                avataname = f1.getAbsolutePath().substring(f1.getAbsolutePath().lastIndexOf("\\")+1, f1.getAbsolutePath().length()-1);
            
                //Open(f1);
                
                break;
            case SAVE_FILE:
                File f = fc.getSelectedFile();
                fc.showOpenDialog(null);
                Save(f);
                break;
        }
       

    }

    private static void Save(File file) {
        try {
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream dos = new ObjectOutputStream(fos);
            dos.writeObject(new Object());
            fos.close();
            dos.close();
        } catch (Exception ex) {
            Logger.getLogger(XLF.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static Object Open(File file) {
        Object t;
        try {
            FileInputStream fin = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fin);
            t = ois.readObject();
            fin.close();
            ois.close();
            return t;

        } catch (Exception ex) {
            Logger.getLogger(XLF.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
