/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library;


import java.util.List;
import java.util.Scanner;

/**
 *
 * @author xuansang
 */
public class Method {
// *** lấy chữ cái đầu của tên
    public static String cutFirstWord(String a){
      int[]LocalSpace=XLC.CountEmptyString(a);
      int []b=new int[LocalSpace.length];
      b[0]=LocalSpace[0]+1;
      b[LocalSpace.length-2]=LocalSpace.length-1;
      for (int i = 1; i < LocalSpace.length-1; i++) {
           b[i]=LocalSpace[i]+2;
        }
      String c="";
        for (int i = 0; i < LocalSpace.length-1; i++) {
                 c =c+ a.substring(LocalSpace[i], b[i]);
            
        }
        return XLC.change(XLC.cutempty(c));
    }
//** chuyển tiền tệ in sang VNĐ
    
    public static String moneyVN(int a) {
        String b = XLC.change(String.valueOf(a));

        int dem = 0;
        int m = 1;
        for (int i = 0; i < b.length(); i++) {

            if (m == 3) {
                dem++;
                m = 0;
            }

            m++;
        }
        int t[] = new int[dem + 2];
        t[0] = 0;
        t[t.length - 1] = b.length();
        int chay = 1;
        int m2 = 1;
        for (int i = 0; i < b.length(); i++) {
            if (m2 == 3) {
                t[chay] = i + 1;
                chay++;
                m2 = 0;
            }
            m2++;
        }
        String kq = "";
        for (int i = 0; i < t.length - 1; i++) {
            String d = b.substring(t[i], t[i + 1]);
            kq = kq + "." + d;
        }
        String l = XLC.change(kq.substring(1, kq.length()));
        if (l.substring(0, 1).equals(".")) {
            return l.substring(1, l.length()) + "đ";
        }
        return l + "đ";
    }
    
    public static String moneyVN(String a) {
        String b = XLC.change(a);
        int dem = 0;
        int m = 1;
        for (int i = 0; i < b.length(); i++) {

            if (m == 3) {
                dem++;
                m = 0;
            }

            m++;
        }
        int t[] = new int[dem + 2];
        t[0] = 0;
        t[t.length - 1] = b.length();
        int chay = 1;
        int m2 = 1;
        for (int i = 0; i < b.length(); i++) {
            if (m2 == 3) {
                t[chay] = i + 1;
                chay++;
                m2 = 0;
            }
            m2++;
        }
        String kq = "";
        for (int i = 0; i < t.length - 1; i++) {
            String d = b.substring(t[i], t[i + 1]);
            kq = kq + "." + d;
        }
        String l = XLC.change(kq.substring(1, kq.length()));
        if (l.substring(0, 1).equals(".")) {
            return l.substring(1, l.length()) + "đ";
        }
        return l + "đ";
    }
    public static String moneyVN(double a) {
        String b = XLC.change(String.valueOf(a));

        int dem = 0;
        int m = 1;
        for (int i = 0; i < b.length(); i++) {

            if (m == 3) {
                dem++;
                m = 0;
            }

            m++;
        }
        int t[] = new int[dem + 2];
        t[0] = 0;
        t[t.length - 1] = b.length();
        int chay = 1;
        int m2 = 1;
        for (int i = 0; i < b.length(); i++) {
            if (m2 == 3) {
                t[chay] = i + 1;
                chay++;
                m2 = 0;
            }
            m2++;
        }
        String kq = "";
        for (int i = 0; i < t.length - 1; i++) {
            String d = b.substring(t[i], t[i + 1]);
            kq = kq + "." + d;
        }
        String l = XLC.change(kq.substring(1, kq.length()));
        if (l.substring(0, 1).equals(".")) {
            return l.substring(1, l.length()) + "đ";
        }
        return l + "đ";
    }

    public static boolean ktluong(String a) {
        if (a.matches("[0-9]{1,99}")) {
            return true;
        }
        return false;
    }

    public static boolean setluong(String a) {
        int i = Integer.parseInt(a);
        return i >= 5000000;
    }

    public static boolean Setten(String a) {
        String mau = "[a-zA-Z]{1}[a-zA-Z]{1,5}(\\s[a-zA-Z]{1,7}){0,4}\\s[a-zA-Z]{1,7}";
        return a.matches(mau);
    }

    public static boolean setma(String a) {
        return a.matches("QL[0-9]{3}")||a.matches("NV[0-9]{3}")||a.matches("TV[0-9]{3}")||
                a.matches("ql[0-9]{3}")||a.matches("nv[0-9]{3}")||a.matches("tv[0-9]{3}");
    }

    public static boolean setma(List<Object> list, String a,String b) {
        for (int i = 0; i < list.size(); i++) {
            if (b.equalsIgnoreCase(a)) {
                return false;
            }
        }
        return true;
    }

    public static boolean settuoi(String a) {
        String mau = "[0-9]{1,2}";
        int max = 55;
        int min = 16;
        return a.matches(mau) && (Integer.parseInt(a) >= min && Integer.parseInt(a) <= max) ? true : false;
    }

    public static boolean setmail(List<Object> list, String a,String b) {
        for (int i = 0; i < list.size(); i++) {
            if (b.equalsIgnoreCase(a)) {
                return false;
            }
        }
        return true;
    }

    public static boolean setmail(String a) {
        return a.matches("[a-z]{1,99}[0-9]{0,9}(\\.[a-z]{5,6}[0-9]{0,9}){0,4}\\@{1}[a-z]{3,9}\\.[a-z]{2,5}(\\.[a-z]{2,5}){0,3}");
    }

}
