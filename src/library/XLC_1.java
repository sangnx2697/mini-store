/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library;

import java.util.ArrayList;

/**
 *
 * @author xuansang
 */
public class XLC_1 {

    public static void main(String[] args) {
    
        System.out.println(cutDoubleEmpty("                  nguyen     xuan     sang    "));
    }

    // cắt ô trống thừa ở cuối
    public static String cutLastEmpty(String a) {
        ArrayList<String> arr = new ArrayList<>();
        String kq = "";
        for (int i = 0; i < a.length(); i++) {
            arr.add(a.substring(i, i + 1));
        }

        if (arr.get(arr.size() - 1).equals(" ")) {
            arr.remove(arr.size() - 1);
            for (int i = 0; i < arr.size(); i++) {
                kq = kq + arr.get(i);
            }
            return cutLastEmpty(kq);
        } else {
            for (int i = 0; i < arr.size(); i++) {
                kq = kq + arr.get(i);
            }
            return kq;

        }

    }
// cắt ô trống thừa ở  giữa

    public static String cutDoubleEmpty(String b) {
        String kq = "";
        int dem = 0;
        int demkt = 0;
        String a=b.trim();
        for (int i = 0; i < a.length(); i++) {
            if (a.substring(i, i + 1).equalsIgnoreCase(" ")) {
                demkt++;
            }
            if (a.substring(i, i + 1).equals(" ") && a.substring(i + 1, i + 2).matches("[a-zA-Z]")) {
                dem++;
            }
        }
        ArrayList<String> arr = new ArrayList<String>();
        for (int i = 0; i < a.length(); i++) {
            arr.add(a.substring(i, i + 1));
        }
        for (int i = 0; i < arr.size() - 1; i++) {

            if (arr.get(i).equals(" ") && arr.get(i + 1).equals(" ")) {
                arr.remove(i + 1);
            }
        }
        for (int i = 0; i < arr.size(); i++) {
            kq = kq + arr.get(i);
        }
        if (dem < demkt) {
            return cutDoubleEmpty(kq);
        } else {
            return kq;
        }

    }

    // cắt ô trống đầu tiên
    public static String cutFirstEmpty(String a) {

        ArrayList<String> arr = new ArrayList<>();
        String kq = "";
        for (int i = 0; i < a.length(); i++) {
            arr.add(a.substring(i, i + 1));
        }

        if (arr.get(0).equals(" ")) {
            arr.remove(0);
            for (int i = 0; i < arr.size(); i++) {
                kq = kq + arr.get(i);
            }
            return cutFirstEmpty(kq);
        } else {
            for (int i = 0; i < arr.size(); i++) {
                kq = kq + arr.get(i);
            }
            return kq;

        }
    }

    // ký tự tên đầu tiên in hoa tất cả in thường
    public static String ConvertString(String a) {
        String b[] = new String[a.length()];
        int c[] = XLC_1.CountEmptyString(a);
        int d[] = new int[c.length];
        d[0] = 0;
        for (int i = 1; i < c.length; i++) {
            d[i] = c[i] + 1;
        }
        d[d.length - 1] = a.length() - 1;
        String e[] = new String[a.length()];
        int chay = 0;
        for (int i = 0; i < a.length(); i++) {
            String t = a.substring(i, i + 1);
            if (i == d[chay] && chay < d.length - 1) {
                e[i] = t.toUpperCase();
                chay++;
            } else {
                e[i] = t.toLowerCase();
            }

        }
        String kq = "";
        for (int i = 0; i < e.length; i++) {
            kq = kq + e[i];
        }

        return kq;
    }

// sắp xếp theo ký tự đầu của tên
    static String[] SortTd(String a[]) {

        String mau = "abcdefghijklmnopqrstuvwxyz";
        String r[] = new String[a.length];

        for (int i = 0; i < a.length; i++) {
            int b[] = CountEmptyString(a[i]);
            String c = cutempty(a[i].substring(b[b.length - 2], b[b.length - 1])).substring(0, 1);
            r[i] = c;
        }
        System.out.println();
        int t[] = new int[a.length];
        for (int i = 0; i < r.length; i++) {
            for (int j = 0; j < mau.length(); j++) {
                if (r[i].equalsIgnoreCase(mau.substring(j, j + 1))) {
                    t[i] = j;

                }
            }
        }
        String kq[] = new String[a.length];
        for (int i = 0; i < t.length; i++) {
            for (int j = i + 1; j < t.length; j++) {
                if (t[i] > t[j]) {
                    String tam;
                    tam = a[i];
                    a[i] = a[j];
                    a[j] = tam;
                }

            }
            kq[i] = a[i];

        }
        return kq;
    }
// sort ký tự thứ 2

    static String[] Sort(String[] a) {
        String mau = "abcdefghijklmnopqrstuvwxyz";
        String r[] = new String[a.length];

        for (int i = 0; i < a.length; i++) {
            int b[] = CountEmptyString(a[i]);
            String c = cutempty(a[i].substring(b[b.length - 2], b[b.length - 1])).substring(1, 2);
            r[i] = c;

        }
        System.out.println();
        int t[] = new int[a.length];
        for (int i = 0; i < r.length; i++) {
            for (int j = 0; j < mau.length(); j++) {
                if (r[i].equalsIgnoreCase(mau.substring(j, j + 1))) {
                    t[i] = j;
                }
            }
        }
        String kq[] = new String[a.length];
        for (int i = 0; i < t.length; i++) {
            for (int j = i + 1; j < t.length; j++) {
                if (t[i] > t[j]) {
                    String tam;
                    tam = a[i];
                    a[i] = a[j];
                    a[j] = tam;
                }

            }
            kq[i] = a[i];
        }

        return SortTd(kq);
    }

    static String[] SortGd(String[] a) {
        String mau = "abcdefghijklmnopqrstuvwxyz";
        String r[] = new String[a.length];
        for (int i = 0; i < a.length; i++) {
            int b[] = CountEmptyString(a[i]);
            String c = cutempty(a[i].substring(b[b.length - 2], b[b.length - 1])).substring(0, 1);
            r[i] = c;
        }
        System.out.println();
        int t[] = new int[a.length];
        for (int i = 0; i < r.length; i++) {
            for (int j = 0; j < mau.length(); j++) {
                if (r[i].equalsIgnoreCase(mau.substring(j, j + 1))) {
                    t[i] = j;
                }
            }
        }
        String kq[] = new String[a.length];
        for (int i = 0; i < t.length; i++) {
            for (int j = i + 1; j < t.length; j++) {
                if (t[i] < t[j]) {
                    String tam;
                    tam = a[i];
                    a[i] = a[j];
                    a[j] = tam;
                }

            }
            kq[i] = a[i];
        }
        return Sort(kq);
    }
// cắt ký tự trống

    static String cutempty(String a) {
        String b;
        String c = "";

        for (int i = 0; i < a.length(); i++) {
            b = a.substring(i, i + 1);

            if (!b.equals(" ")) {
                c = c + b;

            }
        }
        return c;
    }
// đảo chuỗi

    static String change(String a) {
        String c = "";
        for (int i = 0; i < a.length(); i++) {
            if (a.length() >= 0) {
                String b = a.substring(a.length() - i - 1, a.length() - i);
                c = c + b;
            }
        }
        return c;
    }
// tìm vị trí ký tự trống xuất mảng chứa vị trí ký tự trống
 
    static int[] CountEmptyString(String a) {
        int dem = 1;
        for (int i = 0; i < a.length(); i++) {
            if (a.substring(i, i + 1).equals(" ")) {
                dem++;
            }
        }
        int b[] = new int[dem + 1];
        int chay = 1;
        for (int i = 0; i < a.length(); i++) {

            b[0] = 0;

            if (a.substring(i, i + 1).equals(" ")) {
                b[chay] = i;
                chay++;
            }
            if (i == a.length() - 1) {
                b[b.length - 1] = a.length();
            }
        }

        return b;
    }
    static int[] CountIndexString(String a,char t) {
        String ch ="";
        ch=ch+t;
        int dem = 1;
        for (int i = 0; i < a.length(); i++) {
            if (a.substring(i, i + 1).equals(ch)) {
                dem++;
            }
        }
        int b[] = new int[dem + 1];
        int chay = 1;
        for (int i = 0; i < a.length(); i++) {

            b[0] = 0;

            if (a.substring(i, i + 1).equals(ch)) {
                b[chay] = i;
                chay++;
            }
            if (i == a.length() - 1) {
                b[b.length - 1] = a.length();
            }
        }

        return b;
    }

    // chọn chuỗi số theo thứ tự dài nhất trong chuỗi nhập vào
    static void locchuoi(String a) {
        String b = "";
        b = b + a;

        int c[] = new int[a.length()];
        for (int i = 0; i < c.length; i++) {
            c[i] = Integer.parseInt(a.substring(i, i + 1));
        }

        System.out.println("");
        int dem = 0;
        String dem1 = "";
        int dem0 = 0;
        for (int i = 0; i < c.length - 1; i++) {
            if (c[i] <= c[i + 1]) {
                dem++;
                dem0++;
            }
            if (c[i] > c[i + 1]) {
                dem = 0;

            }
            dem1 = dem1 + dem;
        }

        char[] h = dem1.toCharArray();
        int k[] = new int[c.length - dem0];
        int chay = 1;
        k[0] = 0;
        k[k.length - 1] = h.length - 1;
        for (int i = 0; i < h.length; i++) {
            if (h[i] == '0') {
                k[chay] = i + 1;
                chay++;
            }
        }

        String t[] = new String[k.length - 1];
        int chay2 = 0;
        for (int i = 0; i < k.length - 1; i++) {
            String m = a.substring(k[i], k[i + 1]);

            if (chay2 < t.length) {
                t[chay2] = m;

            }
            chay2++;
        }
        String kq = "";
        for (int i = 0; i < t.length; i++) {

            for (int j = i + 1; j < t.length; j++) {
                if (t[i].length() < t[j].length()) {
                    String tam;
                    tam = t[i];
                    t[i] = t[j];
                    t[j] = tam;
                }
            }
        }
        System.out.println("Chuỗi theo thứ tự tăng dần \n dài nhất là: " + t[0]);
    }
}
