/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library;

import java.util.Scanner;

/**
 *
 * @author xuansang
 */
public class main {

    SinhVien[] sv;

    public static void main(String[] args) {
        try {
    
                
         new main().menu();
        } catch (Exception e) {
        }
        finally{
            try{
            new main().menu();
            } catch(Exception ex){
               
            }
        }
    }

    public void menu() throws Exception{
        System.out.println("1.Nhap SV");
        System.out.println("2.Xuat SV");
        System.out.println("3.Them SV");
        System.out.println("4.Xoa SV");
        System.out.print("moi chon:");
        int a = new Scanner(System.in).nextInt();
        switch (a) {
            case 1:
                nhap();
                menu();
                break;
            case 2:
                xuat();
                menu();
                break;
            case 3:
                them();
                menu();
                break;
            case 4:
                xoa();
                menu();
                break;
            default:
                menu();
                break;
        }
    }

    public void nhap() throws Exception{
        System.out.print("nhap so luong SV: ");
        int sl = new Scanner(System.in).nextInt();
        sv = new SinhVien[sl];
        for (int i = 0; i < sv.length; i++) {
            sv[i] = new SinhVien();
            System.out.print("nhap ten: ");
            sv[i].setName(new Scanner(System.in).nextLine());
            System.out.print("nhap diem: ");
            sv[i].setMark(new Scanner(System.in).nextDouble());
        }
    }

    public void xuat() throws Exception{
        for (SinhVien Sv : sv) {
            System.out.println("Sinh Vien:"+Sv.getName() + "/tDiem: "+Sv.getMark());
        }
    }

    public void them() throws Exception {
        System.out.print("Nhập tên Sv: ");
        String name = new Scanner(System.in).nextLine();
        System.out.print("nhập diem: ");
        double diem = new Scanner(System.in).nextDouble();
       sv= XLM.add(sv, sv.length, new SinhVien(name, diem));
    }

    public void xoa() throws Exception{
        boolean kt = true;
        int index =-1;
        System.out.print("nhap tên sv cần xóa: ");
        String name = new Scanner(System.in).nextLine();
        for (int i = 0; i < sv.length; i++) {
            if (name.equalsIgnoreCase(sv[i].getName())) {
                kt = false;
                index=i;
            }
        }
        if(kt==false){
           sv= XLM.remove(sv, index);
        }else{
            System.out.println("Không có sinh viên này");
        }
    }
}
