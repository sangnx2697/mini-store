/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library;

/**
 *
 * @author xuansang
 */
public class XLS {

    public static void main(String[] args) {
        System.out.println(convert(10.32323, 3));
    }

    public static double convert(String a, int witdh) {
 String b = String.valueOf(a);    
        return b.length()<4?Double.parseDouble(a):Double.parseDouble(a.substring(0, ((a.substring(a.lastIndexOf("."), a.lastIndexOf(".") + 1)).equals(".") ? a.lastIndexOf(".") : a.lastIndexOf(",")) + witdh));
    }

    public static double convert(double a, int witdh) {
        String b = String.valueOf(a);
     
        return b.length()<4?a:Double.parseDouble(b.substring(0, ((b.substring(b.lastIndexOf("."), b.lastIndexOf(".") + 1)).equals(".") ? b.lastIndexOf(".") : b.lastIndexOf(",")) + witdh));
    }

    static int max(int a, int b) {
        return a > b ? a : b;
    }

    static int max(int a, int b, int c) {
        return a > b ? (a > c ? a : c) : (b > c ? b : c);
    }

    static double max(double a, double b) {
        return a > b ? a : b;
    }

    static double max(double a, double b, double c) {
        return a > b ? (a > c ? a : c) : (b > c ? b : c);
    }

    static int min(int a, int b) {
        return a < b ? a : b;
    }

    static int min(int a, int b, int c) {
        return a < b ? (a < c ? a : c) : (b < c ? b : c);
    }

    static double min(double a, double b) {
        return a < b ? a : b;
    }

    static double min(double a, double b, double c) {
        return a < b ? (a < c ? a : c) : (b < c ? b : c);
    }

    static int grow(int a, int b) {
        int c = a;

        for (int i = 1; i < b; i++) {
            c = c * a;
        }
        return b == 0 ? 1 : (b == 1 ? a : c);
    }

    static double grow(double a, double b) {
        double c;
        c = a;
        for (int i = 1; i < b; i++) {
            c = c * a;
        }
        return c;

    }

}
