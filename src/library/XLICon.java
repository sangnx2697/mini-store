/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

/**
 *
 * @author xuansang
 */
public class XLICon {

    public static void ChangColorAndIcon(String IconPath, JLabel lbl, int WIDTH, int HEIGHT, Color color) {
        XLICon.getBackgroundIcon(IconPath, lbl, WIDTH, HEIGHT);
        lbl.setForeground(color);
    }

    public static Icon getBackground(String path, JLabel lb, int width, int heigh) {
        Icon aIcon = null;
        try {
            BufferedImage bi = ImageIO.read(new File(path));
            ImageIcon icon = new ImageIcon(bi.getScaledInstance(width, heigh, bi.SCALE_SMOOTH));
            lb.setIcon(icon);
            aIcon = icon;
        } catch (Exception e) {
        }
        return aIcon;
    }

    public static void getBackgroundIcon(String path, JLabel lb, int width, int heigh) {

        try {
            BufferedImage bi = ImageIO.read(new File(path));
            ImageIcon icon = new ImageIcon(bi.getScaledInstance(width, heigh, bi.SCALE_SMOOTH));
            lb.setIcon(icon);

        } catch (Exception e) {
        }

    }

    public static void getBackground(String path, JButton lb, int width, int heigh) {
        try {
            BufferedImage bi = ImageIO.read(new File(path));
            ImageIcon icon = new ImageIcon(bi.getScaledInstance(width, heigh, bi.SCALE_SMOOTH));
            lb.setIcon(icon);
       
        } catch (Exception e) {
        }
        
    }

    public static Icon geticon(String path, int width, int heigh) {
        Icon aIcon = null;
        try {
            BufferedImage bi = ImageIO.read(new File(path));
            ImageIcon icon = new ImageIcon(bi.getScaledInstance(width, heigh, bi.SCALE_SMOOTH));
            aIcon = icon;
        } catch (Exception e) {
        }
        return aIcon;
    }

    public static void getBackgroundIcon(String path, JButton lb, int width, int heigh) {

        try {
            BufferedImage bi = ImageIO.read(new File(path));
            ImageIcon icon = new ImageIcon(bi.getScaledInstance(width, heigh, bi.SCALE_AREA_AVERAGING));
            lb.setIcon(icon);

        } catch (Exception e) {
        }

    }
}
