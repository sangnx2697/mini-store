/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author vokie
 */
public class NhaCungCap_DAO {

    String url = "jdbc:sqlserver://127.0.0.1:1433;databaseName=QLSmartShop";
    //-----------------câu lệnh insert--------------------//
    public void InsNCC(String MaNCC, String TenNCC, String DiaChi) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "insert into NhaCungCap values (?, ?, ?)";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaNCC);
            st.setString(2, TenNCC);
            st.setString(3, DiaChi);
            st.executeUpdate();
            con.close();
            System.out.println("Ins thanh cong");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    //-----------------câu lệnh select--------------------//
    public void SelNCC(String MaNCC){
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "select * from NhaCungCap where MaNCC = ?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaNCC);
            st.execute();
            con.close();
            System.out.println("sel thanh cong");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    //-----------------câu lệnh update--------------------//
    public void UpNCC( String TenNCC, String Diachi,String MaNCC){
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "update NhaCungCap set TenNC = ?,DiaChi = ? where MaNCC = ?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, TenNCC);
            st.setString(2, Diachi);
            st.setString(3, MaNCC);
            st.executeUpdate();
            con.close();
            System.out.println("up thanh cong");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
        //-----------------câu lệnh Delete--------------------//

    public void DelNCC(String MaNCC){
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "delete from NhaCungCap where MaNCC = ?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaNCC);
            st.executeUpdate();
            con.close();
            System.out.println("del thanh cong");
        } catch (Exception e) {
        }
    }
    public void GetDataNCC(JTable tbl) {
        DefaultTableModel tb = (DefaultTableModel)tbl.getModel();
        tb.setRowCount(0);
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "select * from NhaCungCap";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                tb.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3)});
            }
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
      /* Câu lệnh Find */
    public void GetDataNCC(JTable tbl,String param,String column) {
        DefaultTableModel tb = (DefaultTableModel)tbl.getModel();
        tb.setRowCount(0);
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "select * from NhaCungCap where "+column+" like '%"+param+"%'";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                tb.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3)});
            }
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
