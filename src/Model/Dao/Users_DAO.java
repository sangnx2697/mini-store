/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Dao;

import Model.Table.Users;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author vokie
 */
public class Users_DAO {
   Connection con ;
    public Users_DAO() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databaseName=QLSmartShop;user=sa;password=123;");
        } catch (Exception ex) {
            Logger.getLogger(Users_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    String url = "jdbc:sqlserver://localhost:1433;databaseName=QLSmartShop";

    //-----------------câu lệnh insert--------------------//
    public void InsUsers(String MaNS, String Username, String Pass, String Role, String avt) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "insert into Users values (?, ?, ?, ?, ?)";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaNS);
            st.setString(2, Username);
            st.setString(3, Pass);
            st.setString(4, Role);
            st.setString(5, avt);
            st.executeUpdate();
            con.close();
            System.out.println("ins thanh cong");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    //-----------------câu lệnh select--------------------//
    public String checkUser(String Name, String pass) {
        try {

            String sql = "select role from Users where UserName = ? and Password = ?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, Name);
            st.setString(2, pass);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                return rs.getString(1);
            }
            st.close();
            rs.close();
            con.close();

        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public void SelUsers(String MaNS) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "select * from Users where MaNV = ?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaNS);
            st.execute();
            con.close();
            System.out.println("sel thanh cong");
        } catch (Exception e) {
        }
    }

    //-----------------câu lệnh update--------------------//
    public void UpUsers(String Username, String Pass, String Role, String avt, String MaNS) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "update Users set username = ?, password = ?, role = ?, avatar = ? where MaNV = ? ";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, Username);
            st.setString(2, Pass);
            st.setString(3, Role);
            st.setString(4, avt);
            st.setString(5, MaNS);
            st.executeUpdate();
            con.close();
            System.out.println("Up thanh cong");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    //-----------------câu lệnh Delete--------------------//

    public void DelUsers(String MaNS) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "delete from Users where MaNV = ?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaNS);
            st.executeUpdate();
            con.close();
            System.out.println("del thanh cong");
        } catch (Exception e) {
        }
    }
    public void GetDataUsers(JTable tbl,JComboBox cbb) {
        DefaultTableModel tb = (DefaultTableModel)tbl.getModel();
        tb.setRowCount(0);
        cbb.removeAllItems();
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "select * from Users";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                tb.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5)});
            }
            rs = st.executeQuery("Select * from LuongNV");
            while (rs.next()) {
                cbb.addItem(rs.getString(1));
            }
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
                            /* Câu lệnh Find */
     public void GetDataUsers(JTable tbl,String param,String column) {
        DefaultTableModel tb = (DefaultTableModel)tbl.getModel();
        tb.setRowCount(0);
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "select * from Users where "+column+" like '%"+param+"%'";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                tb.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5)});
            }
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
