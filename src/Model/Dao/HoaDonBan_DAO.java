/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Dao;

import static Model.Dao.CTHDB_DAO.url;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author vokie
 */
public class HoaDonBan_DAO {

    String url = "jdbc:sqlserver://127.0.0.1:1433;databaseName=QLSmartShop";

    //-----------------câu lệnh insert--------------------//
    public void InsHDB(String MaHDB, String MaNS, String NgayLap, String SoTien) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "insert into HoaDonBan values (?, ?, ?, ?)";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaHDB);
            st.setString(2, MaNS);
            st.setString(3, NgayLap);
            st.setString(4, SoTien);
            st.executeUpdate();
            con.close();
            System.out.println(" ins thanh cong");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    //-----------------câu lệnh select--------------------//
    public void SelHDB(String MaHDB) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "select * from HoaDonBan where MaHDB = ?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaHDB);
            st.execute();
            con.close();
            System.out.println("thanh cong");
        } catch (Exception e) {
        }
    }

    //-----------------câu lệnh update--------------------//
    public void UpHDB(String MaNS, String NgayLap, String SoTien, String MaHDB) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "update HoaDonBan set MaNV = ?, NgayLap = ?,ThanhTien = ? where MaHDB = ?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaNS);
            st.setString(2, NgayLap);
            st.setString(3, SoTien);
            st.setString(4, MaHDB);
            st.executeUpdate();
            con.close();
            System.out.println("update thanh cong");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    //-----------------câu lệnh Delete--------------------//

    public void DelHDB(String MaHDB) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "delete from HoaDonBan where MaHDB = ?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaHDB);
            st.executeUpdate();
            con.close();
            System.out.println("del thanh cong");
        } catch (Exception e) {
        }
    }
    public void GetDataHDB(JTable tbl) {
        DefaultTableModel tb = (DefaultTableModel)tbl.getModel();
        tb.setRowCount(0);
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "SELECT HoaDonBan.MaHDB,MaNV,NgayLap,MaSP,DonGia,SoLuong,CTHDB.ThanhTien FROM HoaDonBan INNER JOIN CTHDB ON HoaDonBan.MaHDB=CTHDB.MaHDB";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                tb.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7)});
            }
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    /* Câu lệnh Find */
    public void GetDataHDB(JTable tbl, String columnInner,String param,String column) {
        DefaultTableModel tb = (DefaultTableModel)tbl.getModel();
        tb.setRowCount(0);
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "SELECT HoaDonBan.MaHDB, MaNV, NgayLap, MaSP, DonGia, SoLuong, CTHDB.ThanhTien FROM HoaDonBan INNER JOIN CTHDB ON HoaDonBan.MaHDB=CTHDB.MaHDB  where "+columnInner+"."+column+" like '%"+param+"%'";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                tb.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7)});
            }
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
