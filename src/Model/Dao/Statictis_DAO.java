/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Dao;

import Model.Table.Statictis;
import Model.Table.Statictis_Finacial;
import Model.Table.Statictis_InvoiceIn;
import Model.Table.Statictis_InvoiceOut;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author nhm95
 */
public class Statictis_DAO {

    public static final int all = 0;
    public static final int T1 = 1;
    public static final int T2 = 2;
    public static final int T3 = 3;
    public static final int T4 = 4;
    public static final int T5 = 5;
    public static final int T6 = 6;
    public static final int T7 = 7;
    public static final int T8 = 8;
    public static final int T9 = 9;
    public static final int T10 = 10;
    public static final int T11 = 11;
    public static final int T12 = 12;
    public static final String DT = "sp_DoanhThuThang ";
    public static final String LN = "sp_LoiNhuanThang ";
    public static final String CT = "sp_ChiThang ";

    static final String url = "jdbc:sqlserver://localhost:1433;databaseName=QLSmartShop";

    public static void Statictis(JTable tbl, int ACTION, String Table) {
        Connection con = null;
        DefaultTableModel model = null;
        ResultSet rs = null;
        String sql = "exec " + Table + " ";
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(url, "sa", "123");
            Statement st = con.createStatement();
            switch (ACTION) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                    sql += "" + ACTION + "";
                    rs = st.executeQuery(sql);
                    switch (Table) {
                        case DT:
                            model = new DefaultTableModel(new Object[]{"Product ID", "Month", "Finalcial"}, 0);
                            while (rs.next()) {
                                model.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3) + "$"});
                            }
                            break;
                        case CT:
                            model = new DefaultTableModel(new Object[]{"Invoice ID", "Employee ID", "Month", "Money Bought", "Finalcial"}, 0);
                            while (rs.next()) {
                                model.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3), rs.getDouble(4) + "$", rs.getDouble(5) + "$"});

                            }
                            break;
                        case LN:
                            model = new DefaultTableModel(new Object[]{"Month", "Invoices Sold Total", "Salary Total", "Invoice Bought", "Finalcial"}, 0);
                            while (rs.next()) {
                                model.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5) + "$"});
                            }
                    }
                    tbl.setModel(model);
                    break;
                default:
                    sql += "" + null + "";
                    rs = st.executeQuery(sql);
                    switch (Table) {
                        case DT:
                            model = new DefaultTableModel(new Object[]{"Month", "Finalcial"}, 0);
                            while (rs.next()) {
                                model.addRow(new Object[]{rs.getString(1), rs.getString(2) + "$"});
                            }
                            tbl.setModel(model);
                            break;
                        case CT:
                            model = new DefaultTableModel(new Object[]{"Month", "Finalcial"}, 0);
                            while (rs.next()) {
                                model.addRow(new Object[]{rs.getString(1), rs.getString(2) + "$"});
                            }
                            tbl.setModel(model);
                            break;

                        case LN:
                            model = new DefaultTableModel(new Object[]{"Month", "Invoices Sold Total", "Salary Total", "Invoice Bought", "Finalcial"}, 0);
                            while (rs.next()) {
                                model.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5) + "$"});
                            }
                            tbl.setModel(model);
                            break;
                    }

            }

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                System.out.println(e);
            }

        }
    }

    public static ArrayList<Statictis> StatictisData(JTable tbl, int ACTION, String Table) {
        Connection con = null;
        DefaultTableModel model = null;
        ResultSet rs = null;
        String sql = "exec " + Table + " ";
        ArrayList<Statictis> arr = new ArrayList<>();
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(url, "sa", "123");
            Statement st = con.createStatement();
            switch (ACTION) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                    sql += "" + ACTION + "";
                    rs = st.executeQuery(sql);
                    switch (Table) {
                        case DT:
                            model = new DefaultTableModel(new Object[]{"Product ID", "Month", "Finalcial"}, 0);
                            while (rs.next()) {
                                model.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3) + "$"});
                                arr.add(new Statictis(new Statictis_InvoiceIn(rs.getString(1), rs.getString(3))));
                            }
                            return arr;
                        case CT:
                            model = new DefaultTableModel(new Object[]{"Invoice ID", "Employee ID", "Month", "Money Bought", "Finalcial"}, 0);
                            while (rs.next()) {
                                model.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3), rs.getDouble(4) + "$", rs.getDouble(5) + "$"});
                                arr.add(new Statictis(new Statictis_InvoiceOut(rs.getString(1), rs.getString(4))));

                            }
                            return arr;
                        case LN:
                            model = new DefaultTableModel(new Object[]{"Month", "Invoices Sold Total", "Salary Total", "Invoice Bought", "Finalcial"}, 0);
                            while (rs.next()) {
                                model.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5) + "$"});
                                arr.add(new Statictis(new Statictis_InvoiceOut(rs.getString(1), rs.getString(5))));
                            }
                    }

                    tbl.setModel(model);
                    break;
                default:
                    sql += "" + null + "";
                    rs = st.executeQuery(sql);
                    switch (Table) {
                        case DT:
                            model = new DefaultTableModel(new Object[]{"Month", "Finalcial"}, 0);
                            while (rs.next()) {
                                model.addRow(new Object[]{rs.getString(1), rs.getString(2) + "$"});
                                arr.add(new Statictis(new Statictis_InvoiceIn(rs.getString(1), rs.getDouble(2))));
                            }
                            tbl.setModel(model);
                            return arr;
                        case CT:
                            model = new DefaultTableModel(new Object[]{"Month", "Finalcial"}, 0);
                            while (rs.next()) {
                                model.addRow(new Object[]{rs.getString(1), rs.getString(2) + "$"});
                                arr.add(new Statictis(new Statictis_InvoiceOut(rs.getDouble(2), rs.getString(1) + "")));
                            }
                            tbl.setModel(model);
                            return arr;

                        case LN:
                            model = new DefaultTableModel(new Object[]{"Month", "Invoices Sold Total", "Salary Total", "Invoice Bought", "Finalcial"}, 0);
                            while (rs.next()) {
                                model.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5)});
                                arr.add(new Statictis(new Statictis_Finacial(rs.getString(1), rs.getDouble(5) + "")));
                            }
                            tbl.setModel(model);
                            return arr;
                    }

            }

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                System.out.println(e);
            }

        }
        return null;
    }

    public void SelInvoiceIn(String MaNS) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "select * from nhanvien where MaNV = ?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaNS);
            st.execute();
            con.close();
            System.out.println("sel thanh cong");
        } catch (Exception e) {
        }
    }
}
