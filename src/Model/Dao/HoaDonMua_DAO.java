/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author vokie
 */
public class HoaDonMua_DAO {
     String url = "jdbc:sqlserver://127.0.0.1:1433;databaseName=QLSmartShop";

    //-----------------câu lệnh insert--------------------//
    public void InsHDM(String MaHDM, String MaNS, String MaNC, String NgayLap, String ThanhTien) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "insert into HoaDonMua values (?, ?, ?, ?, ?)";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaHDM);
            st.setString(2, MaNS);
            st.setString(3, MaNC);
            st.setString(4, NgayLap);
            st.setString(5, ThanhTien);
            st.executeUpdate();
            con.close();
            System.out.println("ins thanh cong");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    //-----------------câu lệnh select--------------------//
    public void SelHDM(String MaHDM) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "select * from HoaDonMua where MaHDM = ?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaHDM);
            st.execute();
            con.close();
            System.out.println("select thanh cong");
        } catch (Exception e) {
        }
    }

    //-----------------câu lệnh update--------------------//
    public void UpHDM(String MaNS, String MaNC, String NgayLap, String ThanhTien, String MaHDM) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "update HoaDonMua set MaNV = ?,MaNCC = ?, NgayLap = ?,ThanhTien = ? where MaHDM = ?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaNS);
            st.setString(2, MaNC);
            st.setString(3, NgayLap);
            st.setString(4, ThanhTien);
            st.setString(5, MaHDM);
            st.executeUpdate();
            con.close();
            System.out.println("up thanh cong");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    //-----------------câu lệnh Delete--------------------//

    public void DelHDM(String MaHDM) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "delete from HoaDonMua where MaHDM = ?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaHDM);
            st.executeUpdate();
            con.close();
            System.out.println("del thanh cong");
        } catch (Exception e) {
        }
    }
    public void GetDataHDM(JTable tbl) {
        DefaultTableModel tb = (DefaultTableModel)tbl.getModel();
        tb.setRowCount(0);
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "SELECT HoaDonMua.MaHDM,MaNV,MaSP,MaNCC,Dongia,Soluong,NgayLap,CTHDM.ThanhTien FROM HoaDonMua INNER JOIN CTHDM ON HoaDonMua.MaHDM=CTHDM.MaHDM";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                tb.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8)});
            }
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    /* Câu lệnh Find */
    public void GetDataHDM(JTable tbl, String columnInner,String param,String column) {
        DefaultTableModel tb = (DefaultTableModel)tbl.getModel();
        tb.setRowCount(0);
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "SELECT HoaDonMua.MaHDM,MaNV,MaSP,MaNCC,Dongia,Soluong,NgayLap,CTHDM.ThanhTien FROM HoaDonMua INNER JOIN CTHDM ON HoaDonMua.MaHDM=CTHDM.MaHDM WHERE "+columnInner+"."+column+" like '%"+param+"%'";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                tb.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8)});
            }
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
