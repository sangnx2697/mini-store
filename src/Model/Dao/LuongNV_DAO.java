/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author vokie
 */
public class LuongNV_DAO {
    String url = "jdbc:sqlserver://127.0.0.1:1433;databaseName=QLSmartShop";
    //-----------------câu lệnh insert--------------------//
    public void InsLuongNV(String MaCV, String ChucVu, String MucLuong) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "insert into LuongNV values (?, ?, ?)";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaCV);
            st.setString(2, ChucVu);
            st.setString(3, MucLuong);
            st.executeUpdate();
            con.close();
            System.out.println(" ins thanh cong");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    //-----------------câu lệnh select--------------------//
    public void SelLuongNV(String MaCV){
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "select * from LuongNV where MaCV = ?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaCV);
            st.execute();
            con.close();
            System.out.println("thanh cong");
        } catch (Exception e) {
        }
    }
    //-----------------câu lệnh update--------------------//
    public void UpLuongNV( String ChucVu, String MucLuong,String MaCV){
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "update LuongNV set ChucVu = ?,MucLuong = ? where MaCV = ?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, ChucVu);
            st.setString(2, MucLuong);
            st.setString(3, MaCV);
            st.executeUpdate();
            con.close();
            System.out.println("update thanh cong");
        } catch (Exception e) {
        }
    }
        //-----------------câu lệnh Delete--------------------//

    public void DelLuongNV(String MaCV){
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "delete from LuongNV where MaCV = ?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaCV);
            st.executeUpdate();
            con.close();
            System.out.println("del thanh cong");
        } catch (Exception e) {
        }
    }
    public void GetDataLuongNV(JTable tbl){
        DefaultTableModel tb = (DefaultTableModel)tbl.getModel();
        tb.setRowCount(0);
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "select * from LuongNV";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                tb.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3)});
            }
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
                                    /* Câu lệnh Find */
    public void GetDataLuongNV(JTable tbl,String param,String column){
        DefaultTableModel tb = (DefaultTableModel)tbl.getModel();
        tb.setRowCount(0);
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "select * from LuongNV where "+column+" like '%"+param+"%'";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                tb.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3)});
            }
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    } 
}
