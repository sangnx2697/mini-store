/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

/**
 *
 * @author vokie
 */
public class CTHDM_DAO {
    String url = "jdbc:sqlserver://127.0.0.1:1433;databaseName=QLSmartShop";

    //-----------------câu lệnh insert--------------------//
    public void InsCTHDM(String MaHDM, String MaSP, String DonGia, int Soluong, String ThanhTien) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "insert into CTHDM values (?, ?, ?, ?, ?)";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaHDM);
            st.setString(2, MaSP);
            st.setString(3, DonGia);
            st.setInt(4, Soluong);
            st.setString(5, ThanhTien);
            st.executeUpdate();
            con.close();
            System.out.println("Ins thanh cong");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    //-----------------câu lệnh select--------------------//
    public void SelCTHDM(String MaHDM) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "select * from CTHDM where MaHDM = ?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaHDM);
            st.execute();
            con.close();
            System.out.println("select thanh cong");
        } catch (Exception e) {
        }
    }

    //-----------------câu lệnh update--------------------//
    public void UpCTHDM( String DonGia, int Soluong, String ThanhTien ,String MaHDM, String MaSP) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "update CTHDM set DonGia = ?,SoLuong = ?, ThanhTien = ? where MaHDM = ? and MaSP = ?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, DonGia);
            st.setInt(2, Soluong);
            st.setString(3, ThanhTien);
            st.setString(4, MaHDM);
            st.setString(5, MaSP);
            st.executeUpdate();
            con.close();
            System.out.println("up thanh cong");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    //-----------------câu lệnh Delete--------------------//

    public void DelCTHDM(String MaHDM) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "delete from CTHDM where MaHDM = ?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaHDM);
            st.executeUpdate();
            con.close();
            System.out.println("del thanh cong");
        } catch (Exception e) {
        }
    }
}
