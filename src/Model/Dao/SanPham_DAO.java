/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author vokie
 */
public class SanPham_DAO {
     String url = "jdbc:sqlserver://127.0.0.1:1433;databaseName=QLSmartShop";

    public SanPham_DAO() {
    }
    
    //-----------------câu lệnh insert--------------------//
    public void InsSanPham(String MaSP, String TenSP, String MaNC, String DonViTinh,String DonGia ,int SoLuong) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "insert into SanPham values (?, ?, ?, ?, ?, ?)";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaSP);
            st.setString(2, TenSP);
            st.setString(3, MaNC);
            st.setString(4, DonViTinh);
            st.setString(5, DonGia);
            st.setInt(6, SoLuong);
            st.executeUpdate();
            System.out.println("ins thanh cong");
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    //-----------------câu lệnh select--------------------//
    public void SelSanPham(String MaSP) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "select * from SanPham where MaSP = ?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaSP);
            st.execute();
            con.close();
            System.out.println("sel thanh cong");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    //-----------------câu lệnh update--------------------//
    public void UpSanPham(String TenSP, String MaNC, String DonViTinh,String DonGia ,int SoLuong, String MaSP) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "update SanPham set TenSP = ?, MaNC = ?, DonViTinh = ?, DonGia = ?,SoLuong = ? where MaSP = ? ";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, TenSP);
            st.setString(2, MaNC);
            st.setString(3, DonViTinh);
            st.setString(4, DonGia);
            st.setInt(5, SoLuong);
            st.setString(6, MaSP);
            st.executeUpdate();
            con.close();
            System.out.println("up thanh cong");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    //-----------------câu lệnh Delete--------------------//

    public void DelSanPham(String MaSP) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "delete from SanPham where MaSP = ?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaSP);
            st.executeUpdate();
            con.close();
            System.out.println("del thanh cong");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public void GetDataSP(JTable tbl) {
        DefaultTableModel tb = (DefaultTableModel)tbl.getModel();
        tb.setRowCount(0);
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "SELECT * from SanPham";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                tb.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6)});
            }
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
      /* Câu lệnh Find */
    public void GetDataSP(JTable tbl,String param,String column) {
        DefaultTableModel tb = (DefaultTableModel)tbl.getModel();
        tb.setRowCount(0);
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "SELECT * from SanPham where "+column+" like '%"+param+"%'";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                tb.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6)});
            }
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
        public void GetDataSP(JTable tbl, JComboBox cbb) {
        DefaultTableModel tb = (DefaultTableModel) tbl.getModel();
        tb.setRowCount(0);
        cbb.removeAllItems();
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "SELECT * from SanPham";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                tb.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6)});
            }
            rs = st.executeQuery("Select DonViTinh From SanPham Group by DonViTinh");

            while (rs.next()) {
                cbb.addItem(rs.getString(1));
            }
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
