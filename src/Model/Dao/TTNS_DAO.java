/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author vokie
 */
public class TTNS_DAO {
    String url = "jdbc:sqlserver://127.0.0.1:1433;databaseName=QLSmartShop";

    //-----------------câu lệnh insert--------------------//
    public void InsTTNS(String MaNS, String Hoten, String Diachi, String CMND,String MaCV ,String avt) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "insert into nhanvien values (?, ?, ?, ?, ?, ?)";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaNS);
            st.setString(2, Hoten);
            st.setString(3, Diachi);
            st.setString(4, CMND);
            st.setString(5, MaCV);
            st.setString(6, avt);
            st.executeUpdate();
            con.close();
            System.out.println("ins thanh cong");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    //-----------------câu lệnh select--------------------//
    public void SelTTNS(String MaNS) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "select * from nhanvien where MaNV = ?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaNS);
            st.execute();
            con.close();
            System.out.println("sel thanh cong");
        } catch (Exception e) {
        }
    }

    //-----------------câu lệnh update--------------------//
    public void UpTTNS(String Hoten, String Diachi, String CMND,String MaCV ,String avt, String MaNS) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "update nhanvien set Hoten = ?, Diachi = ?, CMND = ?, MaCV = ?,avatar = ? where MaNV = ? ";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, Hoten);
            st.setString(2, Diachi);
            st.setString(3, CMND);
            st.setString(4, MaCV);
            st.setString(5, avt);
            st.setString(6, MaNS);
            st.executeUpdate();
            con.close();
            System.out.println("up thanh cong");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    //-----------------câu lệnh Delete--------------------//

    public void DelTTNS(String MaNS) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "delete nhanvien where MaNV = ?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaNS);
            st.executeUpdate();
            con.close();
            System.out.println("del thanh cong");
            
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public void GetDataNS(JTable tbl,JComboBox cbb) {
        DefaultTableModel tb = (DefaultTableModel)tbl.getModel();
        tb.setRowCount(0);
        cbb.removeAllItems();
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "SELECT * from NhanVien";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                tb.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6)});
            }
            rs = st.executeQuery("Select * from LuongNV");
            while (rs.next()) {
                cbb.addItem(rs.getString(1));
            }
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
      /* Câu lệnh Find */
    public void GetDataNS(JTable tbl,String param,String column) {
        DefaultTableModel tb = (DefaultTableModel)tbl.getModel();
        tb.setRowCount(0);
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url, "sa", "123");
            String sql = "select * from NhanVien where "+column+" like '%"+param+"%'";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                tb.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),rs.getString(6)});
            }
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
