/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author vokie
 */
public class CTHDB_DAO {

    static final String url = "jdbc:sqlserver://localhost:1433;databaseName=QLSmartShop";
    static Connection con=null;

    //-----------------câu lệnh insert--------------------//
    public void InsCTHDB(String MaHDB, String MaSP, String DonGia, int Soluong, String ThanhTien) {
        try {
            System.out.println("");
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(url,"sa","123");
            String sql = "insert into CTHDB values (?, ?, ?, ?, ?)";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaHDB);
            st.setString(2, MaSP);
            st.setString(3, DonGia);
            st.setInt(4, Soluong);
            st.setString(5, ThanhTien);
            st.executeUpdate();
            con.close();
            System.out.println("ins thanh cong");

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    //-----------------câu lệnh select--------------------//
    public void SelCTHDB(String MaHDB) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(url, "sa", "123");
            String sql = "select * from CTHDB where MaHDB = ?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaHDB);
            st.execute();
            con.close();
            System.out.println("select thanh cong");
        } catch (Exception e) {
        }
    }

    //-----------------câu lệnh update--------------------//
    public void UpCTHDB(String DonGia, int Soluong, String ThanhTien, String MaHDB, String MaSP) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(url, "sa", "123");
            String sql = "update CTHDB set DonGia = ?,SoLuong = ?, ThanhTien = ? where MaHDB = ? and MaSP = ?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, DonGia);
            st.setInt(2, Soluong);
            st.setString(3, ThanhTien);
            st.setString(4, MaHDB);
            st.setString(5, MaSP);
            st.executeUpdate();
            con.close();
            System.out.println("update thanh cong");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    //-----------------câu lệnh Delete--------------------//

    public void DelCTHDB(String MaHDB) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(url, "sa", "123");
            String sql = "delete from CTHDB where MaHDB = ?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, MaHDB);
            st.executeUpdate();
            con.close();
            System.out.println("del thanh cong");
        } catch (Exception e) {
        }
    }
      
    
}
