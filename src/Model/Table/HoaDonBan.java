/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Table;

import Model.Dao.HoaDonBan_DAO;
import javax.swing.JTable;
import library.XLDL;

/**
 *
 * @author vokie
 */
public class HoaDonBan extends HoaDonBan_DAO {

    String MaHDB;
    String MaNS;
    String NgayLap;
    String SoTien;

    public String getMaHDB() {
        return MaHDB;
    }

    public HoaDonBan(String MaHDB, String MaNS, String NgayLap, String SoTien, int Action) {
        this.MaHDB = MaHDB;
        this.MaNS = MaNS;
        this.NgayLap = NgayLap;
        this.SoTien = SoTien;
        Action(Action);
    }

    public HoaDonBan(String MaNS, String NgayLap, String SoTien, int Action) {
        Action(Action);
    }

    public HoaDonBan(String MaHDB, int Action) {
        this.MaHDB = MaHDB;
        Action(Action);
    }

    public HoaDonBan() {
    }

    public void setMaHDB(String MaHDB) {
        this.MaHDB = MaHDB;
    }

    public String getMaNS() {
        return MaNS;
    }

    public void setMaNS(String MaNS) {
        this.MaNS = MaNS;
    }

    public String getNgayLap() {
        return NgayLap;
    }

    public void setNgayLap(String NgayLap) {
        this.NgayLap = NgayLap;
    }

    public String getSoTien() {
        return SoTien;
    }

    public void setSoTien(String SoTien) {
        this.SoTien = SoTien;
    }

    @Override
    public void InsHDB(String MaHDB, String MaNS, String NgayLap, String SoTien) {
        super.InsHDB(MaHDB, MaNS, NgayLap, SoTien); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void SelHDB(String MaHDB) {
        super.SelHDB(MaHDB); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void UpHDB(String MaNS, String NgayLap, String SoTien, String MaHDB) {
        super.UpHDB(MaNS, NgayLap, SoTien, MaHDB); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void DelHDB(String MaHDB) {
        super.DelHDB(MaHDB); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void GetDataHDB(JTable tbl) {
        super.GetDataHDB(tbl); //To change body of generated methods, choose Tools | Templates.
    }

    private void InsertHDB() {
        if (MaHDB.equals("")) {
            Diagram.Dialog.Showmess(null, "Code HoaDonBan mustn't be empty", 0, null);
        } else if (MaNS.equals("")) {
            Diagram.Dialog.Showmess(null, "Code NhanSu mustn't be empty", 0, null);
        } else if (NgayLap.equals("")) {
            Diagram.Dialog.Showmess(null, "Ngay Lap mustn't be empty", 0, null);
        } else if (SoTien.equals("")) {
            Diagram.Dialog.Showmess(null, "The money mustn't be empty", 0, null);
        } else if ((SoTien.length() < 1 ? true : XLDL.checkCharaterByABC(SoTien) ? (Double.parseDouble(SoTien) >= 0 ? false : true) : true)) {
            Diagram.Dialog.Showmess(null, "The money must be greater than 0 and don't write words", 0, null);
        } else {
            if (XLDL.setMaHDB(MaHDB)) {
                if (XLDL.setmaNS(MaNS)) {
                    InsHDB(MaHDB, MaNS, NgayLap, SoTien);
                } else {
                    Diagram.Dialog.Showmess(null, "Code MaNS isn't correct", 0, null);
                }
            } else {
                Diagram.Dialog.Showmess(null, "Code HoaDonBan isn't correct", 0, null);
            }
        }
    }

    private void SelectHDB() {
        if (MaHDB.equals("")) {
            Diagram.Dialog.Showmess(null, "Code HoaDonBan mustn't be empty", 0, null);
        } else if (!XLDL.setMaHDB(MaHDB)) {
            Diagram.Dialog.Showmess(null, "Code HoaDonBan isn't correct", 0, null);
        } else {
            SelHDB(MaHDB);
        }
    }

    private void UpdateHDB() {
        if (MaHDB.equals("")) {
            Diagram.Dialog.Showmess(null, "Code HoaDonBan mustn't be empty", 0, null);
        } else if (MaNS.equals("")) {
            Diagram.Dialog.Showmess(null, "Code NhanSu mustn't be empty", 0, null);
        } else if (NgayLap.equals("")) {
            Diagram.Dialog.Showmess(null, "Ngay Lap mustn't be empty", 0, null);
        } else if (SoTien.equals("")) {
            Diagram.Dialog.Showmess(null, "The money mustn't be empty", 0, null);
        } else if ((SoTien.length() < 1 ? true : XLDL.checkCharaterByABC(SoTien) ? (Double.parseDouble(SoTien) >= 0 ? false : true) : true)) {
            Diagram.Dialog.Showmess(null, "The money must be greater than 0", 0, null);
        } else {
            if (XLDL.setMaHDB(MaHDB)) {
                if (XLDL.setmaNS(MaNS)) {
                    UpHDB(MaNS, NgayLap, SoTien, MaHDB);
                } else {
                    Diagram.Dialog.Showmess(null, "Code MaNS isn't correct", 0, null);
                }
            } else {
                Diagram.Dialog.Showmess(null, "Code HoaDonBan isn't correct", 0, null);
            }
        }
    }

    private void DeleteHDB() {
        if (MaHDB.equals("")) {
            Diagram.Dialog.Showmess(null, "Code HoaDonBan mustn't be empty", 0, null);
        } else if (!XLDL.setMaHDB(MaHDB)) {
            Diagram.Dialog.Showmess(null, "Code HoaDonBan isn't correct", 0, null);
        } else {
            DelHDB(MaHDB);
        }
    }
    public static final int INSERT = 1;

    public static final int SELECT = 2;

    public static final int UPDATE = 3;

    public static final int DELETE = 4;

    public void Action(int Action) {
        switch (Action) {
            case INSERT:
                InsertHDB();
                break;
            case UPDATE:
                UpdateHDB();
                break;
            case DELETE:
                DeleteHDB();
                break;
            case SELECT:
                SelectHDB();
                break;
        }
    }
}
