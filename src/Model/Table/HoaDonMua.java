/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Table;

import Model.Dao.HoaDonMua_DAO;
import javax.swing.JTable;
import library.XLDL;

/**
 *
 * @author vokie
 */
public class HoaDonMua extends HoaDonMua_DAO {

    String MaHDM;
    String MaNS;
    String MaNC;
    String NgayLap;
    String ThanhTien;

    public String getMaHDM() {
        return MaHDM;
    }

    public void setMaHDM(String MaHDM) {
        this.MaHDM = MaHDM;
    }

    public String getMaNS() {
        return MaNS;
    }

    public void setMaNS(String MaNS) {
        this.MaNS = MaNS;
    }

    public String getMaNC() {
        return MaNC;
    }

    public void setMaNC(String MaNC) {
        this.MaNC = MaNC;
    }

    public String getNgayLap() {
        return NgayLap;
    }

    public void setNgayLap(String NgayLap) {
        this.NgayLap = NgayLap;
    }

    public String getSoTien() {
        return ThanhTien;
    }

    public void setSoTien(String SoTien) {
        this.ThanhTien = SoTien;
    }

    public HoaDonMua(String MaHDM, String MaNS,String MaNC, String NgayLap, String ThanhTien, int Action) {
        this.MaHDM = MaHDM;
        this.MaNS = MaNS;
        this.MaNC = MaNC;
        this.NgayLap = NgayLap;
        this.ThanhTien = ThanhTien;
        Action(Action);
    }

    public HoaDonMua(String MaHDM, int Action) {
        this.MaHDM = MaHDM;
        Action(Action);
    }

    public HoaDonMua() {
    }

    @Override
    public void InsHDM(String MaHDM, String MaNS, String MaNC, String NgayLap, String ThanhTien) {
        super.InsHDM(MaHDM, MaNS, MaNC, NgayLap, ThanhTien); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void SelHDM(String MaHDM) {
        super.SelHDM(MaHDM); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void UpHDM(String MaNS, String MaNC, String NgayLap, String ThanhTien, String MaHDM) {
        super.UpHDM(MaNS, MaNC, NgayLap, ThanhTien, MaHDM); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void DelHDM(String MaHDM) {
        super.DelHDM(MaHDM); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void GetDataHDM(JTable tbl) {
        super.GetDataHDM(tbl); //To change body of generated methods, choose Tools | Templates.
    }

    private void InsertHDM() {
        if (MaHDM.equals("")) {
            new Diagram.Dialog().Showmess(null, "Code HoaDonMua mustn't be empty", 0, null);
        } else if (MaNS.equals("")) {
            new Diagram.Dialog().Showmess(null, "Code NhanSu mustn't be empty", 0, null);
        } else if (MaNC.equals("")) {
            new Diagram.Dialog().Showmess(null, "Code NhaCungCap mustn't be empty", 0, null);
        } else if (NgayLap.equals("")) {
            new Diagram.Dialog().Showmess(null, "Ngay Lap mustn't be empty", 0, null);
        } else if (ThanhTien.equals("")) {
            new Diagram.Dialog().Showmess(null, "The money mustn't be empty", 0, null);
        } else if ((ThanhTien.length() < 1 ? true : XLDL.checkCharaterByABC(ThanhTien) ? (Double.parseDouble(ThanhTien) >= 0 ? false : true) : true)) {
            new Diagram.Dialog().Showmess(null, "The money must be greater than 0 and don't write words", 0, null);
        } else {
            if (XLDL.setMaHDM(MaHDM)) {
                if (XLDL.setmaNS(MaNS)) {
                    if (XLDL.setMaNCC(MaNC)) {
                        InsHDM(MaHDM, MaNS, MaNC, NgayLap, ThanhTien);
                    } else {
                        new Diagram.Dialog().Showmess(null, "Code NhaCungCap isn't correct", 0, null);
                    }
                } else {
                    new Diagram.Dialog().Showmess(null, "Code MaNS isn't correct", 0, null);
                }
            } else {
                new Diagram.Dialog().Showmess(null, "Code HoaDonMua isn't correct", 0, null);
            }
        }
    }

    private void SelectHDM() {
        if (MaHDM.equals("")) {
            new Diagram.Dialog().Showmess(null, "Code HoaDonMua mustn't be empty", 0, null);
        } else if (!XLDL.setMaHDM(MaHDM)) {
            new Diagram.Dialog().Showmess(null, "Code HoaDonMua isn't correct", 0, null);
        } else {
            SelHDM(MaHDM);
        }
    }

    private void UpdateHDM() {
        if (MaHDM.equals("")) {
            new Diagram.Dialog().Showmess(null, "Code HoaDonMua mustn't be empty", 0, null);
        } else if (MaNS.equals("")) {
            new Diagram.Dialog().Showmess(null, "Code NhanSu mustn't be empty", 0, null);
        } else if (MaNC.equals("")) {
            new Diagram.Dialog().Showmess(null, "Code NhaCungCap mustn't be empty", 0, null);
        } else if (NgayLap.equals("")) {
            new Diagram.Dialog().Showmess(null, "Ngay Lap mustn't be empty", 0, null);
        } else if (ThanhTien.equals("")) {
            new Diagram.Dialog().Showmess(null, "The money mustn't be empty", 0, null);
        } else if ((ThanhTien.length() < 1 ? true : XLDL.checkCharaterByABC(ThanhTien) ? (Double.parseDouble(ThanhTien) >= 0 ? false : true) : true)) {
            new Diagram.Dialog().Showmess(null, "The money must be greater than 0 and don't write words", 0, null);
        } else {
            if (XLDL.setMaHDM(MaHDM)) {
                if (XLDL.setmaNS(MaNS)) {
                    if (XLDL.setMaNCC(MaNC)) {
                        UpHDM(MaNS, MaNC, NgayLap, ThanhTien, MaHDM);
                    } else {
                        new Diagram.Dialog().Showmess(null, "Code NhaCungCap isn't correct", 0, null);
                    }
                } else {
                    new Diagram.Dialog().Showmess(null, "Code MaNS isn't correct", 0, null);
                }
            } else {
                new Diagram.Dialog().Showmess(null, "Code HoaDonMua isn't correct", 0, null);
            }
        }
    }

    private void DeleteHDM() {
        if (MaHDM.equals("")) {
            new Diagram.Dialog().Showmess(null, "Code HoaDonMua mustn't be empty", 0, null);
        } else if (!XLDL.setMaHDM(MaHDM)) {
            new Diagram.Dialog().Showmess(null, "Code HoaDonMua isn't correct", 0, null);
        } else {
            DelHDM(MaHDM);
        }
    }
    public static final int INSERT = 1;

    public static final int SELECT = 2;

    public static final int UPDATE = 3;

    public static final int DELETE = 4;

    public void Action(int Action) {
        switch (Action) {
            case INSERT:
                InsertHDM();
                break;
            case UPDATE:
                UpdateHDM();
                break;
            case DELETE:
                DeleteHDM();
                break;
            case SELECT:
                SelectHDM();
                break;
        }
    }
}
