/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Table;

import Model.Dao.CTHDM_DAO;
import library.XLDL;

/**
 *
 * @author vokie
 */
public class CTHDM extends CTHDM_DAO {

    String MaHDM;
    String MaSP;
    String DonGia;
    int SoLuong;
    String ThanhTien;

    public String getMaHDM() {
        return MaHDM;
    }

    public void setMaHDM(String MaHDM) {
        this.MaHDM = MaHDM;
    }

    public String getMaSP() {
        return MaSP;
    }

    public void setMaSP(String MaSP) {
        this.MaSP = MaSP;
    }

    public String getDonGia() {
        return DonGia;
    }

    public void setDonGia(String DonGia) {
        this.DonGia = DonGia;
    }

    public int getSoLuong() {
        return SoLuong;
    }

    public void setSoLuong(int SoLuong) {
        this.SoLuong = SoLuong;
    }

    public String getThanhTien() {
        return ThanhTien;
    }

    public void setThanhTien(String ThanhTien) {
        this.ThanhTien = ThanhTien;
    }

    public CTHDM(String MaHDM, String MaSP, String DonGia, int SoLuong, String ThanhTien, int Action) {
        super();
        this.MaHDM = MaHDM;
        this.MaSP = MaSP;
        this.DonGia = DonGia;
        this.SoLuong = SoLuong;
        this.ThanhTien = ThanhTien;
        Action(Action);
    }

    public CTHDM(String DonGia, int SoLuong, String ThanhTien, String MaHDM, String MaSP, int Action) {
        super();
        this.MaHDM = MaHDM;
        this.MaSP = MaSP;
        this.DonGia = DonGia;
        this.SoLuong = SoLuong;
        this.ThanhTien = ThanhTien;
        Action(Action);
    }

    public CTHDM(String MaHDM, int Action) {
        super();
        this.MaHDM = MaHDM;
        Action(Action);
    }

    public CTHDM() {
    }

    @Override
    public void InsCTHDM(String MaHDM, String MaSP, String DonGia, int Soluong, String ThanhTien) {
        super.InsCTHDM(MaHDM, MaSP, DonGia, Soluong, ThanhTien); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void UpCTHDM(String DonGia, int Soluong, String ThanhTien, String MaHDM, String MaSP) {
        super.UpCTHDM(DonGia, Soluong, ThanhTien, MaHDM, MaSP); //To change body of generated methods, choose Tools | Templates.
    }

    
    @Override
    public void DelCTHDM(String MaHDM) {
        super.DelCTHDM(MaHDM); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void SelCTHDM(String MaHDM) {
        super.SelCTHDM(MaHDM); //To change body of generated methods, choose Tools | Templates.
    }

    private void InsertCTHDM() {
        if (MaHDM.equals("")) {
            Diagram.Dialog.Showmess(null, "Code HoaDonMua mustn't be empty", 0, null);
        } else if (MaSP.equals("")) {
            Diagram.Dialog.Showmess(null, "Code SanPham mustn't be empty", 0, null);
        } else if ((DonGia).equals("")) {
            Diagram.Dialog.Showmess(null, "DonGia mustn't be empty", 0, null);
        } else if ((DonGia.length() < 1 ? true : XLDL.checkCharaterByABC(DonGia) ? (Double.parseDouble(DonGia) >= 0 ? false : true) : true)) {
            Diagram.Dialog.Showmess(null, "The money must be greater than 0 and don't write words", 0, null);
        } else if (Integer.toString(SoLuong).equals("")) {
            Diagram.Dialog.Showmess(null, "SoLuong mustn't be empty", 0, null);
        } else if ((ThanhTien.length() < 1 ? true : XLDL.checkCharaterByABC(ThanhTien) ? (Double.parseDouble(ThanhTien) >= 0 ? false : true) : true)) {
            Diagram.Dialog.Showmess(null, "The total must be greater than 0 and don't write words", 0, null);
        } else {
            if (XLDL.setMaHDM(MaHDM)) {
                if (XLDL.setmaSP(MaSP)) {
                    if (SoLuong > 0) {

                        InsCTHDM(MaHDM, MaSP, DonGia, SoLuong, ThanhTien);

                    } else {
                        Diagram.Dialog.Showmess(null, "Quantity of goods must be greater than 0", 0, null);
                    }
                } else {
                    Diagram.Dialog.Showmess(null, "Code SanPham isn't correct", 0, null);
                }
            } else {
                Diagram.Dialog.Showmess(null, "Code HoaDonMua isn't correct", 0, null);
            }
        }
    }
    private void SelectCTHDM() {
        if (MaHDM.equals("")) {
            Diagram.Dialog.Showmess(null, "Code HoaDonMua mustn't be empty", 0, null);
        } else if (XLDL.setMaHDM(MaHDM)) {
            SelCTHDM(MaHDM);
        }
    }
 private void UpdateCTHDM() {
        if (MaHDM.equals("")) {
            Diagram.Dialog.Showmess(null, "Code HoaDonMua mustn't be empty", 0, null);
        } else if (MaSP.equals("")) {
            Diagram.Dialog.Showmess(null, "Code SanPham mustn't be empty", 0, null);
        } else if ((DonGia).equals("")) {
            Diagram.Dialog.Showmess(null, "DonGia mustn't be empty", 0, null);
        } else if ((DonGia.length() < 1 ? true : XLDL.checkCharaterByABC(DonGia) ? (Double.parseDouble(DonGia) >= 0 ? false : true) : true)) {
            Diagram.Dialog.Showmess(null, "The money must be greater than 0 and don't write words", 0, null);
        } else if (Integer.toString(SoLuong).equals("")) {
            Diagram.Dialog.Showmess(null, "SoLuong mustn't be empty", 0, null);
        } else if ((ThanhTien.length() < 1 ? true : XLDL.checkCharaterByABC(ThanhTien) ? (Double.parseDouble(ThanhTien) >= 0 ? false : true) : true)) {
            Diagram.Dialog.Showmess(null, "The total must be greater than 0 and don't write words", 0, null);
        } else {
            if (XLDL.setMaHDM(MaHDM)) {
                if (XLDL.setmaSP(MaSP)) {
                    if (SoLuong > 0) {

                        UpCTHDM(DonGia, SoLuong, ThanhTien, MaHDM, MaSP);

                    } else {
                        Diagram.Dialog.Showmess(null, "Quantity of goods must be greater than 0", 0, null);
                    }
                } else {
                    Diagram.Dialog.Showmess(null, "Code SanPham isn't correct", 0, null);
                }
            } else {
                Diagram.Dialog.Showmess(null, "Code HoaDonMua isn't correct", 0, null);
            }
        }
    }
  private void DeleteCTHDM() {
        if (MaHDM.equals("")) {
            Diagram.Dialog.Showmess(null, "Code HoaDonMua mustn't be empty", 0, null);
        } else if (XLDL.setMaHDM(MaHDM)) {
            DelCTHDM(MaHDM);
        }
    }
    public static final int INSERT = 1;

    public static final int SELECT = 2;

    public static final int UPDATE = 3;

    public static final int DELETE = 4;

    public void Action(int Action) {
        switch (Action) {
            case INSERT:
                InsertCTHDM();
                break;
            case UPDATE:
                UpdateCTHDM();
                break;
            case DELETE:
                DeleteCTHDM();
                break;
            case SELECT:
                SelectCTHDM();
                break;
        }
    }
}
