/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Table;

import Model.Dao.TTNS_DAO;
import javax.swing.JComboBox;
import javax.swing.JTable;
import library.XLDL;

/**
 *
 * @author vokie
 */
public class TTNS extends TTNS_DAO {

    String MaNS;
    String HoTen;
    String DiaChi;
    String CMND;
    String MaCV;
    String Avatar;

    public TTNS() {
    }

    public String getMaNS() {
        return MaNS;
    }

    public void setMaNS(String MaNS) {
        this.MaNS = MaNS;
    }

    public String getHoTen() {
        return HoTen;
    }

    public void setHoTen(String HoTen) {
        this.HoTen = HoTen;
    }

    public String getDiaChi() {
        return DiaChi;
    }

    public void setDiaChi(String DiaChi) {
        this.DiaChi = DiaChi;
    }

    public String getCMND() {
        return CMND;
    }

    public void setCMND(String CMND) {
        this.CMND = CMND;
    }

    public String getMaCV() {
        return MaCV;
    }

    public void setMaCV(String MaCV) {
        this.MaCV = MaCV;
    }

    public String getAvatar() {
        return Avatar;
    }

    public void setAvatar(String Avatar) {
        this.Avatar = Avatar;
    }

    public TTNS(String MaNS, String HoTen, String DiaChi, String CMND, String MaCV, String Avatar, int Action) {
        super();
        this.MaNS = MaNS;
        this.HoTen = HoTen;
        this.DiaChi = DiaChi;
        this.CMND = CMND;
        this.MaCV = MaCV;
        this.Avatar = Avatar;
        Action(Action);
    }

    public TTNS(String MaNS, String HoTen, String DiaChi, String CMND, String MaCV, String Avatar) {
        super();
        this.MaNS = MaNS;
        this.HoTen = HoTen;
        this.DiaChi = DiaChi;
        this.CMND = CMND;
        this.MaCV = MaCV;
        this.Avatar = Avatar;
    }

    public TTNS(String MaNS, int Action) {
        super();
        this.MaNS = MaNS;
        Action(Action);

    }

    @Override
    public void SelTTNS(String MaNS) {
        super.SelTTNS(MaNS); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void InsTTNS(String MaNS, String Hoten, String Diachi, String CMND, String MaCV, String avt) {
        super.InsTTNS(MaNS, Hoten, Diachi, CMND, MaCV, avt); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void UpTTNS(String Hoten, String Diachi, String CMND, String MaCV, String avt, String MaNS) {
        super.UpTTNS(Hoten, Diachi, CMND, MaCV, avt, MaNS); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void DelTTNS(String MaNS) {
        super.DelTTNS(MaNS); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void GetDataNS(JTable tbl, JComboBox cbb) {
        super.GetDataNS(tbl, cbb); //To change body of generated methods, choose Tools | Templates.
    }

    private void InsertTTNS() {
        if (MaNS.equals("")) {
            Diagram.Dialog.Showmess(null, "Code NhanSu mustn't be empty", 0, null);
        } else if (HoTen.equals("")) {
            Diagram.Dialog.Showmess(null, "HoTen mustn't be empty", 0, null);
        } else if (DiaChi.equals("")) {
            Diagram.Dialog.Showmess(null, "DiaChi mustn't be empty", 0, null);
        } else if ((CMND).equals("")) {
            Diagram.Dialog.Showmess(null, "DonGia mustn't be empty", 0, null);
        } else if ((CMND.length() < 1 ? true : XLDL.checkCharaterByABC(CMND) ? (Integer.parseInt(CMND) >= 0 ? false : true) : true)) {
            Diagram.Dialog.Showmess(null, "CMND must be greater than 0 and don't write words", 0, null);
        } else if (MaCV.equals("")) {
            Diagram.Dialog.Showmess(null, "Code ChucVu mustn't be empty", 0, null);
        } else if (Avatar.equals("")) {
            Diagram.Dialog.Showmess(null, "Avatar mustn't be empty", 0, null);
        } else {
            if (XLDL.setmaNS(MaNS)) {
                if (XLDL.setCMND(CMND)) {
                    if (XLDL.setChucVu(MaCV)) {

                        InsTTNS(MaNS, HoTen, DiaChi, CMND, MaCV, Avatar);

                    } else {
                        Diagram.Dialog.Showmess(null, "Code ChucVu isn't correct", 0, null);
                    }
                } else {
                    Diagram.Dialog.Showmess(null, "CMND isn't correct", 0, null);
                }
            } else {
                Diagram.Dialog.Showmess(null, "Code NhanSu isn't correct", 0, null);
            }
        }
    }

    private void SelectTTNS() {
        if (MaNS.equals("")) {
            Diagram.Dialog.Showmess(null, "Code NhanSu mustn't be empty", 0, null);
        } else if (XLDL.setmaNS(MaNS)) {
            SelTTNS(MaNS);
        } else {
            Diagram.Dialog.Showmess(null, "Code NhanSu isn't correct", 0, null);
        }
    }

    private void UpdateTTNS() {
        if (MaNS.equals("")) {
            Diagram.Dialog.Showmess(null, "Code NhanSu mustn't be empty", 0, null);
        } else if (HoTen.equals("")) {
            Diagram.Dialog.Showmess(null, "HoTen mustn't be empty", 0, null);
        } else if (DiaChi.equals("")) {
            Diagram.Dialog.Showmess(null, "DiaChi mustn't be empty", 0, null);
        } else if ((CMND).equals("")) {
            Diagram.Dialog.Showmess(null, "DonGia mustn't be empty", 0, null);
        } else if ((CMND.length() < 1 ? true : XLDL.checkCharaterByABC(CMND) ? (Integer.parseInt(CMND) >= 0 ? false : true) : true)) {
            Diagram.Dialog.Showmess(null, "CMND must be greater than 0 and don't write words", 0, null);
        } else if (MaCV.equals("")) {
            Diagram.Dialog.Showmess(null, "Code ChucVu mustn't be empty", 0, null);
        } else if (Avatar.equals("")) {
            Diagram.Dialog.Showmess(null, "Avatar mustn't be empty", 0, null);
        } else {
            if (XLDL.setmaNS(MaNS)) {
                if (XLDL.setCMND(CMND)) {
                    if (XLDL.setChucVu(MaCV)) {

                        UpTTNS(HoTen, DiaChi, CMND, MaCV, Avatar, MaNS);

                    } else {
                        Diagram.Dialog.Showmess(null, "Code ChucVu isn't correct", 0, null);
                    }
                } else {
                    Diagram.Dialog.Showmess(null, "CMND isn't correct", 0, null);
                }
            } else {
                Diagram.Dialog.Showmess(null, "Code NhanSu isn't correct", 0, null);
            }
        }
    }

    private void DeleteTTNS() {
        if (MaNS.equals("")) {
            Diagram.Dialog.Showmess(null, "Code NhanSu mustn't be empty", 0, null);
        } else if (XLDL.setmaNS(MaNS)) {
            DelTTNS(MaNS);
        } else {
            Diagram.Dialog.Showmess(null, "Code NhanSu isn't correct", 0, null);
        }
    }
    public static final int INSERT = 1;

    public static final int SELECT = 2;

    public static final int UPDATE = 3;

    public static final int DELETE = 4;

    public void Action(int Action) {
        switch (Action) {
            case INSERT:
                InsertTTNS();
                break;
            case UPDATE:
                UpdateTTNS();
                break;
            case DELETE:
                DeleteTTNS();
                break;
            case SELECT:
                SelectTTNS();
                break;
        }
    }
}
