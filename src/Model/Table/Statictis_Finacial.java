/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Table;

/**
 *
 * @author nhm95
 */
public class Statictis_Finacial {
  private String month,money;

    public Statictis_Finacial() {
    }

    public Statictis_Finacial(String month, String money) {
        this.month = month;
        this.money = money;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }
  
}
