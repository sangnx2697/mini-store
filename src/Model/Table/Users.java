/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Table;

import Model.Dao.Users_DAO;
import javax.swing.JComboBox;
import javax.swing.JTable;
import library.XLDL;

/**
 *
 * @author vokie
 */
public class Users extends Users_DAO {

    private String MaNS;
    private String UserName;
    private String Password;
    private String Role;
    private String Avatar;

    public Users(String MaNS, String UserName, String Password, String Role, String Avatar) {
        super();
        this.MaNS = MaNS;
        this.UserName = UserName;
        this.Password = Password;
        this.Role = Role;
        this.Avatar = Avatar;

    }

    public Users() {
    }

    public String getMaNS() {
        return MaNS;
    }

    public void setMaNS(String MaNS) {
        this.MaNS = MaNS;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String UserName) {
        this.UserName = UserName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public String getRole() {
        return Role;
    }

    public void setRole(String Role) {
        this.Role = Role;
    }

    public String getAvatar() {
        return Avatar;
    }

    public void setAvatar(String Avatar) {
        this.Avatar = Avatar;
    }

    public Users(String MaNS, String UserName, String Password, String Role, String Avatar, int Action) {
        super();
        this.MaNS = MaNS;
        this.UserName = UserName;
        this.Password = Password;
        this.Role = Role;
        this.Avatar = Avatar;
        Action(Action);
    }

    public Users(String MaNS, int Action) {
        super();
        this.MaNS = MaNS;
        Action(Action);
    }

    @Override
    public void InsUsers(String MaNS, String Username, String Pass, String Role, String avt) {
        super.InsUsers(MaNS, Username, Pass, Role, avt); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void DelUsers(String MaNS) {
        super.DelUsers(MaNS); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void SelUsers(String MaNS) {
        super.SelUsers(MaNS); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void UpUsers(String Username, String Pass, String Role, String avt, String MaNS) {
        super.UpUsers(Username, Pass, Role, avt, MaNS); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void GetDataUsers(JTable tbl, JComboBox cbb) {
        super.GetDataUsers(tbl, cbb); //To change body of generated methods, choose Tools | Templates.
    }

    

    
 
    private void InsertUsers() {
        if (MaNS.equals("")) {
            Diagram.Dialog.Showmess(null, "Code NhanSu mustn't be empty", 0, null);
        } else if (UserName.equals("")) {
            Diagram.Dialog.Showmess(null, "UserName mustn't be empty", 0, null);
        } else if (Password.equals("")) {
            Diagram.Dialog.Showmess(null, "Password mustn't be empty", 0, null);
        } else if (Role.equals("")) {
            Diagram.Dialog.Showmess(null, "Role mustn't be empty", 0, null);
        } else if (Avatar.equals("")) {
            Diagram.Dialog.Showmess(null, "Avatar ChucVu mustn't be empty", 0, null);
        } else {
            if (XLDL.setmaNS(MaNS)) {
                if (XLDL.setUser(UserName)) {
                    if (XLDL.setPass(Password)) {

                        InsUsers(MaNS, UserName, MaNS, Role, Avatar);

                    } else {
                        Diagram.Dialog.Showmess(null, "UserName can't write special characters", 0, null);
                    }
                } else {
                    Diagram.Dialog.Showmess(null, "Password can't write special characters ", 0, null);
                }
            } else {
                Diagram.Dialog.Showmess(null, "Code NhanSu isn't correct", 0, null);
            }
        }
    }

    private void SelectUsers() {
        if (MaNS.equals("")) {
            Diagram.Dialog.Showmess(null, "Code NhanSu mustn't be empty", 0, null);
        } else if (XLDL.setmaNS(MaNS)) {
            SelUsers(MaNS);
        }
    }

    private void UpdateUsers() {
        if (MaNS.equals("")) {
            Diagram.Dialog.Showmess(null, "Code NhanSu mustn't be empty", 0, null);
        } else if (UserName.equals("")) {
            Diagram.Dialog.Showmess(null, "UserName mustn't be empty", 0, null);
        } else if (Password.equals("")) {
            Diagram.Dialog.Showmess(null, "Password mustn't be empty", 0, null);
        } else if (Role.equals("")) {
            Diagram.Dialog.Showmess(null, "Role mustn't be empty", 0, null);
        } else if (Avatar.equals("")) {
            Diagram.Dialog.Showmess(null, "Avatar ChucVu mustn't be empty", 0, null);
        } else {
            if (XLDL.setmaNS(MaNS)) {
                if (XLDL.setUser(UserName)) {
                    if (XLDL.setPass(Password)) {

                        UpUsers(MaNS, UserName, MaNS, Role, Avatar);

                    } else {
                        Diagram.Dialog.Showmess(null, "UserName can't write special characters", 0, null);
                    }
                } else {
                    Diagram.Dialog.Showmess(null, "Password can't write special characters ", 0, null);
                }
            } else {
                Diagram.Dialog.Showmess(null, "Code NhanSu isn't correct", 0, null);
            }
        }
    }

    private void DeleteUsers() {
        if (MaNS.equals("")) {
            Diagram.Dialog.Showmess(null, "Code NhanSu mustn't be empty", 0, null);
        } else if (XLDL.setmaNS(MaNS)) {
            DelUsers(MaNS);
        }
    }
    public static final int INSERT = 1;

    public static final int SELECT = 2;

    public static final int UPDATE = 3;

    public static final int DELETE = 4;

    public void Action(int Action) {
        switch (Action) {
            case INSERT:
                InsertUsers();
                break;
            case UPDATE:
                UpdateUsers();
                break;
            case DELETE:
                DeleteUsers();
                break;
            case SELECT:
                SelectUsers();
                break;
        }
    }
}
