/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Table;

/**
 *
 * @author nhm95
 */
public class Statictis_InvoiceOut {

    private String InvoiceID, money, month;

    public Statictis_InvoiceOut(String InvoiceID, String money) {
        this.InvoiceID = InvoiceID;
        this.money = money;
    }

    public Statictis_InvoiceOut(double money, String month) {
        this.money = money+"";
        this.month = month;
    }

    public Statictis_InvoiceOut() {
    }

    public String getInvoiceID() {
        return InvoiceID;
    }

    public void setInvoiceID(String InvoiceID) {
        this.InvoiceID = InvoiceID;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

}
