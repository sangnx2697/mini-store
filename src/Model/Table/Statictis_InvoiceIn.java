/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Table;

/**
 *
 * @author nhm95
 */
public class Statictis_InvoiceIn {

    private String product, month, money;

    public Statictis_InvoiceIn() {
    }

    public Statictis_InvoiceIn(String product, String month, String money) {
        this.product = product;
        this.month = month;
        this.money = money;
    }

    public Statictis_InvoiceIn(String product, String money) {
        this.product = product;
        this.money = money;
    }

    public Statictis_InvoiceIn(String month, double money) {
        this.month = month;
        this.money = money+"";
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

}
