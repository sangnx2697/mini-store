/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Table;

import Model.Dao.NhaCungCap_DAO;
import javax.swing.JTable;
import library.XLDL;

/**
 *
 * @author vokie
 */
public class NhaCungCap extends NhaCungCap_DAO {

    public String MaNCC;
    public String TenNCC;
    public String DiaChiNCC;

    public String getMaNCC() {
        return MaNCC;
    }

    public void setMaNCC(String MaNCC) {
        this.MaNCC = MaNCC;
    }

    public String getTenNCC() {
        return TenNCC;
    }

    public void setTenNCC(String TenNCC) {
        this.TenNCC = TenNCC;
    }

    public String getDiaChiNCC() {
        return DiaChiNCC;
    }

    public void setDiaChiNCC(String DiaChiNCC) {
        this.DiaChiNCC = DiaChiNCC;
    }

    public NhaCungCap(String MaNCC, String TenNCC, String DiaChiNCC, int Action) {
        this.MaNCC = MaNCC;
        this.TenNCC = TenNCC;
        this.DiaChiNCC = DiaChiNCC;
        Action(Action);
    }

    public NhaCungCap(String MaNCC, int Action) {
        this.MaNCC = MaNCC;
        Action(Action);
    }

    public NhaCungCap() {
    }

    @Override
    public void InsNCC(String MaNCC, String TenNCC, String DiaChi) {
        super.InsNCC(MaNCC, TenNCC, DiaChi); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void SelNCC(String MaNCC) {
        super.SelNCC(MaNCC); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void UpNCC(String TenNCC, String Diachi, String MaNCC) {
        super.UpNCC(TenNCC, Diachi, MaNCC); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void DelNCC(String MaNCC) {
        super.DelNCC(MaNCC); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void GetDataNCC(JTable tbl) {
        super.GetDataNCC(tbl); //To change body of generated methods, choose Tools | Templates.
    }

    
    
    private void InsertNCC() {
        if (MaNCC.equals("")) {
            Diagram.Dialog.Showmess(null, "Code NhaCungCap mustn't be empty", 0, null);
        } else if (TenNCC.equals("")) {
            Diagram.Dialog.Showmess(null, "NhaCungCap's Name mustn't be empty", 0, null);
        }
        if (XLDL.setMaNCC(MaNCC)) {
            InsNCC(MaNCC, TenNCC, DiaChiNCC);
        } else {
            Diagram.Dialog.Showmess(null, "Code NhaCungCap isn't correct", 0, null);
        }
    }

    private void SelectNCC() {
        if (MaNCC.equals("")) {
            Diagram.Dialog.Showmess(null, "Code NhaCungCap mustn't be empty", 0, null);
        } else if (XLDL.setMaNCC(MaNCC)) {
            SelNCC(MaNCC);
        } else {
            Diagram.Dialog.Showmess(null, "Code NhaCungCap isn't correct", 0, null);
        }
    }

    private void UpdateNCC() {
        if (MaNCC.equals("")) {
            Diagram.Dialog.Showmess(null, "Code NhaCungCap mustn't be empty", 0, null);
        } else if (TenNCC.equals("")) {
            Diagram.Dialog.Showmess(null, "NhaCungCap's Name mustn't be empty", 0, null);
        }
        if (XLDL.setMaNCC(MaNCC)) {
            UpNCC(TenNCC, DiaChiNCC, MaNCC);
        } else {
            Diagram.Dialog.Showmess(null, "Code NhaCungCap isn't correct", 0, null);
        }
    }

    private void DeleteNCC() {
        if (MaNCC.equals("")) {
            Diagram.Dialog.Showmess(null, "Code NhaCungCap mustn't be empty", 0, null);
        } else if (XLDL.setMaNCC(MaNCC)) {
            DelNCC(MaNCC);
        } else {
            Diagram.Dialog.Showmess(null, "Code NhaCungCap isn't correct", 0, null);
        }
    }
    public static final int INSERT = 1;

    public static final int SELECT = 2;

    public static final int UPDATE = 3;

    public static final int DELETE = 4;

    public void Action(int Action) {
        switch (Action) {
            case INSERT:
                InsertNCC();
                break;
            case UPDATE:
                UpdateNCC();
                break;
            case DELETE:
                DeleteNCC();
                break;
            case SELECT:
                SelectNCC();
                break;
        }
    }
}
