/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Table;

import Model.Dao.SanPham_DAO;
import javax.swing.JComboBox;
import javax.swing.JTable;
import library.XLDL;

/**
 *
 * @author vokie
 */
public class SanPham extends SanPham_DAO{

    String MaSP;
    String TenSP;
    String MaNC;
    String DonViTinh;
    String DonGia;
    int SoLuong;

    public SanPham() {
    }

    public String getMaSP() {
        return MaSP;
    }

    public void setMaSP(String MaSP) {
        this.MaSP = MaSP;
    }

    public String getTenSP() {
        return TenSP;
    }

    public void setTenSP(String TenSP) {
        this.TenSP = TenSP;
    }

    public String getMaNC() {
        return MaNC;
    }

    public void setMaNC(String MaNC) {
        this.MaNC = MaNC;
    }

    public String getDonViTinh() {
        return DonViTinh;
    }

    public void setDonViTinh(String DonViTinh) {
        this.DonViTinh = DonViTinh;
    }

    public String getDonGia() {
        return DonGia;
    }

    public void setDonGia(String DonGia) {
        this.DonGia = DonGia;
    }

    public int getSoLuong() {
        return SoLuong;
    }

    public void setSoLuong(int SoLuong) {
        this.SoLuong = SoLuong;
    }

    public SanPham(String MaSP, String TenSP, String MaNC, String DonViTinh, String DonGia, int SoLuong, int Action) {
        this.MaSP = MaSP;
        this.TenSP = TenSP;
        this.MaNC = MaNC;
        this.DonViTinh = DonViTinh;
        this.DonGia = DonGia;
        this.SoLuong = SoLuong;
        Action(Action);
    }

    public SanPham(String MaSP, int Action) {
        super();
        this.MaSP = MaSP;
        Action(Action);
    }

    @Override
    public void GetDataSP(JTable tbl, JComboBox cbb) {
        super.GetDataSP(tbl, cbb); //To change body of generated methods, choose Tools | Templates.
    }
    

    @Override
    public void InsSanPham(String MaSP, String TenSP, String MaNC, String DonViTinh, String DonGia, int SoLuong) {
        super.InsSanPham(MaSP, TenSP, MaNC, DonViTinh, DonGia, SoLuong); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void SelSanPham(String MaSP) {
        super.SelSanPham(MaSP); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void DelSanPham(String MaSP) {
        super.DelSanPham(MaSP); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void UpSanPham(String TenSP, String MaNC, String DonViTinh, String DonGia, int SoLuong, String MaSP) {
        super.UpSanPham(TenSP, MaNC, DonViTinh, DonGia, SoLuong, MaSP); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void GetDataSP(JTable tbl) {
        super.GetDataSP(tbl); //To change body of generated methods, choose Tools | Templates.
    }

    private void InsertSanPham() {
        if (MaSP.equals("")) {
            Diagram.Dialog.Showmess(null, "Code SanPham mustn't be empty", 0, null);
        } else if (TenSP.equals("")) {
            Diagram.Dialog.Showmess(null, "TenSanPham mustn't be empty", 0, null);
        } else if (MaNC.equals("")) {
            Diagram.Dialog.Showmess(null, "Code MaNhaCC mustn't be empty", 0, null);
        } else if (DonViTinh.equals("")) {
            Diagram.Dialog.Showmess(null, "DonViTinh mustn't be empty", 0, null);
        } else if ((DonGia).equals("")) {
            Diagram.Dialog.Showmess(null, "DonGia mustn't be empty", 0, null);
        } else if ((DonGia.length() < 1 ? true : XLDL.checkCharaterByABC(DonGia) ? (Double.parseDouble(DonGia) >= 0 ? false : true) : true)) {
            Diagram.Dialog.Showmess(null, "The money must be greater than 0 and don't write words", 0, null);
        } else if (Integer.toString(SoLuong).equals("")) {
            Diagram.Dialog.Showmess(null, "SoLuong mustn't be empty", 0, null);
        }  else {
            if (XLDL.setmaSP(MaSP)) {
                if (XLDL.setMaNCC(MaNC)) {
                    if (SoLuong > 0) {

                        InsSanPham(MaSP, TenSP, MaNC, DonViTinh, DonGia, SoLuong);

                    } else {
                        Diagram.Dialog.Showmess(null, "Quantity of goods must be greater than 0", 0, null);
                    }
                } else {
                    Diagram.Dialog.Showmess(null, "Code MaNhaCC isn't correct", 0, null);
                }
            } else {
                Diagram.Dialog.Showmess(null, "Code SanPham isn't correct", 0, null);
            }
        }
    }
    private void SelectSanPham() {
        if (MaSP.equals("")) {
            Diagram.Dialog.Showmess(null, "Code SanPham mustn't be empty", 0, null);
        } else if (XLDL.setmaSP(MaSP)) {
            SelSanPham(MaSP);
        }
    }
    
    private void UpdateSanPham() {
        if (MaSP.equals("")) {
            Diagram.Dialog.Showmess(null, "Code SanPham mustn't be empty", 0, null);
        } else if (TenSP.equals("")) {
            Diagram.Dialog.Showmess(null, "TenSanPham mustn't be empty", 0, null);
        } else if (MaNC.equals("")) {
            Diagram.Dialog.Showmess(null, "Code MaNhaCC mustn't be empty", 0, null);
        } else if (DonViTinh.equals("")) {
            Diagram.Dialog.Showmess(null, "DonViTinh mustn't be empty", 0, null);
        } else if ((DonGia).equals("")) {
            Diagram.Dialog.Showmess(null, "DonGia mustn't be empty", 0, null);
        } else if ((DonGia.length() < 1 ? true : XLDL.checkCharaterByABC(DonGia) ? (Double.parseDouble(DonGia) >= 0 ? false : true) : true)) {
            Diagram.Dialog.Showmess(null, "The money must be greater than 0 and don't write words", 0, null);
        } else if (Integer.toString(SoLuong).equals("")) {
            Diagram.Dialog.Showmess(null, "SoLuong mustn't be empty", 0, null);
        }  else {
            if (XLDL.setmaSP(MaSP)) {
                if (XLDL.setMaNCC(MaNC)) {
                    if (SoLuong > 0) {

                        UpSanPham(TenSP, MaNC, DonViTinh, DonGia, SoLuong, MaSP);

                    } else {
                        Diagram.Dialog.Showmess(null, "Quantity of goods must be greater than 0", 0, null);
                    }
                } else {
                    Diagram.Dialog.Showmess(null, "Code MaNhaCC isn't correct", 0, null);
                }
            } else {
                Diagram.Dialog.Showmess(null, "Code SanPham isn't correct", 0, null);
            }
        }
    }
    private void DeleteSanPham() {
        if (MaSP.equals("")) {
            Diagram.Dialog.Showmess(null, "Code SanPham mustn't be empty", 0, null);
        } else if (XLDL.setmaSP(MaSP)) {
            DelSanPham(MaSP);
        }
    }
    public static final int INSERT = 1;

    public static final int SELECT = 2;

    public static final int UPDATE = 3;

    public static final int DELETE = 4;

    public void Action(int Action) {
        switch (Action) {
            case INSERT:
                InsertSanPham();
                break;
            case UPDATE:
                UpdateSanPham();
                break;
            case DELETE:
                DeleteSanPham();
                break;
            case SELECT:
                SelectSanPham();
                break;
        }
    }
}
