/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Table;

import Model.Dao.LuongNV_DAO;
import javax.swing.JTable;
import library.XLDL;

/**
 *
 * @author vokie
 */
public class LuongNV extends LuongNV_DAO {

    String MaCV;
    String ChucVu;
    String MucLuong;

    public String getMaCV() {
        return MaCV;
    }

    public LuongNV() {
    }

    public void setMaCV(String MaCV) {
        this.MaCV = MaCV;
    }

    public String getChucVu() {
        return ChucVu;
    }

    public void setChucVu(String ChucVu) {
        this.ChucVu = ChucVu;
    }

    public String getMucLuong() {
        return MucLuong;
    }

    public void setMucLuong(String MucLuong) {
        this.MucLuong = MucLuong;
    }

    public LuongNV(String MaCV, String ChucVu, String MucLuong,int Action) {
        super();
        this.MaCV = MaCV;
        this.ChucVu = ChucVu;
        this.MucLuong = MucLuong;
        Action(Action);
    }

    public LuongNV(String MaCV, String ChucVu,int Action) {
        super();
        this.MaCV = MaCV;
        this.ChucVu = ChucVu;
        Action(Action);
    }

    public LuongNV(String MaCV, int MucLuong,int Action) {
        super();
        this.MaCV = MaCV;
        this.ChucVu = ChucVu;
        this.MucLuong = MucLuong + "";
        Action(Action);
    }

    public LuongNV(String MaCV, String ChucVu, int MucLuong,int Action) {
        super();
        this.MaCV = MaCV;
        this.ChucVu = ChucVu;
        this.MucLuong = MucLuong + "";
        Action(Action);
    }

    public LuongNV(String MaCV, int Action) {
        super();
        this.MaCV = MaCV;
        Action(Action);
    }

    @Override
    public void DelLuongNV(String MaCV) {
        super.DelLuongNV(MaCV);
    }

    @Override
    public void InsLuongNV(String MaCV, String ChucVu, String MucLuong) {
        super.InsLuongNV(MaCV, ChucVu, MucLuong);
    }

    @Override
    public void SelLuongNV(String MaCV) {
        super.SelLuongNV(MaCV);
    }

    @Override
    public void UpLuongNV(String ChucVu, String MucLuong, String MaCV) {
        super.UpLuongNV(ChucVu, MucLuong, MaCV);
    }
    @Override
    public void GetDataLuongNV(JTable tbl) {
        super.GetDataLuongNV(tbl); 
    }
    private void InsertLuongNV() {
        if (MaCV.equals("")) {
            Diagram.Dialog.Showmess(null, "Code ChucVu mustn't be empty", 0, null);
        } else if (ChucVu.equals("")) {
            Diagram.Dialog.Showmess(null, "The position mustn't be empty", 0, null);
        } else if ((MucLuong.length() < 1 ? true : XLDL.checkCharaterByABC(MucLuong) ? (Double.parseDouble(MucLuong) >= 0 ? false : true) : true)) {
            Diagram.Dialog.Showmess(null, "Salary must be greater than 0 VND", 0, null);
        } else {
            if (XLDL.setChucVu(MaCV)) {
            if (XLDL.setluong((MucLuong))) {
                InsLuongNV(MaCV, ChucVu, MucLuong);
            } else {
                Diagram.Dialog.Showmess(null, "Salary must be less than 50.000.000 VND", 0, null);
            }
            }else{
                Diagram.Dialog.Showmess(null, "Code ChucVu isn't correct", 0, null);
            }
        }
    }

    private void SelectLuongNV() {
        if (MaCV.equals("")) {
            Diagram.Dialog.Showmess(null, "Code ChucVu mustn't be empty", 0, null);
        } else {
            SelLuongNV(MaCV);
        }
    }

    private void UpdateLuongNV() {
        if (MaCV.equals("")) {
            Diagram.Dialog.Showmess(null, "Code ChucVu mustn't be empty", 0, null);
        } else if (ChucVu.equals("")) {
            Diagram.Dialog.Showmess(null, "The position mustn't be empty", 0, null);
        } else if ((MucLuong.length() < 1 ? true : XLDL.checkCharaterByABC(MucLuong) ? (Double.parseDouble(MucLuong) >= 0 ? false : true) : true)) {
            Diagram.Dialog.Showmess(null, "Salary must be greater than 0 VND and don't write words", 0, null);
        } else {
            if (XLDL.setChucVu(MaCV)) {
            if (XLDL.setluong((MucLuong))) {
                UpLuongNV(MaCV, ChucVu, MucLuong);
            } else {
                Diagram.Dialog.Showmess(null, "Salary must be less than 50.000.000 VND", 0, null);
            }
            }else{
                Diagram.Dialog.Showmess(null, "Code ChucVu isn't correct", 0, null);
            }
        }
    }

    private void DeleteLuongNV() {
        if (MaCV.equals("")) {
            Diagram.Dialog.Showmess(null, "Code ChucVu mustn't be empty", 0, null);
        } else {
            DelLuongNV(MaCV);
        }
    }
    public static final int INSERT = 1;

    public static final int SELECT = 2;

    public static final int UPDATE = 3;

    public static final int DELETE = 4;

    public void Action(int Action) {
        switch (Action) {
            case INSERT:
                InsertLuongNV();
                break;
            case UPDATE:
                UpdateLuongNV();
                break;
            case DELETE:
                DeleteLuongNV();
                break;
            case SELECT:
                SelectLuongNV();
                break;
        }
    }
}
