/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Table;

import Model.Dao.CTHDB_DAO;
import library.XLDL;

/**
 *
 * @author vokie
 */
public class CTHDB extends CTHDB_DAO {

    String MaHDB;
    String MaSP;
    String DonGia;
    int SoLuong;
    String ThanhTien;

    public String getMaHDB() {
        return MaHDB;
    }

    public CTHDB() {
    }

    public void setMaHDB(String MaHDB) {
        this.MaHDB = MaHDB;
    }

    public String getMaSP() {
        return MaSP;
    }

    public void setMaSP(String MaSP) {
        this.MaSP = MaSP;
    }

    public String getDonGia() {
        return DonGia;
    }

    public void setDonGia(String DonGia) {
        this.DonGia = DonGia;
    }

    public int getSoLuong() {
        return SoLuong;
    }

    public void setSoLuong(int SoLuong) {
        this.SoLuong = SoLuong;
    }

    public String getThanhTien() {
        return ThanhTien;
    }

    public void setThanhTien(String ThanhTien) {
        this.ThanhTien = ThanhTien;
    }

    public CTHDB(String MaHDB, String MaSP, String DonGia, int SoLuong, String ThanhTien, int Action) {
        super();
        this.MaHDB = MaHDB;
        this.MaSP = MaSP;
        this.DonGia = DonGia;
        this.SoLuong = SoLuong;
        this.ThanhTien = ThanhTien;
        Action(Action);
    }

    public CTHDB(String DonGia, int SoLuong, String ThanhTien, String MaHDB, String MaSP, int Action) {
        super();
        this.MaHDB = MaHDB;
        this.MaSP = MaSP;
        this.DonGia = DonGia;
        this.SoLuong = SoLuong;
        this.ThanhTien = ThanhTien;
        Action(Action);
    }

    public CTHDB(String MaHDB, int Action) {
        super();
        this.MaHDB = MaHDB;
        Action(Action);
    }

    @Override
    public void InsCTHDB(String MaHDB, String MaSP, String DonGia, int Soluong, String ThanhTien) {
        super.InsCTHDB(MaHDB, MaSP, DonGia, Soluong, ThanhTien); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void SelCTHDB(String MaHDB) {
        super.SelCTHDB(MaHDB); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void UpCTHDB(String DonGia, int Soluong, String ThanhTien, String MaHDB, String MaSP) {
        super.UpCTHDB(DonGia, Soluong, ThanhTien, MaHDB, MaSP); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void DelCTHDB(String MaHDB) {
        super.DelCTHDB(MaHDB); //To change body of generated methods, choose Tools | Templates.
    }

    private void InsertCTHDB() {
        if (MaHDB.equals("")) {
            Diagram.Dialog.Showmess(null, "Code HoaDonBan mustn't be empty", 0, null);
        } else if (MaSP.equals("")) {
            Diagram.Dialog.Showmess(null, "Code SanPham mustn't be empty", 0, null);
        } else if ((DonGia).equals("")) {
            Diagram.Dialog.Showmess(null, "DonGia mustn't be empty", 0, null);
        } else if ((DonGia.length() < 1 ? true : XLDL.checkCharaterByABC(DonGia) ? (Double.parseDouble(DonGia) >= 0 ? false : true) : true)) {
            Diagram.Dialog.Showmess(null, "The money must be greater than 0 and don't write words", 0, null);
        } else if (Integer.toString(SoLuong).equals("")) {
            Diagram.Dialog.Showmess(null, "SoLuong mustn't be empty", 0, null);
        } else if ((ThanhTien.length() < 1 ? true : XLDL.checkCharaterByABC(ThanhTien) ? (Double.parseDouble(ThanhTien) >= 0 ? false : true) : true)) {
            Diagram.Dialog.Showmess(null, "The total must be greater than 0 and don't write words", 0, null);
        } else {
            if (XLDL.setMaHDB(MaHDB)) {
                if (XLDL.setmaSP(MaSP)) {
                    if (SoLuong > 0) {

                        InsCTHDB(MaHDB, MaSP, DonGia, SoLuong, ThanhTien);

                    } else {
                        Diagram.Dialog.Showmess(null, "Quantity of goods must be greater than 0", 0, null);
                    }
                } else {
                    Diagram.Dialog.Showmess(null, "Code SanPham isn't correct", 0, null);
                }
            } else {
                Diagram.Dialog.Showmess(null, "Code HoaDonBan isn't correct", 0, null);
            }
        }
    }

    private void SelectCTHDB() {
        if (MaHDB.equals("")) {
            Diagram.Dialog.Showmess(null, "Code HoaDonBan mustn't be empty", 0, null);
        } else if (XLDL.setMaHDB(MaHDB)) {
            SelCTHDB(MaHDB);
        }
    }

    private void UpdateCTHDB() {
        if (MaHDB.equals("")) {
            Diagram.Dialog.Showmess(null, "Code HoaDonBan mustn't be empty", 0, null);

        } else if (MaSP.equals("")) {
            Diagram.Dialog.Showmess(null, "Code SanPham mustn't be empty", 0, null);
        } else if ((DonGia).equals("")) {
            Diagram.Dialog.Showmess(null, "DonGia mustn't be empty", 0, null);
        } else if ((DonGia.length() < 1 ? true : XLDL.checkCharaterByABC(DonGia) ? (Double.parseDouble(DonGia) >= 0 ? false : true) : true)) {
            Diagram.Dialog.Showmess(null, "The money must be greater than 0 and don't write words", 0, null);
        } else if (Integer.toString(SoLuong).equals("")) {
            Diagram.Dialog.Showmess(null, "SoLuong mustn't be empty", 0, null);
        } else if ((ThanhTien.length() < 1 ? true : XLDL.checkCharaterByABC(ThanhTien) ? (Double.parseDouble(ThanhTien) >= 0 ? false : true) : true)) {
            Diagram.Dialog.Showmess(null, "The total must be greater than 0 and don't write words", 0, null);
        } else {
            if (XLDL.setMaHDB(MaHDB)) {
                if (XLDL.setmaSP(MaSP)) {
                    if (SoLuong > 0) {

                        UpCTHDB(DonGia, SoLuong, ThanhTien, MaHDB, MaSP);

                    } else {
                        Diagram.Dialog.Showmess(null, "Quantity of goods must be greater than 0", 0, null);
                    }
                } else {
                    Diagram.Dialog.Showmess(null, "Code SanPham isn't correct", 0, null);
                }
            } else {
                Diagram.Dialog.Showmess(null, "Code HoaDonBan isn't correct", 0, null);
            }
        }
    }

    private void DeleteCTHDB() {
        if (MaHDB.equals("")) {
            Diagram.Dialog.Showmess(null, "Code HoaDonBan mustn't be empty", 0, null);
        } else if (XLDL.setMaHDB(MaHDB)) {
            DelCTHDB(MaHDB);
        }
    }
    public static final int INSERT = 1;

    public static final int SELECT = 2;

    public static final int UPDATE = 3;

    public static final int DELETE = 4;

    public void Action(int Action) {
        switch (Action) {
            case INSERT:
                InsertCTHDB();
                break;
            case UPDATE:
                UpdateCTHDB();
                break;
            case DELETE:
                DeleteCTHDB();
                break;
            case SELECT:
                SelectCTHDB();
                break;
        }
    }
}



