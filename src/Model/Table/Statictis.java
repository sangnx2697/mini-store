/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Table;

/**
 *
 * @author nhm95
 */
public class Statictis {

    private Statictis_Finacial FN;
    private Statictis_InvoiceIn II;
    private Statictis_InvoiceOut IO;

    public Statictis(Statictis_Finacial FN, Statictis_InvoiceIn II, Statictis_InvoiceOut IO) {
        this.FN = FN;
        this.II = II;
        this.IO = IO;
    }

    public Statictis(Statictis_InvoiceOut IO) {
        this.IO = IO;
    }

    public Statictis(Statictis_InvoiceIn II) {
        this.II = II;
    }

    public Statictis(Statictis_Finacial FN) {
        this.FN = FN;
    }

    public Statictis() {
    }

    public Statictis_Finacial getFN() {
        return FN;
    }

    public void setFN(Statictis_Finacial FN) {
        this.FN = FN;
    }

    public Statictis_InvoiceIn getII() {
        return II;
    }

    public void setII(Statictis_InvoiceIn II) {
        this.II = II;
    }

    public Statictis_InvoiceOut getIO() {
        return IO;
    }

    public void setIO(Statictis_InvoiceOut IO) {
        this.IO = IO;
    }

}
