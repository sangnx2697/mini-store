/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Helper;

import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author nhm95
 */
public class Navigation {

   /* public static void next(JButton bt, JTable tb, int Index) {
        if (Index <= tb.getRowCount()) {

        }
    }

    public static void pre(JButton bt, JTable tb, int Index) {
        tb.getSelectionModel().setSelectionInterval(Index, Index);
    }

    public static void top(JTable tb, int Index) {
        tb.getSelectionModel().setSelectionInterval(Index, Index);
    }

    public static void bottom(JTable tb, int Index) {
        tb.getSelectionModel().setSelectionInterval(Index, Index);
    }*/
                        /* Hàm lấy vị trí index trong Table để điểu hướng*/
    public static void Navigator(JTable tb, JButton bt, int index) {

        switch (bt.getText()) {
            case "Next":
                if (index < tb.getRowCount()) {
                    index++;
                    if (index>=tb.getRowCount()) {
                        index=tb.getRowCount()-1;
                    }
                tb.getSelectionModel().setSelectionInterval(index, index);
                
                tb.scrollRectToVisible(new Rectangle(tb.getCellRect(index, 0, true)));
                }
                break;
            case "Preview":
                if (index >= 0) {
                    index--;
                    if (index<0) {
                        index=0;
                    }
                    tb.getSelectionModel().setSelectionInterval(index, index);
                    tb.scrollRectToVisible(new Rectangle(tb.getCellRect(index, 0, true)));
                }
                break;
            case "Top":
                index = 0;
                tb.getSelectionModel().setSelectionInterval(index, index);
                tb.scrollRectToVisible(new Rectangle(tb.getCellRect(index, 0, true)));
                break;
            case "Bottom":
                index = tb.getRowCount() - 1;
                tb.getSelectionModel().setSelectionInterval(index, index);
                tb.scrollRectToVisible(new Rectangle(tb.getCellRect(index, 0, true)));
                break;

        }
    }
   
                                    /* Hàm gọi Form Invoices Out */
    public static void showInfor_InvoicesOut(JTable tbl, JTextField tf1, JTextField tf2, JTextField tf3, JTextField tf4, JTextField tf5, JTextField tf6, JTextField tf7, int index ){
        if (index<0) {
            return;
        }
        tf1.setText(tbl.getValueAt(index, 0).toString());
        tf2.setText(tbl.getValueAt(index, 1).toString());
        tf3.setText(tbl.getValueAt(index, 2).toString());
        tf4.setText(tbl.getValueAt(index, 3).toString());
        tf5.setText(tbl.getValueAt(index, 4).toString());
        tf6.setText(tbl.getValueAt(index, 5).toString());
        tf7.setText(tbl.getValueAt(index, 6).toString());
        
    }
    
                                        /* Hàm gọi Form UseLogin */
    public static void showInfor_UserLogin(JTable tbl, JTextField tf1, JTextField tf2, JTextField tf3, JTextField tf4, JComboBox cbx, JLabel lb1, int index ){
        if (index<0) {
            return;
        }
        tf1.setText(tbl.getValueAt(index, 0).toString());
        tf2.setText(tbl.getValueAt(index, 1).toString());
        tf3.setText(tbl.getValueAt(index, 2).toString());
        tf4.setText(tbl.getValueAt(index, 2).toString());
        cbx.setSelectedItem(tbl.getValueAt(index, 3));
        lb1.setText(tbl.getValueAt(index, 4).toString());
        
    }
        
                                        /* Hàm gọi Form Persional */
    public static void showInfor_persional(JTable tbl, JTextField tf1, JTextField tf2, JTextField tf3, JTextArea ta, JComboBox cbx, JLabel lb, int index){
        if (index <0) {
            return; 
        }
        tf1.setText(tbl.getValueAt(index, 0).toString());
        tf2.setText(tbl.getValueAt(index, 1).toString());
        tf3.setText(tbl.getValueAt(index, 3).toString());
        ta.setText(tbl.getValueAt(index, 2).toString());
        cbx.setSelectedItem(tbl.getValueAt(index, 4));
        lb.setText(tbl.getValueAt(index, 5).toString());  
    }
    
                                        /* Hàm gọi Form Vendors */
    public static void showInfor_vendors(JTable tbl, JTextField tf1, JTextField tf2, JTextArea ta, int index){
        if (index<0) {
            return ;
        }
            tf1.setText(tbl.getValueAt(index, 0).toString());
            tf2.setText(tbl.getValueAt(index, 1).toString());
            ta.setText(tbl.getValueAt(index, 2).toString());            
    }
    
    
                                        /* Hàm gọi Form Products */
    public static void showInfor_products(JTable tbl, JTextField tf1, JTextField tf2, JTextField tf3, JTextField tf4, JTextField tf5, JComboBox cb1, int index){
        if (index<0) {
            return;
        }       
        tf1.setText(tbl.getValueAt(index, 0).toString());
        tf2.setText(tbl.getValueAt(index, 2).toString());
        tf3.setText(tbl.getValueAt(index, 1).toString());
        tf4.setText(tbl.getValueAt(index, 4).toString());
        tf5.setText(tbl.getValueAt(index, 5).toString());
        cb1.setSelectedItem(tbl.getValueAt(index, 3));
    }
    
    
                                        /* Hàm gọi Form Invoices In */
    public static void showInfor_InvoicesIn(JTable tbl, JTextField tf1, JTextField tf2, JTextField tf3, JTextField tf4, JTextField tf5, JTextField tf6, JTextField tf7, JTextField tf8, int index){
        if (index<0) {
            return;
        }      
        tf1.setText(tbl.getValueAt(index,0).toString());
        tf2.setText(tbl.getValueAt(index,1).toString());
        tf3.setText(tbl.getValueAt(index,6).toString());
        tf4.setText(tbl.getValueAt(index,5).toString());
        tf5.setText(tbl.getValueAt(index,7).toString());
        tf6.setText(tbl.getValueAt(index,3).toString());
        tf7.setText(tbl.getValueAt(index,2).toString());
        tf8.setText(tbl.getValueAt(index,4).toString());

        
    }
                                        /* Hàm gọi Form Financial */
    public static void showInfor_Financial(JTable tbl, JTextField tf1, JTextField tf2, JTextField tf3,int index){
        if (index<0) {
            return;
        }      
        tf1.setText(tbl.getValueAt(index,0).toString());
        tf2.setText(tbl.getValueAt(index,1).toString());
        tf3.setText(tbl.getValueAt(index,2).toString());
        
    }
      
}
 
