/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Helper;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JTextField;
/**
 *
 * @author nhm95
 */
public class ButtonHelper {
    JButton bt;
   
    public ButtonHelper() {

    }

    class ButtonAction implements ActionListener {

        public final byte PRODUCT = 1;
        public final byte VENDOR = 2;
        public final byte EMPLOYEE = 3;
        public final byte FINALCIAL = 4;
        public final byte INVOICEIN = 5;
        public final byte INVOICEOUT = 6;
        public final byte USER = 7;
        public final byte ACTION_INSERT = 8;
        public final byte ACTION_UPDATE = 9;
        public final byte ACTION_DELETE = 19;
        private byte ACTION;
        private byte TABLE;

        public ButtonAction() {

        }

        public ButtonAction(byte ACTION, byte TABLE) {
            this.ACTION = ACTION;
            this.TABLE = TABLE;
        }
        private void BtnActionPerform(ActionEvent ae) {
            switch (TABLE) {
                case PRODUCT:
                    switch (ACTION) {
                        case ACTION_DELETE:
                            // 
                            break;
                        case ACTION_INSERT:
                            //
                            break;
                        case ACTION_UPDATE:
                            //
                            break;
                    }
                    break;
                case EMPLOYEE:
                    switch (ACTION) {
                        case ACTION_DELETE:
                            //
                            break;
                        case ACTION_INSERT:
                            //
                            break;
                        case ACTION_UPDATE:
                            //
                            break;
                    }
                    break;
                case FINALCIAL:
                    switch (ACTION) {
                        case ACTION_DELETE:
                            //
                            break;
                        case ACTION_INSERT:
                            //
                            break;
                        case ACTION_UPDATE:
                            //
                            break;
                    }
                    break;
                case INVOICEIN:
                    switch (ACTION) {
                        case ACTION_DELETE:
                            //
                            break;
                        case ACTION_INSERT:
                            //
                            break;
                        case ACTION_UPDATE:
                            //
                            break;
                    }
                    break;
                case INVOICEOUT:
                    switch (ACTION) {
                        case ACTION_DELETE:
                            //
                            break;
                        case ACTION_INSERT:
                            //
                            break;
                        case ACTION_UPDATE:
                            //
                            break;
                    }
                    break;
                case USER:
                    switch (ACTION) {
                        case ACTION_DELETE:
                            //
                            break;
                        case ACTION_INSERT:
                            //
                            break;
                        case ACTION_UPDATE:
                            //
                            break;
                    }
                    break;
                case VENDOR:
                    switch (ACTION) {
                        case ACTION_DELETE:
                            //
                            break;
                        case ACTION_INSERT:
                            //
                            break;
                        case ACTION_UPDATE:
                            //
                            break;
                    }
                    break;
            }
        }
        
        @Override
        public void actionPerformed(ActionEvent ae) {
            BtnActionPerform(ae);
        }

    }

}
