/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Helper;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author nhm95
 */
public class AnimationHelper {

    public static void OneLeftOneRight(JPanel ChoiceMain, JPanel Choice, JFrame f, JPanel pnlMain_View, JPanel pnlView1, JPanel pnlView2, JPanel pnlView3, JPanel pnlMain_Controller, JPanel pnlController1, JPanel pnlController2, JPanel pnlController3) {
        Thread moveView = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = pnlMain_View.getX(); i <= f.getWidth(); i += 10) {
                        pnlMain_View.setLocation(i, pnlMain_View.getY());
                        Thread.sleep(5);
                    }
                    pnlView1.setVisible(true);
                    pnlView2.setVisible(false);
                    pnlView3.setVisible(false);
                    for (int i = f.getWidth(); i >= Choice.getWidth(); i -= 10) {
                        pnlMain_View.setLocation(i, pnlMain_View.getY());
                        Thread.sleep(5);
                    }

                } catch (Exception e) {
                }

            }
        });
        moveView.start();
        Thread moveControl = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = pnlMain_Controller.getX(); i >= -pnlMain_Controller.getWidth(); i -= 10) {
                        pnlMain_Controller.setLocation(i, pnlMain_Controller.getY());
                        Thread.sleep(5);
                    }
                    pnlController1.setVisible(true);
                    pnlController2.setVisible(false);
                    pnlController3.setVisible(false);
                    for (int i = -pnlMain_Controller.getWidth(); i <= 0; i += 10) {
                        pnlMain_Controller.setLocation(i, pnlMain_Controller.getY());
                        Thread.sleep(5);
                    }

                } catch (Exception e) {
                }
            }
        });
        moveControl.start();
    }

    public static void OneLeftOneRight(JFrame f, JPanel pnlMain_View, JPanel pnlView1, JPanel pnlView2, JPanel pnlView3, JPanel pnlMain_Controller, JPanel pnlController1, JPanel pnlController2, JPanel pnlController3) {
        Thread moveView = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = pnlMain_View.getX(); i <= f.getWidth(); i += 10) {
                        pnlMain_View.setLocation(i, pnlMain_View.getY());
                        Thread.sleep(5);
                    }
                    pnlView1.setVisible(true);
                    pnlView2.setVisible(false);
                    pnlView3.setVisible(false);
                    for (int i = f.getWidth(); i >= 0; i -= 10) {
                        pnlMain_View.setLocation(i, pnlMain_View.getY());
                        Thread.sleep(5);
                    }

                } catch (Exception e) {
                }

            }
        });
        moveView.start();
        Thread moveControl = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = pnlMain_Controller.getX(); i >= -pnlMain_Controller.getWidth(); i -= 10) {
                        pnlMain_Controller.setLocation(i, pnlMain_Controller.getY());
                        Thread.sleep(5);
                    }
                    pnlController1.setVisible(true);
                    pnlController2.setVisible(false);
                    pnlController3.setVisible(false);
                    for (int i = -pnlMain_Controller.getWidth(); i <= 0; i += 10) {
                        pnlMain_Controller.setLocation(i, pnlMain_Controller.getY());
                        Thread.sleep(5);
                    }

                } catch (Exception e) {
                }
            }
        });
        moveControl.start();
    }

    public static void OneLeftOneRight(JFrame f, JPanel pnlMain_View, JPanel pnlMain_Controller) {
        Thread moveView = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = pnlMain_View.getX(); i <= f.getWidth(); i += 10) {
                        pnlMain_View.setLocation(i, pnlMain_View.getY());
                        Thread.sleep(5);
                    }
                    for (int i = f.getWidth(); i >= 0; i -= 10) {
                        pnlMain_View.setLocation(i, pnlMain_View.getY());
                        Thread.sleep(5);
                    }
                } catch (Exception e) {
                }

            }
        });
        moveView.start();
        Thread moveControl = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = pnlMain_Controller.getX(); i >= -pnlMain_Controller.getWidth(); i -= 10) {
                        pnlMain_Controller.setLocation(i, pnlMain_Controller.getY());
                        Thread.sleep(5);
                    }
                    for (int i = -pnlMain_Controller.getWidth(); i <= 0; i += 10) {
                        pnlMain_Controller.setLocation(i, pnlMain_Controller.getY());
                        Thread.sleep(5);
                    }
                } catch (Exception e) {
                }
            }
        });
        moveControl.start();
    }

    public static void OneLeftOneRightOut(JFrame f, JPanel pnlMain_View, JPanel pnlMain_Controller) {
        Thread moveView = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = pnlMain_View.getX(); i <= f.getWidth(); i += 10) {
                        pnlMain_View.setLocation(i, pnlMain_View.getY());
                        Thread.sleep(5);
                    }
                } catch (Exception e) {
                }

            }
        });
        moveView.start();
        Thread moveControl = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = pnlMain_Controller.getX(); i >= -pnlMain_Controller.getWidth(); i -= 10) {
                        pnlMain_Controller.setLocation(i, pnlMain_Controller.getY());
                        Thread.sleep(5);
                    }
                } catch (Exception e) {
                }
            }
        });
        moveControl.start();
    }

    public static void OneLeftOneRightin(JFrame f, JPanel pnlMain_View, JPanel pnlMain_Controller) {
        Thread moveView = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = f.getWidth(); i >= 0; i -= 10) {
                        pnlMain_View.setLocation(i, pnlMain_View.getY());
                        Thread.sleep(5);
                    }
                } catch (Exception e) {
                }

            }
        });
        moveView.start();
        Thread moveControl = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = -pnlMain_Controller.getWidth(); i <= 0; i += 10) {
                        pnlMain_Controller.setLocation(i, pnlMain_Controller.getY());
                        Thread.sleep(5);
                    }
                } catch (Exception e) {
                }
            }
        });
        moveControl.start();
    }
}
