/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Helper;

import static Model.Helper.ButtonAction.ACTION_DELETE;
import static Model.Helper.ButtonAction.ACTION_INSERT;
import static Model.Helper.ButtonAction.ACTION_UPDATE;
import static Model.Helper.ButtonAction.EMPLOYEE;
import static Model.Helper.ButtonAction.FINALCIAL;
import static Model.Helper.ButtonAction.INVOICEIN;
import static Model.Helper.ButtonAction.INVOICEOUT;
import static Model.Helper.ButtonAction.PRODUCT;
import static Model.Helper.ButtonAction.USER;
import static Model.Helper.ButtonAction.VENDOR;
import Model.Table.CTHDB;
import Model.Table.HoaDonBan;
import Model.Table.TTNS;
import static java.awt.Component.TOP_ALIGNMENT;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import static java.awt.image.ImageObserver.SOMEBITS;
import javax.swing.JButton;
import javax.swing.JTextField;

/**
 *
 * @author nhm95
 */
public class ButtonAction {

    public ButtonAction() {
    }

    public static final byte PRODUCT = 1;
    public static final byte VENDOR = 2;
    public static final byte EMPLOYEE = 3;
    public static final byte FINALCIAL = 4;
    public static final byte INVOICEIN = 5;
    public static final byte INVOICEOUT = 6;
    public static final byte USER = 7;
    public static final byte ACTION_INSERT = 8;
    public static final byte ACTION_UPDATE = 9;
    public static final byte ACTION_DELETE = 10;
   

    public ButtonAction(JButton bt, byte ACTION, byte TABLE, TTNS ttns) {
        bt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                BtnActionPerform(ACTION, TABLE, ttns);
            }
        });
    }

    private void BtnActionPerform(byte ACTION, byte TABLE, TTNS ttns) {
        switch (TABLE) {
            case PRODUCT:
                switch (ACTION) {
                    case ACTION_DELETE:
                        // 
                        break;
                    case ACTION_INSERT:
                        //
                        break;
                    case ACTION_UPDATE:
                        //
                        break;
                }
                break;
            case EMPLOYEE:
                switch (ACTION) {
                    case ACTION_DELETE:
                        //
                        break;
                    case ACTION_INSERT:
                        //
                        ttns.Action(TTNS.INSERT);
                        break;
                    case ACTION_UPDATE:
                        //
                        ttns.Action(TTNS.UPDATE);
                        break;
                }
                break;
            case FINALCIAL:
                switch (ACTION) {
                    case ACTION_DELETE:
                        //
                        break;
                    case ACTION_INSERT:
                        //
                        break;
                    case ACTION_UPDATE:
                        //
                        break;
                }
                break;
            case INVOICEIN:
                switch (ACTION) {
                    case ACTION_DELETE:
                        //
                        break;
                    case ACTION_INSERT:
                        //
                        break;
                    case ACTION_UPDATE:
                        //
                        break;
                }
                break;
            case INVOICEOUT:
                switch (ACTION) {
                    case ACTION_DELETE:
                        //
                        break;
                    case ACTION_INSERT:
                        //

                        break;
                    case ACTION_UPDATE:
                        //
                        break;
                }
                break;
            case USER:
                switch (ACTION) {
                    case ACTION_DELETE:
                        //
                        break;
                    case ACTION_INSERT:
                        //
                        break;
                    case ACTION_UPDATE:
                        //
                        break;
                }
                break;
            case VENDOR:
                switch (ACTION) {
                    case ACTION_DELETE:
                        //
                        break;
                    case ACTION_INSERT:
                        //
                        break;
                    case ACTION_UPDATE:
                        //
                        break;
                }
                break;
        }
    }

}


