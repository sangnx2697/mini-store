/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Helper;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

/**
 *
 * @author nhm95
 */
public class VisibleHelper {

    public static void SetVisible(boolean b, JPanel... panel) {
        for (int i = 0; i < panel.length; i++) {
            panel[i].setVisible(b);
        }

    }

    public static void SetEnable(boolean b, JMenuItem... panel) {
        for (int i = 0; i < panel.length; i++) {
            panel[i].setVisible(b);
        }

    }
    public static void SetEnable(boolean b, JMenu... panel) {
        for (int i = 0; i < panel.length; i++) {
            panel[i].setVisible(b);
        }

    }
     public static void SetEnable(boolean b, boolean... panel) {
        for (int i = 0; i < panel.length; i++) {
            panel[i]=b;
        }

    }

}
