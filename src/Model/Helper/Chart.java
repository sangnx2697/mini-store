/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Helper;

import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.LayoutManager;
import java.awt.Point;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author nhm95
 */
public class Chart extends JPanel {

    /*   public static void main(String[] args) {
        Chart st = new Chart(8000, 8000, 10000000 * new Random().nextInt(1000), 10000000 * new Random().nextInt(1000), 10000000 * new Random().nextInt(1000), 10000000 * new Random().nextInt(1000), 10000000 * new Random().nextInt(1000), 10000000 * new Random().nextInt(1000), 10000000 * new Random().nextInt(1000), 10000000 * new Random().nextInt(1000), 10000000 * new Random().nextInt(1000), 10000000 * new Random().nextInt(1000), 10000000 * new Random().nextInt(1000), 10000000 * new Random().nextInt(1000));
        st.setVisible(true);
    }*/
    Object[] value;
    private int WIDTH;
    private int HEIGHT;

    @Override
    public int getWidth() {
        return WIDTH; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getHeight() {
        return HEIGHT; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LayoutManager getLayout() {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    public Chart(int WIDTH, int HEIGHT, String Title, String CurrentX, String CurrentY, String NameY, Object[] NameX, Object[] valueX) {
        this.HEIGHT = HEIGHT;
        this.WIDTH = WIDTH;
        this.setLayout(null);
        setStatictis(valueX);
        ChartPanel cpn = new ChartPanel(CreateChart(Title, CurrentX, CurrentY, NameY, NameX, valueX));
        cpn.setSize(getWidth(), getHeight());
        add(cpn);
    }

    public JFreeChart CreateChart(String Title, String CurrentX, String CurrentY, String NameY, Object[] NameX, Object[] valueX) {
        JFreeChart chart = ChartFactory.createBarChart(Title, CurrentX, CurrentY, Createdataset(NameY, NameX, valueX));
        return chart;
    }

    public DefaultCategoryDataset Createdataset(String NameY, Object[] NameX, Object[] valueX) {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (int i = 0; i < valueX.length; i++) {
            dataset.addValue(Double.parseDouble(valueX[i].toString()), NameY.toUpperCase(), NameX[i].toString());
        }
        return dataset;
    }

    private void setStatictis(Object[] value) throws HeadlessException {
        this.value = value;
    }

}
