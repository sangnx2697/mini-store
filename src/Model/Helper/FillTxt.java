/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Helper;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author vokie
 */
public class FillTxt {

    public static void Showsp(JTable tb, JTextField txtID, JTextField txtName, JTextField txtVenD, JComboBox<String> cboUnit, JTextField txtPrice, JTextField txtQuar) {
        tb.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int r = tb.getSelectedRow();
                        if (r < 0) {
                            return;
                        }
                        txtID.setText(tb.getValueAt(r, 0).toString());
                        txtName.setText(tb.getValueAt(r, 1).toString());
                        txtVenD.setText(tb.getValueAt(r, 2).toString());
                        cboUnit.setSelectedItem(tb.getValueAt(r, 3).toString());
                        txtPrice.setText(tb.getValueAt(r, 4).toString());
                        txtQuar.setText(tb.getValueAt(r, 5).toString());
                    }
                });
                t.start();
            }
        });

    }

    public static void ShowHDM(JTable tb, JTextField txtID, JTextField txtEmID, JTextField txtProductID, JTextField txtVenD, JTextField txtPrice, JTextField txtQuan, JTextField txtDate, JTextField txtTotal) {
        tb.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int r = tb.getSelectedRow();
                        if (r < 0) {
                            return;
                        }
                        txtID.setText(tb.getValueAt(r, 0).toString());
                        txtEmID.setText(tb.getValueAt(r, 1).toString());
                        txtProductID.setText(tb.getValueAt(r, 2).toString());
                        txtVenD.setText(tb.getValueAt(r, 3).toString());
                        txtPrice.setText(tb.getValueAt(r, 4).toString());
                        txtQuan.setText(tb.getValueAt(r, 5).toString());
                        txtDate.setText(tb.getValueAt(r, 6).toString());
                        txtTotal.setText(tb.getValueAt(r, 7).toString());
                    }
                });
                t.start();
            }
        });

    }

    public static void ShowVendor(JTable tb, JTextField txtVenID, JTextField txtName, JTextArea txtAddress) {
        tb.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int r = tb.getSelectedRow();
                        if (r < 0) {
                            return;
                        }
                        txtVenID.setText(tb.getValueAt(r, 0).toString());
                        txtName.setText(tb.getValueAt(r, 1).toString());
                        txtAddress.setText(tb.getValueAt(r, 2).toString());
                    }
                });
                t.start();
            }
        });

    }

    public static void ShowTTNV(JTable tb, JTextField txtID, JTextField txtName, JTextArea txtAddress, JTextField txtIden, JComboBox<String> cboPos, JLabel lblAvt) {
        tb.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            int r = tb.getSelectedRow();
                            if (r < 0) {
                                return;
                            }
                            txtID.setText(tb.getValueAt(r, 0).toString());
                            txtName.setText(tb.getValueAt(r, 1).toString());
                            txtAddress.setText(tb.getValueAt(r, 2).toString());
                            txtIden.setText(tb.getValueAt(r, 3).toString());
                            cboPos.setSelectedItem(tb.getValueAt(r, 4).toString());
                            lblAvt.setText(tb.getValueAt(r, 5).toString());
                            SolveIMG.getBackground("src\\Icon\\" + tb.getValueAt(r, 5).toString(), lblAvt, lblAvt.getWidth(), lblAvt.getHeight());
                        } catch (Exception e) {
                            try {
                                SolveIMG.getBackground("src\\Icon\\account.png", lblAvt, lblAvt.getWidth(), lblAvt.getHeight());
                            } catch (Exception ex) {
                                Logger.getLogger(FillTxt.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        }
                    }
                });
                t.start();
            }

        });

    }

    public static void ShowUsers(JTable tb, JTextField txtID, JTextField txtUser, JPasswordField pwPass, JComboBox<String> cboRole, JLabel lblAvt) {
        tb.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int r = tb.getSelectedRow();
                        if (r < 0) {
                            return;
                        }
                        txtID.setText(tb.getValueAt(r, 0).toString());
                        txtUser.setText(tb.getValueAt(r, 1).toString());
                        pwPass.setText(tb.getValueAt(r, 2).toString());
                        cboRole.setSelectedItem(tb.getValueAt(r, 3).toString());
                        lblAvt.setText(tb.getValueAt(r, 4).toString());
                        if (tb.getValueAt(r, 4).toString().trim().equals("")) {
                            try {
                                SolveIMG.getBackground("src\\Icon\\human.png", lblAvt, lblAvt.getWidth(), lblAvt.getHeight());
                            } catch (Exception ex) {
                                Logger.getLogger(FillTxt.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        } else {
                            try {
                                SolveIMG.getBackground(tb.getValueAt(r, 4).toString().equals("xxx.jpg") ? "src\\Icon\\human.png" : "src\\Icon\\" + tb.getValueAt(r, 4).toString(), lblAvt, lblAvt.getWidth(), lblAvt.getHeight());
                            } catch (Exception ex) {
                                Logger.getLogger(FillTxt.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                });
                t.start();
            }
        });

    }

    public static void ShowHDB(JTable tb, JTextField txtID, JTextField txtEmID, JTextField txtDate, JTextField txtProduct, JTextField txtPrice, JTextField txtQuar, JTextField txtTotal) {
        tb.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int r = tb.getSelectedRow();
                        if (r < 0) {
                            return;
                        }
                        txtID.setText(tb.getValueAt(r, 0).toString());
                        txtEmID.setText(tb.getValueAt(r, 1).toString());
                        txtDate.setText(tb.getValueAt(r, 2).toString());
                        txtProduct.setText(tb.getValueAt(r, 3).toString());
                        txtPrice.setText(tb.getValueAt(r, 4).toString());
                        txtQuar.setText(tb.getValueAt(r, 5).toString());
                        txtTotal.setText(tb.getValueAt(r, 6).toString());

                    }
                });
                t.start();
            }
        });

    }

    public static void ShowLuongNV(JTable tb, JTextField ID, JTextField Name, JTextField Salary) {
        tb.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int r = tb.getSelectedRow();
                        if (r < 0) {
                            return;
                        }
                        ID.setText(tb.getValueAt(r, 0).toString());
                        Name.setText(tb.getValueAt(r, 1).toString());
                        Salary.setText(tb.getValueAt(r, 2).toString());

                    }
                });
                t.start();
            }
        });

    }
}
