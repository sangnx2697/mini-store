/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Helper;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import library.XLICon;

/**
 *
 * @author nhm95
 */
public class LabelHeper extends MouseAdapter {

    String IconPathIn;
    String IconPathOut;
    JLabel lbl;
    int WIDTH;
    int HEIGHT;
    Color colorIn;
    Color colorOut;
    boolean chk;

    public LabelHeper(String IconPathIn, String IconPathOut, JLabel lbl, int WIDTH, int HEIGHT, Color colorIn, Color colorOut, boolean chk) {
        this.IconPathIn = IconPathIn;
        this.IconPathOut = IconPathOut;
        this.lbl = lbl;
        this.WIDTH = WIDTH;
        this.HEIGHT = HEIGHT;
        this.colorIn = colorIn;
        this.colorOut = colorOut;
        this.chk = chk;
    }

    public LabelHeper() {
    }

    public static void LabelActionHover(String IconPathIn, String IconPathOut, JLabel lbl, int WIDTH, int HEIGHT, Color colorIn, Color colorOut) {
        lbl.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseExited(MouseEvent me) {

                XLICon.getBackgroundIcon(IconPathOut, lbl, WIDTH, HEIGHT);
                lbl.setForeground(colorOut != colorOut.BLACK || colorOut.getRGB() < 0 ? colorOut : colorOut.black);
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                XLICon.getBackgroundIcon(IconPathIn, lbl, WIDTH, HEIGHT);
                lbl.setForeground(colorIn != colorIn.BLACK || colorIn.getRGB() < 0 ? colorIn : colorIn.black);
            }

        });
    }

}
