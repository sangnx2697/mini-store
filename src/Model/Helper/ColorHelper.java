/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Helper;

import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JPanel;

/**
 *
 * @author nhm95
 */
public class ColorHelper {

    public static void ButtonColor(JButton [] b) {
        JColorChooser choosecl = new JColorChooser();
        Color color = choosecl.showDialog(null, "Choose Color", null);
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                for (JButton bt : b) {
                    bt.setBackground(color);
                }
            }
        });
        t.start();
    }
     public static void PanelColor(JPanel[] b) {
        JColorChooser choosecl = new JColorChooser();
        Color color = choosecl.showDialog(null, "Choose Color", null);
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                for (JPanel bt : b) {
                    bt.setBackground(color);
                }
            }
        });
        t.start();
    }
}
