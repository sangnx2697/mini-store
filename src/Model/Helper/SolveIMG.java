/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Helper;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;

/**
 *
 * @author vokie
 */
public class SolveIMG {

    public static Icon getBackground(String path, JButton lb, int width, int height) {
        Icon aIcon = null;
        try {
            BufferedImage bi = ImageIO.read(new File(path));
            ImageIcon icon = new ImageIcon(bi.getScaledInstance(width, height, bi.SCALE_AREA_AVERAGING));
            lb.setIcon(icon);
            aIcon = icon;
        } catch (Exception e) {
        }
        return aIcon;
    }

    public static void getBackground(String path, JLabel lb, int width, int heigh) throws Exception {

        BufferedImage bi = ImageIO.read(new File(path));
        ImageIcon icon = new ImageIcon(bi.getScaledInstance(width, heigh, bi.SCALE_AREA_AVERAGING));
        lb.setIcon(icon);

    }

    public static String OpenIMG(JLabel lb) {
        String link = null;
        JFileChooser jfc = new JFileChooser();
        int x = jfc.showOpenDialog(null);
        if (x == JFileChooser.APPROVE_OPTION) {
            File taptin = jfc.getSelectedFile();
            link = taptin.getAbsolutePath().substring(taptin.getAbsolutePath().lastIndexOf("\\") + 1, taptin.getAbsolutePath().length());
            try {
                FileInputStream in = new FileInputStream(taptin);
                FileOutputStream out = new FileOutputStream("src\\Icon\\" + link);
                byte[] mang = new byte[1024];
                int lengh;
                while ((lengh = in.read(mang)) > 0) {
                    out.write(mang, 0, lengh);
                }
                SolveIMG.getBackground("src/Icon/" + link, lb, lb.getWidth(), lb.getHeight());

            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
        return link;
    }

}
