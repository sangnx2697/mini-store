/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Helper;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author vokie
 */
public class Dialog extends JOptionPane {

    private final String MESSAGE = "System Notifcation!!";
    private static final int YES_NO = JOptionPane.YES_NO_OPTION;
    private static final int YES_NO_CANCEL = JOptionPane.YES_NO_CANCEL_OPTION;
    private static final int YES = JOptionPane.YES_OPTION;

    

    public Dialog() {
    }

    public  void Showmess(Component cbnComponent, String message, int TypeM, Icon icon) {
        showMessageDialog(cbnComponent, message, MESSAGE, TypeM, icon);
    }

    public String Inputmess(Component cbnComponent, String message, int TypeM, Icon icon) {
        Object input = showInputDialog(cbnComponent, message, MESSAGE, TypeM, icon, null, null);
        return input.toString();
    }

    public boolean confirm(Component cbnComponent, String message, int Option, int TypeM) {
        int result = JOptionPane.showConfirmDialog(cbnComponent, message,
                MESSAGE,
                Option, TypeM);
        return result == JOptionPane.YES_OPTION;
    }

}
