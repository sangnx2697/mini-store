/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clock;

import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author nhm95
 */
public class clock1 extends JComponent implements Runnable{

    private Image im;
    private Image dim;
    private Graphics dgr;
    private int Xmr;
    private int Ymr;
    private int Xhr;
    private int Yhr;
    private int WIDTH;
    private int HEIGHT;
    /**
     * Creates new form NewJFrame
     */
    public clock1(int WIDTH, int HEIGTH) {
        this.setSize(this.WIDTH=WIDTH, this.HEIGHT=HEIGTH);
        
    }

    public clock1() {
        this.setSize(100, 100);
    }

    @Override
    public void paint(Graphics grphcs) {
        dim = createImage(this.getWidth(), this.getHeight());
        dgr = dim.getGraphics();
        paintComponents(dgr);
        grphcs.drawImage(dim, 0, 0, this);
    }

    @Override
    public void paintComponents(Graphics g) {
        try {
            BufferedImage bf = ImageIO.read(new File("src/Icon/Clock.png"));
            ImageIcon ima = new ImageIcon(bf.getScaledInstance(this.getWidth() - 50, this.getHeight() - 50, bf.SCALE_SMOOTH));
            im = ima.getImage();
        } catch (Exception e) {

        }
        g.drawImage(im, 0, 0, this);
        g.drawLine((this.getWidth()+this.getHeight())/2, (this.getWidth()+this.getHeight())/2,50*(this.getWidth()+this.getWidth())/2+((this.getWidth()+this.getWidth())/2), 50*(this.getWidth()+this.getWidth())/2+((this.getWidth()+this.getWidth())/2));
      
    }

    @Override
    public void run() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
class test extends JFrame {
    public test() throws HeadlessException {
        clock1 c = new clock1(getWidth(), getHeight());
        setUndecorated(true);
        setSize(400, 400);
        setShape(new Ellipse2D.Float(2, 2, (this.getWidth()*85/100), (getHeight()*85/100)));
        setShape(new Rectangle2D.Double(this.getWidth()/4, (this.getHeight()-this.getHeight()/4), 50, 20));
        add(c);
        setLocationRelativeTo(null);
        this.setVisible(true);
    }
    public static void main(String[] args) {
        new test();
    }
}
